<?php
/**
 *
 */

namespace app\models;

use yurus\core\base\Model;

class User extends Model {

    public $attributes = [
        "name" => "",
        "login" => "",
        "active" => "",
        "pass" => "",
        "hash" => "",
        "id_teacher" => "",
        "schulte" => "",
        "letter_search" => "",
        "digital_search" => "",
        "even_odd" => "",
        "rem_number" => "",
        "run_words" => "",
        "word_search" => "",
        "words_twins" => "",
        "field_view" => "",
        "chain_words" => "",
        "reading_grade" => "",
        "speed_grade" => "",
        "accelerator" => "",
        "text_search" => "",
        "level_text" => "",
        "birthday" => "",
        "create_at" => "",
    ];

    public $table = 'users';

    public $attributeLabels = [
        "id" => "Id",
        "name" => "Имя",
        "login" => "Логин",
        "pass" => "Пароль",
        "id_teacher" => "Преподаватель",
        "birthday" => "День рождения",
        "active" => "Активность",
        "schulte" => 'Таблица Шульте',
        "letter_search" => "Поиск букв",
        "digital_search" => "Поиск цифр",
        "word_search" => "Поиск слова",
        "even_odd" => "Чет-Нечет",
        "rem_number" => "Запомни число",
        "run_words" => "Бегушие слова",
        "words_twins" => "Слова близнецы",
        "field_view" => "Поле зрения",
        "chain_words" => "Цепь слов",
        "reading_grade" => "Оценка чтения",
        "speed_grade" => "Скорость чтения",
        "accelerator" => "Ускоритель чтения",
        "search_text" => "Ускоритель чтения",
        "level_text" => "Ускоритель чтения",
        "create_at" => "Дата регистрации",
    ];

    public $rules = [
        'required' => [
            ['name'],
            ['login'],
            ['id_teacher'],
        ],
    ];

    /**
     * Сохранение данных пользователя
     *
     * @return bool
     */
    public function saveUser() {

        $fields = "";
        $valueFields = [];

        foreach ( $this->attributes as $item=>$value ) {

            if ( $value ) {

                $fields .= "`" . $item . "` =:" . $item . ", ";
                $valueFields[$item] = $value;

            }

        }

        $fields = substr($fields, 0, -2);

        $sql = "INSERT INTO `" . $this->table . "` SET " . $fields;

        if ( $this->pdo->execute($sql, $valueFields) ) {

            return true;

        } else {

            $_SESSION['errors'] = "Произошла ошибка, попробуйте позже";
            return false;

        }

    }

    /**
     * Редактирование данных пользователя
     *
     * @param $id integer id пользователя, данные которого редактируются.
     * @return bool
     */
    public function updateUser($id) {

        $fields = "";
        $valueFields = [];

        foreach ( $this->attributes as $item=>$value ) {

            if ( trim($value) != '' ) {

                $fields .= "`" . $item . "` =:" . $item . ", ";

                $valueFields[$item] = $value;

            }

        }

        $fields = substr($fields, 0, -2);

        $sql = "UPDATE `" . $this->table . "` SET " . $fields . " WHERE id= '" . $id . "'";

        if ( $this->pdo->execute($sql, $valueFields) ) {

            return true;

        } else {

            $_SESSION['errors'] = "Произошла ошибка, попробуйте позже";

            return false;

        }

    }

    /**
     * Проверка на уникальность логина
     *
     * @return bool
     */
    public function checkUnique() {

        $where = "`login` = ?";
        $value = [$this->attributes['login']];
        $dataFind = $this->findOne($this->table, $where, $value);

        if ( $dataFind ) {

            return false;

        }

        return true;

    }

    /**
     * Проверка пользователя при входе в тренажёр
     *
     * @return bool
     */
    public function login() {

        $login = !empty(trim($_POST['login'])) ? $_POST['login'] : null;
        $password = !empty(trim($_POST['password'])) ? $_POST['password'] : null;

        $sql = "SELECT u.*, a.`role`, a.`date_end` FROM `" . $this->table . "` u INNER JOIN `admin` a ON a.`id` = u.`id_teacher` WHERE u.`login` = ? LIMIT 1";

        $value = array($login);

        if ( $login && $password ) {

            $user = $this->pdo->query($sql, $value);

            if ( $user ) {

                    if ( password_verify($password, $user[0]['pass']) ) {

                    if ( time() - $user[0]['date_end'] > 0 && ($user[0]['role'] == 3 || $user[0]['role'] == 4) ) {

                        $_SESSION['errors'] = 'Доступ запрещён. Обратитесь к Вашему преподавателю.';

                        return false;
                    }

                    foreach ( $user[0] as $key => $value ) {

                        if ( $key != 'pass' ) {
                            if ( $key != 'hash' && $key != 'pass' ) $_SESSION['user'][$key] = $value;
                        }

                    }

                    $_SESSION['user']['role'] = 'pupil';

                    if ( isset($_POST['remember']) ) {

                        $valueCookie = md5($_POST['login'] . "+" . $_POST['password'] . "+genom_speed_reading_hash");
                        setcookie("_user_rem", $valueCookie, "2524608000", "/");
                        setcookie("_user_role", 'pupil', "2524608000", "/");

                        $sql = "UPDATE `" . $this->table . "` SET `hash` = ? WHERE `id` = ?";

                        $value = [$valueCookie, $user[0]['id']];
                        $this->pdo->execute($sql, $value);

                    }

                    return true;

                }

            }

            $_SESSION['errors'] = 'Пользователь с таким логином или паролем не найден';

            return false;
        }

    }

    /**
     * Проверка пользователя, если у него записаны в браузере куки
     *
     * @return bool
     */
    public function rememberLogin() {

        $sql = "`hash` = ?";

        $user = $this->findOne($this->table, $sql, array($_COOKIE['_user_rem']));

        if ( $user ) {

            foreach ( $user[0] as $key => $value ) {

                if ( $key != 'pass' ) {

                    if ( $key != 'hash' ) $_SESSION['user'][$key] = $value;

                }

            }

            $_SESSION['user']['role'] = 'pupil';

            return true;

        }

        return false;

    }

    public function allUsers($where = 1) {

        $fields = "`id`, `active`";

        $dataUsers = $this->find($fields, $this->table, $where);

        return $dataUsers;

    }

    public function updateDataPracticeUser($id, $updateFields, $level) {

        $sql = "UPDATE `" . $this->table . "` SET `" . $updateFields ."` = ? WHERE `id` = '" . $id . "'";

        $valueFields = [$level];

        if ( $this->pdo->execute($sql, $valueFields) ) {

            return true;

        } else {

            $_SESSION['errors'] = "Произошла ошибка, попробуйте позже";

            return false;

        }

    }

}