<?php
/**
 *
 */

namespace app\models;

use yurus\core\base\Model;

class Guest extends Model {

    public $table = 'guest';
    public $attributes = [
        "login" => "",
        "password" => "",
        "guest_ip" => "",
    ];

    public $rules = [
        'required' => [
            ['login'],
            ['password'],
        ],
    ];

    public function saveUser() {

        $fields = "";
        $valueFields = "";

        foreach ( $this->attributes as $item=>$value ) {

            $fields .= "`" . $item . "` =:" . $item . ", ";
            $valueFields .= "'" . $value . "', ";

        }

        $fields = substr($fields, 0, -2);

        $sql = "INSERT INTO `" . $this->table . "` SET " . $fields;

        if ( $this->pdo->execute($sql, $this->attributes) ) {

            return true;

        } else {

            $_SESSION['errors'] = "Произошла ошибка, попробуйте позже";

            return false;

        }

    }

    public function login() {

        $login = !empty(trim($_POST['login'])) ? $_POST['login'] : null;
        $password = !empty(trim($_POST['password'])) ? $_POST['password'] : null;

        $where = "`guest_login` = ?";

        $value = array($login);

        if ( $login && $password ) {

            $user = $this->findOne($this->table, $where, $value);

            if ( $user ) {

                if ( md5($password) == $user[0]['guest_pass'] ) {

                    foreach ( $user[0] as $key => $value ) {

                        if ( $key != 'password' ) $_SESSION['user'][$key] = $value;

                    }

                    $_SESSION['user']['role'] = 'guest';

                    if ( time() - strtotime($user[0]['time_reg'] ) > 172800 ) {

                        $_SESSION['errors'] = 'Ваш ознакомительный период закончен';

                        return false;

                    }

                    return true;

                }

            }

            $_SESSION['errors'] = 'Пользователь с таким логином или паролем не найден';

            return false;
        }

    }

}