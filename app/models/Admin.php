<?php
/**
 *
 */

namespace app\models;

use yurus\core\base\Model;

class Admin extends Model {

    public $table = 'admin';
    public $attributes = [
        "id" => "",
        "name" => "",
        "login" => "",
        "pass" => "",
        "role" => "",
        "parent_id" => "",
        "active" => "",
        "pay_rent" => "",
        "count_users" => "",
        "date_end" => "",
        'create_at' => "",
        "email" => "",
        "phone" => "",
    ];

    public $attributeLabels = [
        "id" => "Id",
        "name" => "Имя",
        "login" => "Логин",
        "pass" => "Пароль",
        "role" => "Роль",
        "parent_id" => "Учебный центр (директор)",
        "active" => "Активность",
        "pay_rent" => "Сумма оплаты",
        "count_users" => "Максимальное кол-во учеников",
        "date_end" => "Дата окончания подписки",
        'create_at' => "Дата регистрации",
        "email" => "E-mail",
        "phone" => "Телефон",
    ];

    public $rules = [
        'required' => [
            ['name'],
            ['login'],
            ['pass'],
        ],
        'email' => [
            ['email'],
        ],
    ];

    /**
     * Сохранение данных пользователя
     *
     * @return bool
     */
    public function saveUser() {

        $fields = "";
        $valueFields = [];

        foreach ( $this->attributes as $item=>$value ) {

            if ( $value ) {

                $fields .= "`" . $item . "` =:" . $item . ", ";
                $valueFields[$item] = $value;

            }

        }

        $fields = substr($fields, 0, -2);

        $sql = "INSERT INTO `" . $this->table . "` SET " . $fields;

        if ( $this->pdo->execute($sql, $valueFields) ) {

            return true;

        } else {

            $_SESSION['errors'] = "Произошла ошибка, попробуйте позже";

            return false;

        }

    }

    /**
     * Редактирование данных пользователя
     *
     * @param $id integer id пользователя, данные которого редактируются.
     * @return bool
     */
    public function updateUser($id) {

        $fields = "";
        $valueFields = [];

        foreach ( $this->attributes as $item=>$value ) {

            if ( trim($value) != '' ) {

                $fields .= "`" . $item . "` =:" . $item . ", ";

                $valueFields[$item] = $value;

            }

        }

        $fields = substr($fields, 0, -2);

        $sql = "UPDATE `" . $this->table . "` SET " . $fields . " WHERE `id`= '" . $id . "'";

        if ( $this->pdo->execute($sql, $valueFields) ) {

            return true;

        } else {

            $_SESSION['errors'] = "Произошла ошибка, попробуйте позже";

            return false;

        }


    }

    /**
     * Обновление даты у преподавателей при изменении даты у директора центра
     *
     * @param $id
     * @param $newDate
     * @return bool
     */
    public function updateDate($id, $newDate) {

        $valueFields = [];

        $valueFields[] = $newDate;

        $sql = "UPDATE `" . $this->table . "` SET `date_end` = ? WHERE `parent_id` = '" . $id . "'";

        if ( $this->pdo->execute($sql, $valueFields) ) {

            return true;

        } else {

            $_SESSION['errors'] = "Не обновлены даты преподавателей центра, сообщите об этой ошибке администратору";

            return false;

        }


    }


    /**
     * Проверка на уникальность логина
     *
     * @return bool
     */
    public function checkUnique() {

        $where = "`login` = ?";
        $value = [$this->attributes['login']];
        $dataFind = $this->findOne($this->table, $where, $value);

        if ( $dataFind ) {

            return false;

        }

        return true;

    }

    /**
     * Проверка пользователя при входе в тренажёр
     *
     * @return bool
     */
    public function login() {

        $login = !empty(trim($_POST['login'])) ? $_POST['login'] : null;
        $password = !empty(trim($_POST['password'])) ? $_POST['password'] : null;

        $where = "`login` = ?";

        $value = array($login);

        // Если переданы логин и пароль
        if ( $login && $password ) {

            // находим в базе данных пользователя с этими логином и паролем
            $user = $this->findOne($this->table, $where, $value);
            $user = $user[0];

            // если пользователь с такими логином и паролем найден
            if ( $user ) {

                // проверяем соответствие пароля
                if ( password_verify($password, $user['pass']) ) {

                    if ( time() - $user['date_end'] > 0 && ( $user['role'] === "3" || $user['role'] === "4" ) ) {

                        $_SESSION['user']['access'] = 'no';

                    }

                    foreach ( $user as $key => $value ) {

                        if ( $key != 'pass' ) {

                            if ( $key != 'hash' ) $_SESSION['user'][$key] = $value;

                        }

                    }

                    // записываем в сессии роль "teacher"
                    $_SESSION['user']['adm_role'] = 'teacher';

                    // если проставлена галочка "Запомнить меня"
                    if ( isset($_POST['remember']) ) {

                        // шифруем и записываем куки
                        $valueCookie = md5($_POST['login'] . "+" . $_POST['password'] . "+genom_speed_reading_hash");
                        setcookie("_user_rem", $valueCookie, "2524608000", "/");
                        setcookie("_user_role", 'teacher', "2524608000", "/");

                        $sql = "UPDATE `" . $this->table . "` SET `hash` = ? WHERE `id` = ?";

                        $value = [$valueCookie, $user['id']];
                        $this->pdo->execute($sql, $value);

                    }

                    return true;

                }

            }

            $_SESSION['errors'] = 'Пользователь с таким логином или паролем не найден';

            return false;
        }

    }

    /**
     * Проверка пользователя, если у него записаны в браузере куки
     *
     * @return bool
     */
    public function rememberLogin() {

        $where = "`hash` = ?";

        $user = $this->findOne($this->table, $where, array($_COOKIE['_user_rem']));

        if ( $user ) {

            foreach ( $user[0] as $key => $value ) {

                if ( $key != 'pass' ) {

                    if ( $key != 'hash' ) $_SESSION['user'][$key] = $value;

                }

            }

            $_SESSION['user']['role'] = 'teacher';

            return true;

        }

        return false;

    }

    /**
     * @param int $where
     * @return array
     */
    public function allUsers($where = 1) {

        $fields = "`id`, `role`, `parent_id`, `active`";

        $dataAdmins = $this->find($fields, $this->table, $where);

        return $dataAdmins;

    }

    /**
     * @param int $where
     * @return array
     */
    public function users($where = 1) {

        $dataAdmins = $this->findAll($this->table, $where);

        return $dataAdmins;

    }

}