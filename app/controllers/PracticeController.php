<?php
/**
 *
 */

namespace app\controllers;

use app\models\User;

class PracticeController extends AppController
{
    public function __construct($route) {

        parent::__construct($route);

        if ( !isset($_SESSION['user']) ) {

            redirect('/');

        }

        if ( isset($_SESSION['user']['access']) && $_SESSION['user']['access'] === 'no' ) {

            redirect('practice/index');

        }

    }

    public $layout = 'training';

    public function actionIndex() {

        $this->set(['languageConstants' => $this->languageConstants]);

    }

    /**
     * Упражнение "Таблица Шульте"
     */
    public function actionSchulte()
    {
        $infoPractice = file_get_contents("data/" . $this->lang . '/info/schulte.txt');

        if ($_POST) {

            if (isset($_POST['alphabet'])) {

                // получение алфавита, согласно используемого языка
                $alphabet = file_get_contents('data/' . $this->lang . '/alf.txt');

                echo $alphabet;

                exit();
            }

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('schulte');

                    if (!$_SESSION['user']['birthday']) {

                        echo 0;

                    } else {

                        echo $this->userAge;

                    }

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);
    }

    /**
     * Упражнение "Поиск букв"
     */
    public function actionLetterSearch()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/letter-search.txt');
        $coupleFile = parse_ini_file('data/' . $this->lang . '/couple.ini');
        $couple = [];

        foreach ($coupleFile as $item) {

            $elem = explode(':', $item);
            $couple[] = $elem[0];

        }

        if ($_POST) {

            if (isset($_POST['alphabet'])) {

                $letters = file_get_contents('data/' . $this->lang . '/alf.txt');

                echo $letters;

                exit();

            }

            if (isset($_POST['couple'])) {

                $coupleFile = parse_ini_file('data/' . $this->lang . '/couple.ini');
                $arrCouple = explode(':', $coupleFile[$_POST['couple']]);
                $arrCouple = $arrCouple[1];

                echo $arrCouple;

                exit();

            }

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('letter_search');

                    echo 'level = ' . $_POST['level'];

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['couple' => $couple, 'languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Поиск цифр"
     */
    public function actionDigitalSearch()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/digital-search.txt');

        if ($_POST) {


            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('digital_search');

                    echo 'level = ' . $_POST['level'];

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Чет/нечет"
     */
    public function actionEvenOdd()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/even-odd.txt');

        if ($_POST) {

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('even_odd');

                    echo 'level = ' . $_POST['level'];

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Запомни число"
     */
    public function actionRememberNumber()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/remember-number.txt');

        if ($_POST) {

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('rem_number');

                    echo 'level = ' . $_POST['level'];

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение"Бегущие слова"
     */
    public function actionRunningWords()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/running-words.txt');

        if ($_POST) {

            if (isset($_POST['words'])) {

                $words = file_get_contents('data/' . $this->lang . '/words-run.txt');

                $letters = parse_ini_file('data/' . $this->lang . '/alf-width.ini');

                $data = [
                    'words' => $words,
                    'letters' => $letters,
                ];

                echo json_encode($data);

                exit();

            }


            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('run_words');

                    echo 'level = ' . $_POST['level'];

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     *  Упражнение "Поиск слова"
     */
    public function actionWordSearch()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/word-search.txt');
        $coupleFile = parse_ini_file('data/' . $this->lang . '/couple-words.ini');
        $couple = [];

        foreach ($coupleFile as $item) {

            $elem = explode(':', $item);
            $couple[] = $elem[0];

        }

        if ($_POST) {

            if (isset($_POST['words'])) {

                $words = file_get_contents('data/' . $this->lang . '/word-search.txt');

                $letters = parse_ini_file('data/' . $this->lang . '/alf-width.ini');

                $letters = array_keys($letters);

                $data = [
                    'words' => $words,
                    'letters' => $letters,
                ];

                echo json_encode($data);

                exit();

            }

            if (isset($_POST['couple'])) {

                $coupleFile = parse_ini_file('data/' . $this->lang . '/couple-words.ini');
                $arrCouple = explode(':', $coupleFile[$_POST['couple']]);
                $arrCouple = $arrCouple[1];

                echo $arrCouple;

                exit();

            }

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('word_search');

                    echo 'level = ' . $_POST['level'];

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['couple' => $couple, 'languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Слова Близнецы"
     */
    public function actionWordsTwins()
    {
        $infoPractice = file_get_contents("data/" . $this->lang . '/info/words-twins.txt');

        if ($_POST) {

            if (isset($_POST['words'])) {

                $handle = fopen('data/' . $this->lang . '/word-twins.csv', 'r');
                $words = fgetcsv($handle, 0, ',');
                fclose($handle);
                $data = [];
                foreach ($words as $key => $item) {
                    $data[$key] = explode('-', $item);
                }

                echo json_encode($data);

                exit();

            }

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('words_twins');

                    echo 'level = ' . $_POST['level'];

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Поле зрения"
     */
    public function actionFieldView()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/fields-view.txt');

        if ($_POST) {

            if (isset($_POST['letters'])) {

                $letters = parse_ini_file('data/' . $this->lang . '/alf-width.ini');

                $letters = array_keys($letters);

                echo json_encode($letters);

                exit();

            }

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('field_view');

                    echo 'level = ' . $_POST['level'];

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Центральная точка"
     */
    public function actionCentralPoint()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/central-point.txt');

        if ($_POST) {

            if (isset($_POST['central'])) {

                $text = file_get_contents('data/' . $this->lang . '/central-point.txt');

                echo $text;

                exit();

            }

        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Оценка чтения"
     */
    public function actionReadingGrade()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/reading-grade.txt');

        $titles = [];

        // в зависимости от возраста пользователя выбираем в библиотеке все файлы с текстами
        if ($this->userAge <= 9 && $this->userAge !== 0) {
            $listFiles = glob('data/'. $this->lang . '/lib/baby/*.xml');
            $numberPart = 4;
        } else {
            $listFiles = glob('data/' . $this->lang . '/lib/*.xml');
            $numberPart = 3;
        }

        // формируем массив с названиями текстов
        foreach ($listFiles as $currentFile) {
            $tmpStr = explode('/', $currentFile);
            $tmpStr = $tmpStr[$numberPart];
            $tmpStr = explode('.', $tmpStr, -1);
            $tmpStr = $tmpStr[0];
            $titles[] = $tmpStr;
        }

        $keyFile = 0;

        if (isset($_SESSION['user']['reading_grade']) && $_SESSION['user']['reading_grade'] !== '') {

            $keyFile = array_search($_SESSION['user']['reading_grade'], $titles);

            $keyFile++;

            if ($keyFile === count($titles)) {

                $keyFile = 0;

            }

        }

        if ($this->userAge <= 9 && $this->userAge !== 0) {
            $dataText = simplexml_load_file('data/' . $this->lang . '/lib/baby/' . $titles[$keyFile] . '.xml');
        } else {
            $dataText = simplexml_load_file('data/' . $this->lang . '/lib/' . $titles[$keyFile] . '.xml');
        }

        $dataText = json_decode(json_encode($dataText));

        if ($_POST) {

            if (isset($_POST['grade'])) {

                if ($this->userAge <= 9 && $this->userAge !== 0) {
                    $dataText = simplexml_load_file('data/' . $this->lang . '/lib/baby/' . $_POST['currentTitle'] . '.xml');
                } else {
                    $dataText = simplexml_load_file('data/' . $this->lang . '/lib/' . $_POST['currentTitle'] . '.xml');
                }

                echo json_encode($dataText);

                exit();

            }

            if (isset($_POST['title'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberTitle('reading_grade');

                    exit();
                }

            }

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('speed_grade');

                }

                exit();
            }
        }

        $this->set(['titles' => $titles, 'dataText' => $dataText, 'languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Управжнение "Ускоритель чтения"
     */
    public function actionReadingAccelerator()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/reading-accelerator.txt');

        // в зависимости от возраста пользователя выбираем в библиотеке все файлы с текстами
        if ($this->userAge <= 9 && $this->userAge !== 0) {
            $listFiles = glob('data/' . $this->lang . '/lib/baby/*.xml');
            $numberPart = 4;
        } else {
            $listFiles = glob('data/' . $this->lang . '/lib/*.xml');
            $numberPart = 3;
        }

        // формируем массив с названиями текстов
        foreach ($listFiles as $currentFile) {
            $tmpStr = explode('/', $currentFile);
            $tmpStr = $tmpStr[$numberPart];
            $tmpStr = explode('.', $tmpStr, -1);
            $tmpStr = $tmpStr[0];
            $titles[] = $tmpStr;
        }

        $keyFile = 0;

        if (isset($_SESSION['user']['accelerator']) && $_SESSION['user']['accelerator'] !== '') {

            $keyFile = array_search($_SESSION['user']['accelerator'], $titles);

            $keyFile++;

            if ($keyFile === count($titles)) {

                $keyFile = 0;

            }

        }

        if ($this->userAge <= 9 && $this->userAge !== 0) {
            $dataText = simplexml_load_file('data/' . $this->lang . '/lib/baby/' . $titles[$keyFile] . '.xml');
        } else {
            $dataText = simplexml_load_file('data/' . $this->lang . '/lib/' . $titles[$keyFile] . '.xml');
        }

        $dataText = json_decode(json_encode($dataText));

        if ($_POST) {

            if (isset($_POST['accelerator'])) {

                if ($this->userAge <= 9 && $this->userAge !== 0) {
                    $dataText = simplexml_load_file('data/' . $this->lang . 'lib/baby/' . $_POST['currentTitle'] . '.xml');
                } else {
                    $dataText = simplexml_load_file('data/' . $this->lang . '/lib/' . $_POST['currentTitle'] . '.xml');
                }

                echo json_encode($dataText);

                exit();


            }

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $_SESSION['user']['accelerator'] = $_POST['level'];
                    $this->rememberLevel('accelerator');

                }

                exit();
            }
        }

        $this->set(['titles' => $titles, 'dataText' => $dataText, 'languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Поиск в тексте"
     */
    public function actionTextSearch()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/text-search.txt');

        $titles = [];

        // выбираем в библиотеке все файлы с текстами
        $listFiles = glob('data/' . $this->lang . '/lib/*.xml');

        // формируем массив с названиями текстов
        foreach ($listFiles as $currentFile) {
            $tmpStr = explode('/', $currentFile);
            $tmpStr = $tmpStr['3'];
            $tmpStr = explode('.', $tmpStr, -1);
            $tmpStr = $tmpStr[0];
            $titles[] = $tmpStr;
        }

        $keyFile = 0;

        if (isset($_SESSION['user']['text_search']) && $_SESSION['user']['text_search'] !== '') {

            $keyFile = array_search($_SESSION['user']['text_search'], $titles);

            $keyFile++;

            if ($keyFile === count($titles)) {

                $keyFile = 0;

            }

        }

        $dataText = simplexml_load_file('data/' . $this->lang . '/lib/' . $titles[$keyFile] . '.xml');
        $dataText = json_decode(json_encode($dataText));


        if ($_POST) {

            if (isset($_POST['new_title'])) {

                $dataText = simplexml_load_file('data/' . $this->lang . '/lib/' . $_POST['title'] . '.xml');
                echo json_encode($dataText);

                exit();

            }

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('level_text');

                    $this->rememberTitle('text_search');

                    echo 'level = ' . $_POST['level'];

                    exit();

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['titles' => $titles, 'dataText' => $dataText, 'languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Тест Струпа"
     */
    public function actionStroop() {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/stroop.txt');

        $arrColors = [];

        if (($handle = fopen("data/" . $this->lang . "/colors.csv", "r")) !== false) {
            while (($data = fgetcsv($handle)) !== false) {
                    $arrColors[] = $data[0];
            }
            fclose($handle);
        }

        if ($_POST) {

            if (isset($_POST['colors'])) {

                echo implode(',', $arrColors);
                exit();

            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * Упражнение "Цепь слов"
     */
    public function actionChainWords()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/chain-words.txt');

        if ($_POST) {

            if (isset($_POST['words'])) {

                $words = file_get_contents('data/' . $this->lang . '/chain-words.txt');

                echo $words;

                exit();

            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);
    }

    /**
     * Упражнение "Запомни слова"
     */
    public function actionRememberWords()
    {

        $infoPractice = file_get_contents("data/" . $this->lang . '/info/remember-words.txt');

        if ($_POST) {

            if (isset($_POST['words'])) {

                $words = file_get_contents('data/' . $this->lang . '/words.txt');
                $words = explode(',', $words);

                echo json_encode($words);

                exit();

            }

            if (isset($_POST['level'])) {

                if ($_SESSION['user']['role'] == 'pupil') {

                    $this->rememberLevel('remember_words');

                    echo 'level = ' . $_POST['level'];

                } else {

                    echo true;

                }

                exit();
            }
        }

        $this->set(['languageConstants' => $this->languageConstants, 'infoPractice' => $infoPractice]);

    }

    /**
     * @param $practiceType
     */
    protected function rememberLevel($practiceType) {

        $user = new User;

        $_SESSION['user'][$practiceType] = $_POST['level'];

        $user->updateDataPracticeUser($_SESSION['user']['id'], $practiceType, $_POST['level']);

    }

    /**
     *
     * @param $practiceType
     */
    protected function rememberTitle($practiceType) {

        $user = new User;

        $_SESSION['user'][$practiceType] = $_POST['title'];

        $user->updateDataPracticeUser($_SESSION['user']['id'], $practiceType, $_POST['title']);

    }

}
