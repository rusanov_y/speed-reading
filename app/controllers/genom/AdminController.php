<?php
/**
 *
 */

namespace app\controllers\genom;

use yurus\core\base\Controller;

class AdminController extends Controller {

    public $layout = 'admin';

    public $lang = 'ru';

    public $listLang = [
        'ru' => ['русский', 'русском'],
        'kz' => ['казахский', 'казахском'],
        'ua' => ['украинский', 'украинском']
    ];

    public function __construct($route) {

        parent::__construct($route);

        if ( !isset($_SESSION['user']) || ( isset($_SESSION['user']) && $_SESSION['user']['adm_role'] !== 'teacher' ) ) {

            redirect('/');

        }

    }

}