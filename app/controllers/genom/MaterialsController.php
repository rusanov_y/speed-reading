<?php


namespace app\controllers\genom;


class MaterialsController extends AdminController
{

    public function actionIndex() {}

    public function actionTexts() {

        /**
         * Получение списка файлов с текстами
         *
         * @param $age - возрастная категория: 7 - для возраста 7-9 лет; любая другая цифра для текстов старших возрастов
         * @param $numberPart - какую часть адреса отсекать
         * @return array - массив с названиями файлов
         */
        function getNameFiles($age, $numberPart) {

            if ($age === 7) {
                $listFiles = glob('data/' . $_GET['lang'] . '/lib/baby/*.xml');
            } else {
                $listFiles = glob('data/' . $_GET['lang'] . '/lib/*.xml');
            }

            // формируем массив с названиями текстов
            foreach ($listFiles as $currentFile) {
                $tmpStr = explode('/', $currentFile);
                $tmpStr = $tmpStr[$numberPart];
                $tmpStr = explode('.', $tmpStr, -1);
                $tmpStr = $tmpStr[0];
                $titles[] = $tmpStr;
            }

            return $titles;

        }

        // в зависимости от возраста пользователя выбираем в библиотеке все файлы с текстами
        $titles7 = getNameFiles(7, 4);
        $titles = getNameFiles(9, 3);

        $languages = $this->listLang;

        $this->set(compact('titles', 'titles7', 'languages'));

    }

    public function actionChainWords() {

        // Получение файла со словами
        $listDir = scandir('data/');
        $listDir = array_slice($listDir, 2);
        $arrWords = [];
        foreach ($listDir as $value) {
            $tmp = file_get_contents('data/' . $value . '/chain-words.txt');
            $tmp = explode(',', $tmp);
            $arrWords[$value] = $tmp;
        }

        $languages = $this->listLang;

        $this->set(compact('languages', 'arrWords'));

    }

    public function actionRunningWords() {

        // Получение файла со словами
        $listDir = scandir('data/');
        $listDir = array_slice($listDir, 2);
        $arrWords = [];
        foreach ($listDir as $value) {
            $tmp = file_get_contents('data/' . $value . '/words-run.txt');
            $tmp = explode(',', $tmp);
            $arrWords[$value] = $tmp;
        }

        $languages = $this->listLang;

        $this->set(compact('languages', 'arrWords'));

    }

    public function actionWordSearch() {

        // Получение файла со словами
        $listDir = scandir('data/');
        $listDir = array_slice($listDir, 2);
        $arrWords = [];
        foreach ($listDir as $value) {
            $tmp = file_get_contents('data/' . $value . '/word-search.txt');
            $tmp = explode(',', $tmp);
            $arrWords[$value] = $tmp;
        }

        $languages = $this->listLang;
        $this->set(compact('languages', 'arrWords'));

    }

    public function actionRememberWords() {

        //Получение файла со словами
        $listDir = scandir('data/');
        $listDir = array_slice($listDir, 2);
        $arrWords = [];
        foreach ($listDir as $value) {
            $tmp = file_get_contents('data/' . $value . '/words.txt');
            $tmp = explode(',', $tmp);
            $arrWords[$value] = $tmp;
        }

        $languages = $this->listLang;

        $this->set(compact('languages', 'arrWords'));

    }

    public function actionWordsTwins() {

        //Получение списка слов
        $listDir = scandir('data/');
        $listDir = array_slice($listDir, 2);
        $arrWords = [];

        foreach ($listDir as $value) {
            $handle = fopen('data/' . $value . '/word-twins.csv', 'r');
            $arrWords[$value] = fgetcsv($handle, 0, ',');
            fclose($handle);
        }

        $languages = $this->listLang;

        $this->set(compact('languages', 'arrWords'));

    }

}