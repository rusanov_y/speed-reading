<?php
/**
 *
 */

namespace app\controllers\genom;


use app\models\Admin;
use app\models\Homework;
use app\models\User;
use yurus\libs\Pagination;

class MainController extends AdminController {

    /**
     * Формирование главной страницы админки
     *
     * В зависимости от админ роли пользователя формируются данные для передачи в вид
     *
     * @var $countAdmins integer - количество администраторов
     * @var $countActiveAdmins integer - количество активированных администраторов
     * @var $coachsCenter integer - количество преподавателей genom
     * @var $directorsCenter integer - количество директоров других центров
     * @var $otherCenter integer - количество преподавателей не genom
     * @var $countUsers integer - количество учеников
     * @var $activeUsers integer - количество активированных учеников
     * @var $arrIdCoach string - строка с id преподавателей центра (не genom), для выборки учащихся всех преподавателей центра
     */
    public function actionIndex() {

        $countAdmins = 0;
        $countActiveAdmins = 0;
        $coachsCenter = 0;
        $directorsCenter = 0;
        $otherCenter = 0;
        $countUsers = 0;
        $activeUsers = 0;

        // Формирование данных для вывода администратору (директору genom)
        // - количество преподавателей genom
        // - количество директоров других центров
        // - количество преподавателей не genom
        // - общее количество учеников
        // - количество активированных учеников
        if ( $_SESSION['user']['role'] === '1' ) {

            $admins = new Admin();
            $users = new User();

            $dataAdmins = $admins->allUsers();

            $countAdmins = count($dataAdmins);

            foreach ( $dataAdmins as $item ) {

                if ( $item['active'] === '1' ) $countActiveAdmins++;
                if ( $item['role'] === '2' ) $coachsCenter++;
                if ( $item['role'] === '3' ) $directorsCenter++;
                if ( $item['role'] === '4' ) $otherCenter++;

            }

            $dataUsers = $users->allUsers();

            $countUsers = count($dataUsers);

            foreach ( $dataUsers as $item ) {

                if ( $item['active'] === '1' ) $activeUsers++;

            }


        }

        // Формирование данных для вывода преподавателям как genom, так и с других центров
        // - количество учеников этого преподавателя
        // - количество активированных учеников
        if ( $_SESSION['user']['role'] === '2' || $_SESSION['user']['role'] === '4' ) {

            $users = new User();

            $dataUsers = $users->allUsers("`id_teacher` = '" . $_SESSION['user']['id'] . "'");

            $countUsers = count($dataUsers);

            foreach ( $dataUsers as $item ) {

                if ( $item['active'] === '1' ) $activeUsers++;

            }

        }

        if ( $_SESSION['user']['role'] === '4' && !isset($_SESSION['user']['parent_id']) ) {

            if ( $_SESSION['user']['count_users'] <= $countUsers ) {

                $_SESSION['user']['no_add_users'] = 1;

            }


            if ( $_SESSION['user']['date_end'] < time() ) {

                $_SESSION['user']['no_entry'] = 1;

            }

        }

        // Формирование данных для вывода директору центра (не genom)
        // - количество преподавателей центра
        // - общее количество учеников этого центра
        // - количество активированных учеников
        if ( $_SESSION['user']['role'] === '3' ) {

            $admins = new Admin();
            $users = new User();

            $dataAdmins = $admins->allUsers("`parent_id` = '" . $_SESSION['user']['id'] . "'");

            $otherCenter = count($dataAdmins);

            $arrIdCoach = "";

            foreach ( $dataAdmins as $item ) {

                if ( $item['active'] === '1' ) $countActiveAdmins++;
                $arrIdCoach .= $item['id'] . ",";

            }

            $str = $_SESSION['user']['id'] . "," . $arrIdCoach;

            $str = rtrim($str, ',');

            $where = "`id_teacher` IN (" . $str . ")";

            $dataUsers = $users->allUsers($where);

            $countUsers = count($dataUsers);

            foreach ( $dataUsers as $item ) {

                if ( $item['active'] === '1' ) $activeUsers++;

            }

            if ( $_SESSION['user']['count_users'] <= $countUsers ) {

                $_SESSION['user']['no_add_users'] = 1;

            }


            if ( $_SESSION['user']['date_end'] < time() ) {

                $_SESSION['user']['no_entry'] = 1;

            }


        }

        $this->set(compact('countAdmins', 'countActiveAdmins', 'coachsCenter', 'directorsCenter', 'otherCenter', 'countUsers', 'activeUsers' ));

    }

    public function actionUsers() {

        $arrCoach = [];
        $pagination = null;

        // получение данных администраторов
        if ( isset($_GET['role']) && $_GET['role'] === "admin" ) {

            $admins = new Admin();

            $fields = "`id`, `name`, `login`, `active`";
            $table = 'admin';
            $users = $admins->find($fields, $table, "`role` = 1");

        }

        // получение данных прeподавателей genom
        if ( isset($_GET['role']) && $_GET['role'] === "genom" ) {

            $admins = new Admin();

            $fields = "`id`, `name`, `login`, `active`";
            $table = 'admin';
            $users = $admins->find($fields, $table, "`role` = 2");

        }

        // получение данных директоров других центров
        if ( isset($_GET['role']) && $_GET['role'] === "director" ) {

            $admins = new Admin();

            $fields = "`id`, `name`, `login`, `active`, `count_users`, `date_end`";
            $table = 'admin';
            $where = "`role` = 3";
            if ( isset($_GET['search']) ) $where .= " AND `name` LIKE '%" . $_GET['search'] . "%' OR `login` LIKE '%" . $_GET['search'] . "%'";

            $users = $admins->find($fields, $table, $where);

        }

        // получение данных преподавателей не genom
        if ( isset($_GET['role']) && $_GET['role'] === "coach" ) {

            $admins = new Admin();

            $fields = "`id`, `name`, `login`, `active`, `parent_id`, `count_users`, `date_end`";
            $table = 'admin';
            $where = "`role` = 4";
            if ( isset($_GET['search']) ) $where .= " AND `name` LIKE '%" . $_GET['search'] . "%' OR `login` LIKE '%" . $_GET['search'] . "%'";

            if ( $_SESSION['user']['role'] === '3' ) {
                $where = "`role` = 4 AND `parent_id` = '" . $_SESSION['user']['id'] . "'";
            }

            $totalCoach = $admins->find("COUNT(`id`)", $table, $where);
            $totalCoach = $totalCoach[0]['COUNT(`id`)'];
            $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
            $perpage = 25;

            $pagination = new Pagination($currentPage, $perpage, $totalCoach);
            $startPage = $pagination->getStart();

            $where .= " LIMIT $startPage, $perpage";

            $users = $admins->find($fields, $table, $where);

        }

        // получение данных учеников
        if ( isset($_GET['role']) && $_GET['role'] === "user" ) {

            $modelAdmin = new Admin();
            $modelUser = new User();

            $tableCoach = 'admin';
            $table = 'users';

            // создание списка преподавателей для вывода имени в таблице
            $fieldsCoach = "`id`, `name`";
            $arrCoach = $modelAdmin->findId($fieldsCoach, $tableCoach);

            // установка полей для вывода информации о пользователе
            $fields = "`id`, `name`, `login`, `active`, `id_teacher`, `create_at`";

            $where = 1;

            // условие для вывода всех учеников
            if ( $_SESSION['user']['role'] === '1' ) {

                if ( isset($_GET['search']) ) $where = "`name` LIKE '%" . $_GET['search'] . "%' OR `login` LIKE '%" . $_GET['search'] . "%'";

                $totalUser = $modelUser->find("COUNT(`id`)", $table, $where);
                $totalUser = $totalUser[0]['COUNT(`id`)'];
                $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                $perpage = 25;

                $pagination = new Pagination($currentPage, $perpage, $totalUser);
                $startPage = $pagination->getStart();

                $where .= " LIMIT $startPage, $perpage";

            }

            // условие для вывода учеников преподавателя genom
            if ( $_SESSION['user']['role'] === '2' ) {

                $where = "`id_teacher` = '" . $_SESSION['user']['id'] . "'";
                if ( isset($_GET['search']) ) $where .= " AND (`name` LIKE '%" . $_GET['search'] . "%' OR `login` LIKE '%" . $_GET['search'] . "%')";
                $totalUser = $modelUser->find("COUNT(`id`)", $table, $where);
                $totalUser = $totalUser[0]['COUNT(`id`)'];
                $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                $perpage = 25;

                $pagination = new Pagination($currentPage, $perpage, $totalUser);
                $startPage = $pagination->getStart();

                $where .= " LIMIT $startPage, $perpage";

            }

            // условие для вывода учеников преподавателя другого учебного центра
            if ( $_SESSION['user']['role'] === '4' && isset($_SESSION['user']['parent_id']) ) {

                // выборка всех преподавателей данного учебного центра
                $fieldsCoach = "`id`";
                $where = "`parent_id` = '" . $_SESSION['user']['parent_id'] . "'";

                $arr = $modelAdmin->find($fieldsCoach, $tableCoach, $where);

                $strC = "'" . $_SESSION['user']['parent_id'] . "'";
                foreach ($arr as $item) {
                    $strC .= ", '" . $item['id'] . "'";
                }

                // подсчёт всех учеников данного учебного центра
                $fieldsCoach = "COUNT(`id`) AS `count_users`";

                $where = "`id_teacher` IN (" . $strC . ")";

                $arr = $modelAdmin->find($fieldsCoach, $table, $where);

                // допустимое количество учеников для центра
                $countUsersCenter = $modelAdmin->find('count_users', $tableCoach, '`id` = ?', array($_SESSION['user']['parent_id']) );

                if ( $arr[0]['count_users'] >= $countUsersCenter[0]['count_users'] ) {

                    $_SESSION['user']['no_add_users'] = 1;

                }

                $where = "`id_teacher` = '" . $_SESSION['user']['id'] . "'";

                $totalUser = $modelUser->find("COUNT(`id`)", $table, $where);
                $totalUser = $totalUser[0]['COUNT(`id`)'];
                $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                $perpage = 25;

                $pagination = new Pagination($currentPage, $perpage, $totalUser);
                $startPage = $pagination->getStart();

                if ( isset($_GET['search']) ) $where .= " AND (`name` LIKE '%" . $_GET['search'] . "%' OR `login` LIKE '%" . $_GET['search'] . "%')";
                $where .= " LIMIT $startPage, $perpage";

            }

            // условие для вывода учеников преподавателя (не genom)
            if ( $_SESSION['user']['role'] === '4' && !isset($_SESSION['user']['parent_id']) ) {

                $where = "`id_teacher` = '" . $_SESSION['user']['id'] . "'";
                if ( isset($_GET['search']) ) $where .= " AND (`name` LIKE '%" . $_GET['search'] . "%' OR `login` LIKE '%" . $_GET['search'] . "%')";

                $totalCoach = $modelUser->find("COUNT(`id`)", $table, $where);
                $totalCoach = $totalCoach[0]['COUNT(`id`)'];
                $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                $perpage = 25;

                $pagination = new Pagination($currentPage, $perpage, $totalCoach);
                $startPage = $pagination->getStart();

                $where .= " LIMIT $startPage, $perpage";

            }

            // условие для вывода учеников учебного центра (не genom)
            if ( $_SESSION['user']['role'] === '3' ) {

                $fieldsCoach = "`id`";
                $where = "`parent_id` = '" . $_SESSION['user']['id'] . "'";
                $arr = $modelAdmin->find($fieldsCoach, $tableCoach, $where);

                $strC = "'" . $_SESSION['user']['id'] . "'";
                foreach ($arr as $item) {
                    $strC .= ", '" . $item['id'] . "'";
                }

                $where = "`id_teacher` IN (" . $strC . ")";

                $totalCoach = $modelUser->find("COUNT(`id`)", $table, $where);
                $totalCoach = $totalCoach[0]['COUNT(`id`)'];
                $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                $perpage = 25;

                $pagination = new Pagination($currentPage, $perpage, $totalCoach);
                $startPage = $pagination->getStart();

                if ( isset($_GET['search']) ) $where .= " AND (`name` LIKE '%" . $_GET['search'] . "%' OR `login` LIKE '%" . $_GET['search'] . "%')";
                $where .= " LIMIT $startPage, $perpage";

            }

            $users = $modelUser->find($fields, $table, $where);

        }

        $this->set(compact('users', 'arrCoach', 'pagination'));

    }

    /**
     * Выход авторизованного пользователя из тренажёра
     *
     * При нажатии на кнопку "Выход" удаляются данные пользователя из сессии
     * Пользователя редиректят на стартовую страницу тренажёра.
     */
    public function actionLogout() {

        if ( isset($_SESSION['user']) ) unset($_SESSION['user']);
        redirect('/');

    }

    public function actionHomework() {

        if ( $_POST ) {

            $model = new Homework();

            if ( $_POST['do'] === 'info' ) :

                $data = $model->readHomework($_POST['id']);

                if ( !empty($data) ) {

                    echo (json_encode($data));

                } else {

                    echo "empty";
                }

                exit();

            endif;

        }

        exit();

    }

}