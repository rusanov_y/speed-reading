<?php


namespace app\controllers\genom;


class WordsTwinsController extends AdminController
{

    public function actionEdit() {

        $lang = $_GET['lang'];

        $handle = fopen('data/' . $lang . '/word-twins.csv', 'r');
        $words = fgetcsv($handle, 0, ',');
        fclose($handle);

        $currentLang = $this->listLang[$lang];

        if ($_POST) {

            $lang = $_POST['lang'];
            $words = $_POST['words'];
            $words = explode(',', $words);

            foreach ( $words as $key => $value) {
                $str = trim($value);
                if ($str !== '') {
                    $tmpArray[] = $str;
                }
            }

            array_multisort(array_map('strlen', $tmpArray), $tmpArray);
            $result = array_unique($tmpArray);

            $handle = fopen('data/' . $lang . '/word-twins.csv', 'w');

            if (fputcsv($handle, $result, ',')) {
                fclose($handle);
                redirect('/genom/materials/words-twins');
            } else {
                fclose($handle);
                $words = $_POST['words'];
                $currentLang = $this->listLang[$lang];
                $error = 'При сохранении файла произошла ошибка. Попробуйте позже.';
                $this->set(compact('words', 'currentLang', 'error'));
                exit();
            }

        }

        $this->set(compact('words', 'currentLang'));

    }

}