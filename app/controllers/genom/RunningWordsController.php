<?php


namespace app\controllers\genom;


class RunningWordsController extends AdminController
{

    public function actionEdit() {

        $lang = $_GET['lang'];

        $words = file_get_contents('data/' . $lang . '/words-run.txt');

        $currentLang = $this->listLang[$lang];

        if ($_POST) {

            $lang = $_POST['lang'];
            $words = $_POST['words'];
            $words = explode(',', $words);

            foreach ( $words as $key => $value) {
                $str = trim($value);
                if ($str !== '') {
                    $tmpArray[] = $str;
                }
            }

            array_multisort(array_map('strlen', $tmpArray), $tmpArray);
            $result = array_unique($tmpArray);
            $result = implode(',', $result);

            if (file_put_contents('data/' . $lang . '/words-run.txt', $result)) {
                redirect('/genom/materials/running-words');
            } else {
                $words = $_POST['words'];
                $currentLang = $this->listLang[$lang];
                $error = 'При сохранении файла произошла ошибка. Попробуйте позже.';
                $this->set(compact('words', 'currentLang', 'error'));
            }

        }

        $this->set(compact('words', 'currentLang'));

    }

}