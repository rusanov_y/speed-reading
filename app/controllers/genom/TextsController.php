<?php


namespace app\controllers\genom;


class TextsController extends AdminController
{

    public function actionEdit() {

        if (!isset($_GET['title'])) redirect();

        if (isset($_GET['age']) && $_GET['age'] === "7") {
            $dataText = simplexml_load_file('data/' . $_GET['lang'] . '/lib/baby/' . $_GET['title'] . '.xml');
        } else {
            $dataText = simplexml_load_file('data/' . $_GET['lang'] . '/lib/' . $_GET['title'] . '.xml');
        }

        $title = $dataText->title;
        $countWords = $dataText->words;
        $contentText = implode(PHP_EOL, (array)$dataText->text->p);
        $questions = $dataText->questions->question_body;
        $arrTmp = [];
        foreach ($questions as $item) {
            $item = (array)$item;
            $arrTmp[] = $item['question'];
            foreach ($item['answers']->answer as $value) {
                $value = '- ' . $value;
                $arrTmp[] = $value;
            }
        }
        $questions = implode(PHP_EOL, $arrTmp);
        $this->set(compact('title', 'countWords', 'contentText', 'questions'));

        if ($_POST) {

            $title = $_POST['title'];
            $countWords = $_POST['count-words'];
            $contentText = $_POST['content-text'];
            $questions = $_POST['questions'];

            $result = $this->saveText();

            switch ($result) {
                case 'fields empty':
                    $error = 'Не все поля заполнены';
                    $this->set(compact('title', 'countWords', 'contentText', 'questions', 'error'));
                    break;
                case "answer error":
                    $error = 'Вы ввели меньше 5 вопросов';
                    $this->set(compact('title', 'countWords', 'contentText', 'questions', 'error'));
                    break;
                case 'error write':
                    $error = 'При сохранении файла произошла ошибка. Попробуйте позже.';
                    $this->set(compact('title', 'countWords', 'contentText', 'questions', 'error'));
                    break;
                case "success":
                    redirect('/genom/materials/texts?lang=' . $_GET['lang'] );
            }

        }

    }

    public function actionCreate() {

        if ($_POST) {

            $result = $this->saveText();

            switch ($result) {
                case 'fields empty':
                    $this->set([$_POST, 'error' => 'Не все поля заполнены' ]);
                    break;
                case "answer error":
                    $this->set([$_POST, 'error' => 'Вы ввели меньше 5 вопросов' ]);
                    break;
                case 'error write':
                    $this->set([$_POST, 'error' => 'При сохранении файла произошла ошибка. Попробуйте позже.' ]);
                    break;
                case "success":
                    $this->set(['success' => true] );
            }

        }

    }

    public function actionDelete() {

        if (isset($_GET['title'])) {
            if (isset($_GET['age']) && $_GET['age'] == "7") {
                unlink('data/' . $_GET['lang'] . '/lib/baby/' . $_GET['title'] . '.xml');
            } else {
                unlink('data/' . $_GET['lang'] . '/lib/' . $_GET['title'] . '.xml');
            }
        }

        redirect('/genom/materials/texts?lang=' . $_GET['lang']);
    }

    protected function saveText() {

        $lang = $_POST['lang'];
        $age = $_POST['age'];
        $title = $_POST['title'];
        $countWords = $_POST['count-words'];
        $arrContent = explode(PHP_EOL, $_POST['content-text']);
        $tmpQuestions = explode(PHP_EOL, $_POST['questions']);

        $i = 0;
        foreach ($tmpQuestions as $key => $item) {
            $item = trim($item);

            if ($item === '') {
                break;
            }

            if ( $item{0} === '-') {
                $item = substr($item, 1);
                $item = trim($item);
                $arrAnswer[$i - 1][] = $item;
            } else {
                $arrQuestions[] = $item;
                $i++;
            }

        }

        if ($title == '' || $countWords == '') {
            return "fields empty";
        }

        if (count($arrQuestions) < 5) {
            return 'answer error';
        }

        // создание и запись xml файла
        $xml = new\XMLWriter();

        if ($age === '7') {

            $writeFile = $xml->openUri('data/' . $lang . '/lib/baby/' . $title . '.xml');

        } else {

            $writeFile = $xml->openUri('data/' . $lang . '/lib/' . $title . '.xml');

        }

        $xml->startDocument('1.0', 'utf-8');

        $xml->startElement('body');
        // write title
        $xml->startElement('title');
        $xml->text($title);
        $xml->endElement();
        // write count words
        $xml->startElement('words');
        $xml->text($countWords);
        $xml->endElement();
        // write content
        $xml->startElement('text');
        foreach ($arrContent as $value) {
            // write paragraph
            $xml->startElement('p');
            $xml->text(trim($value));
            $xml->endElement();
        }
        $xml->endElement();
        // block questions
        $xml->startElement('questions');
        foreach ($arrQuestions as $key => $item) {
            $xml->startElement('question_body');
            $xml->startElement('question');
            $xml->text($item);
            $xml->endElement();
            $xml->startElement('answers');
            foreach ($arrAnswer[$key] as $answer) {
                $xml->startElement('answer');
                $xml->text($answer);
                $xml->endElement();
            }
            $xml->endElement();
            $xml->endElement();

        }
        $xml->endElement();
        // close body
        $xml->endElement();
        $xml->endDocument();

        if (!$writeFile) {
            return "error write";
        } else {
            unset($_POST);
            return 'success';
        }
    }

}