<?php
/**
 *
 */

namespace app\controllers\genom;


use app\models\User;
use app\models\Admin;

class UserController extends AdminController {

    public function actionView() {

        $role = $_GET['role'];
        $table = "";
        $model = "";

        switch ($role) {
            case 'admin':
            case 'genom':
            case 'director':
            case 'coach':
                $table = "admin";
                $model = new Admin();
                break;
            case 'user':
                $table = "users";
                $model = new User();
        }

        $value = array($_GET['id']);

        $user = $model->findOne($table, '`id` = ?', $value);

        $user = $user[0];

        unset($user['pass']);
        unset($user['hash']);
        unset($user['balance']);

        $labels = $model->attributeLabels;

        $this->set(compact('user', 'labels'));

    }

    public function actionEdit() {

        if ( !empty($_POST) ) :

            $data = $_POST;

            if ( isset($data['role']) ) {

                $model = new Admin();

            } else {

                $model = new User();

            }

            $model->loadAttributes($data);

            /*if ( !$model->validate($model->attributes) ) {

                $model->getErrors();
                redirect();

            }*/

            if ( !empty($model->attributes['pass']) ) {

                $model->attributes['pass'] = password_hash($model->attributes['pass'], PASSWORD_DEFAULT);

            }

            $model->attributes['active'] = ( $model->attributes['active'] === 'on' ) ? 1 : '0';

            if ( !empty($model->attributes['date_end']) ) {

                $model->attributes['date_end'] = ( $model->attributes['date_end'] ) ? strtotime($model->attributes['date_end']): '';

            }

            $res = $model->updateUser($data['id']);

            if ( $res ) {

                if ($_GET['role'] = 'director') {
                    $model->updateDate($data['id'], $model->attributes['date_end']);
                }

                $url = $_SERVER['HTTP_REFERER'];
                $url = str_replace('/edit', 's', $url);

                redirect($url);

            } else {

                $_SESSION['errors'] = 'Произошёл сбой, попробуйте ещё раз.';
                redirect();

            }

        endif;

        $role = $_GET['role'];
        $table = "";
        $model = "";
        $coach = [];
        $arrCoach = [];

        switch ($role) {
            case 'admin':
            case 'genom':
            case 'director':
            case 'coach':
                $table = "admin";
                $model = new Admin();
                break;
            case 'user':
                $table = "users";
                $model = new User();
        }

        $value = array($_GET['id']);

        $user = $model->findOne($table, '`id` = ?', $value);

        $user = $user[0];

        if ( $_SESSION['user']['role'] === '1' ) {

            $modelCoach = new Admin();

            $value = "`id`, `name`";

            $arrCoach = $modelCoach->findId($value, 'admin' );

        }

        if ( $_SESSION['user']['role'] === '3' ) {

            $modelCoach = new Admin();

            $fields = "`id`, `name`";
            $where = "`parent_id` = ?";
            $value = array($_SESSION['user']['id']);
            $arrCoach = $modelCoach->findId($fields, 'admin', $where, $value );

        }

        if ( $arrCoach ) {

            foreach ($arrCoach as $key => $item) {

                $coach[$key] = $item['name'];

            }

        }

        $this->set(compact('user', 'role', 'coach'));

    }

    /**
     * Добавление нового пользователя
     */
    public function actionCreate() {

        $coach = "";

        // если пришли POST данные
        if ( !empty($_POST) ) :

            // проверяем какая роль будет у пользователя
            // если ученик, то создаём объект ученика
            // если преподаватель, то создаём объект преподавателя
            $modelUser = ( $_POST['role'] != 10 ) ? new Admin() : new User();
            $data = $_POST;

            $modelUser->loadAttributes($data);

            if ( !$modelUser->validate($modelUser->attributes) ) {

                $modelUser->getErrors();
                redirect();

            }

            $unique = $modelUser->checkUnique();

            if ( !$unique ) {

                $_SESSION['errors'] = 'Такой логин уже занят';
                redirect();

            }

            $modelUser->attributes['pass'] = password_hash($modelUser->attributes['pass'], PASSWORD_DEFAULT);
            $modelUser->attributes['active'] = ( $modelUser->attributes['active'] === 'on' ) ? 1: '';

            if ( $_POST['role'] != 10 ) {

                if ( $_SESSION['user']['role'] === '3' ) {

                    $modelUser->attributes['date_end'] = $_SESSION['user']['date_end'];

                } else {

                    $modelUser->attributes['date_end'] = ($modelUser->attributes['date_end']) ? strtotime($modelUser->attributes['date_end']) : '';

                }
            }

            $res = $modelUser->saveUser();

            if ( $res ) {

                $url = $_SERVER['HTTP_REFERER'];
                $url = str_replace('/create', 's', $url);

                redirect($url);

            } else {

                $_SESSION['errors'] = 'Произошёл сбой, попробуйте ещё раз.';
                redirect();

            }

        endif;

        if ( $_SESSION['user']['role'] === '1' && ($_GET['role'] === 'coach' || $_GET['role'] === 'user') ) {

            $modelCoach = new Admin();

            $value = "`id`, `name`";
            $coach = $modelCoach->findId($value, 'admin' );

        }

        if ( $_SESSION['user']['role'] === '3' ) {

            $modelCoach = new Admin();

            $fields = "`id`, `name`";
            $where = "`parent_id` = ?";
            $value = array($_SESSION['user']['id']);
            $coach = $modelCoach->findId($fields, 'admin', $where, $value );

        }

        $this->set( compact('coach') );

    }

    public function actionDelete() {

        $id = $_GET['id'];
        $role = $_GET['role'];
        $tableName = "";
        $res = "";

        switch ($role) {
            case 'admin':
            case 'genom':
            case 'director':
            case 'coach': $tableName = "admin";
            break;
            case 'user': $tableName = "users";
        }

        $where = "`id` = ?";
        $value = array($id);

        if ( $role === "user" ) {

            $model = new User();

            $res = $model->delete($tableName, $where, $value);

        }

        if ( $role === "admin" ) {

            $model = new Admin();

            $countAdmin = $model->find("COUNT(`id`) AS `count_admin`", $tableName, "`admin_role` = 1");

            if ( $countAdmin[0]['count_admin'] === "1" ) {

                $_SESSION["user"]['errors'] = "Данного пользователя удалять нельзя.";
                redirect('/genom/users?role=admin');

            }

            $res = $model->delete($tableName, $where, $value);

        }

        if ( $role === "genom" || $role === "coach" || $role === "director" ) {

            $model = new Admin();

            $res = $model->delete($tableName, $where, $value);


        }

        if ( !$res ) {

            $_SESSION["user"]['errors'] = "При удалении произошла ошибка. Попробуйте позже.";

        }

        redirect('/genom/users?role=' . $role);

    }

}