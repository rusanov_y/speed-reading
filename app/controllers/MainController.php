<?php
/**
 *
 */

namespace app\controllers;


use app\models\Admin;
use app\models\User;

class MainController extends AppController {

    /**
     * Начальная страница тренажёра
     */
    public function actionIndex() {

        // проверяем ставил ранее пользователь галочку "Запомнить меня"
        // наличием COOKIE
        if ( isset($_COOKIE['_user_rem']) && isset($_COOKIE['_user_role']) ) {

            $user = "";

            switch ($_COOKIE['_user_role']) {
                case 'pupil': $user = new User();
                    break;
                case 'teacher': $user = new Admin();
            }

            if ( $user->rememberLogin() ) {

                redirect('/choice' );

            }
        }

        // если пользователь в рамках текущей сессии уже входил на сайт,
        // то сразу его перекидываем на страницу выбора "Тренировка" или "Упражнение"
        if ( isset($_SESSION['user']) ) {

            redirect('/choice' );

        }

        $this->set(['languageConstants' => $this->languageConstants]);

    }

    public function actionFaq() {}

    public function actionChoice() {

        if ($_POST) {

            if (isset($_POST['age'])) {

                echo $this->userAge;

            }

            exit();

        }

        $this->set(['languageConstants' => $this->languageConstants]);

    }

    public function actionLanguage() {

        if ($_POST) {

            $this->lang = $_POST['lang'];
            $_SESSION['lang'] = $_POST['lang'];
            echo $_POST['lang'];
            exit();

        }


    }

}