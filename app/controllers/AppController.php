<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 22.07.19
 * Time: 11:39
 */

namespace app\controllers;

use yurus\core\base\Controller;

class AppController extends Controller {

    public $userAge;
    public $lang;
    public $languageConstants;

    protected function getUserAge() {

        if (isset($_SESSION['user'])) {
            if (isset($_SESSION['user']['birthday']) && $_SESSION['user']['birthday']) {

                $nowDate = new \DateTime();
                $userBirthday = new \DateTime($_SESSION['user']['birthday']);
                $this->userAge = $nowDate->diff($userBirthday)->y;


            } else {

                $this->userAge = 0;

            }

        } else {

            $this->userAge = 0;

        }

        return $this->userAge;

    }

    public function __construct($route)
    {

        parent::__construct($route);
        $this->getUserAge();
        $this->lang = $this->getLanguage();
        $this->languageConstants = parse_ini_file('data/' . $this->lang . '/language.ini');

    }

    protected function getLanguage() {

        if (isset($_COOKIE['lang']) && !isset($_SESSION['lang'])) {

            $lang = $_COOKIE['lang'];
            $_SESSION['lang'] = $_COOKIE['lang'];

        } else if (isset($_SESSION['lang'])) {

            $lang = $_SESSION['lang'];
            setcookie('lang', $_SESSION['lang'], time() + (365 * 86400));

        } else {

            $lang = "ru";
            $_SESSION['lang'] = 'ru';

        }

        $this->languageConstants = parse_ini_file('data/' . $lang . '/language.ini');

        return $lang;

    }

}