<?php
/**
 * Контроллер Тренировок по скорочтению
 */

namespace app\controllers;

use DateTime;


class TrainingController extends AppController
{
    public $layout = 'training';
    public $timeTraining;

    public function __construct($route) {

        parent::__construct($route);

        if ( !isset($_SESSION['user']) ) {

            redirect('/');

        }

        if ( isset($_SESSION['user']['access']) && $_SESSION['user']['access'] === 'no' ) {

            redirect('training/index');

        }

    }

    /**
     * В зависимости от возраста пользователя выбирается список упражнений для тренировки
     * @var $arrPractice array - массив со списком упражнений для тренировок
     * @return array - весь список упражнений для тренировки
     */
    protected function searchPractice() {

        $arrPractice = [
            [
                ['SCHULTETABLE','schulte'],
                ['SEARCHLETTERS','letter-search'],
                ['SEARCHDIGITAL','digital-search'],
                ['REMEMBERNUMBER','remember-number'],
                ['RUNWORDS','running-words'],
                ['SEARCHWORDS','word-search'],
                ['TWINWORDS','words-twins'],
                ['FIELDVIEW','field-view'],
                ['CHAINWORDS','chain-words'],
                ['READINGACCELERATOR','reading-accelerator'],
                ['TEXTSEARCH','text-search'],
                ['READINGGRADE','reading-grade'],
                ['STROOPTEST', 'stroop'],
                ['REMEMBERWORDS', 'remember-words']
            ],
            [
                ['SCHULTETABLE','schulte'],
                ['SEARCHLETTERS','letter-search'],
                ['SEARCHDIGITAL','digital-search'],
                ['EVENODD','even-odd'],
                ['REMEMBERNUMBER','remember-number'],
                ['RUNWORDS','running-words'],
                ['SEARCHWORDS','word-search'],
                ['TWINWORDS','words-twins'],
                ['FIELDVIEW','field-view'],
                ['CHAINWORDS','chain-words'],
                ['READINGACCELERATOR','reading-accelerator'],
                ['TEXTSEARCH','text-search'],
                ['READINGGRADE','reading-grade'],
                ['STROOPTEST', 'stroop'],
                ['REMEMBERWORDS', 'remember-words']
            ]
        ];
        /*$arrPractice = [
            [
                ['Таблица Шульте','schulte'],
                ['Поиск букв','letter-search'],
                ['Поиск цифр','digital-search'],
                ['Запомни число','remember-number'],
                ['Бегущие слова','running-words'],
                ['Поиск слова','word-search'],
                ['Слова близнецы','words-twins'],
                ['Поле зрения','field-view'],
                ['Цепь слов','chain-words'],
                ['Ускоритель чтения','reading-accelerator'],
                ['Поиск в тексте','text-search'],
                ['Оценка чтения','reading-grade'],
                ['Тест Струпа', 'stroop'],
                ['Запомни слова', 'remember-words']
            ],
            [
                ['Таблица Шульте','schulte'],
                ['Поиск букв','letter-search'],
                ['Поиск цифр','digital-search'],
                ['Чет/нечет','even-odd'],
                ['Запомни число','remember-number'],
                ['Бегущие слова','running-words'],
                ['Поиск слова','word-search'],
                ['Слова близнецы','words-twins'],
                ['Поле зрения','field-view'],
                ['Цепь слов','chain-words'],
                ['Ускоритель чтения','reading-accelerator'],
                ['Поиск в тексте','text-search'],
                ['Оценка чтения','reading-grade'],
                ['Тест Струпа', 'stroop'],
                ['Запомни слова', 'remember-words']
            ]
        ];*/

        if ($this->userAge < 10 && $this->userAge !== 0) {

            $listPractice = $arrPractice[0];

        } else {

            $listPractice = $arrPractice[1];

        }

        return $listPractice;
    }

    /**
     * Функция, в которой в зависти от вида тренировки устанавливается
     * продолжительность упражнений и записывается в свойство timeTraining
     */
    protected function currentTraining() {

        $training = $_GET['training'];

        switch ($training) {
            case 'speedy':
                $this->timeTraining = 60000;
                break;
            case 'periodic':
                $this->timeTraining = 90000;
                break;
            case 'optimal':
            case 'intense':
            $this->timeTraining = 150000;
                break;
        }
    }

    /**
     *
     */
    protected function replaceText($arrReplace) {

        $tmpArray = [];
        foreach ($arrReplace as $key => $item) {

            $tmpArray[$key][0] = $this->languageConstants[$item[0]];
            $tmpArray[$key][1] = $item[1];

        }

        return$tmpArray;

    }

    /**
     * Начальная страница тренировок
     */
    public function actionIndex() {

        $this->set(['languageConstants' => $this->languageConstants]);

    }

    /**
     * Тренировка "Быстрая"
     *
     * получается список упражнений для данного возраста
     * перемешивается и берутся первые 5 упражнений.
     * В файл передаётся массив упражнений для тренировки.
     */
    public function actionSpeedy() {

        $listPractice = $this->searchPractice();
        $listPractice = array_slice($listPractice, 0, -3);
        shuffle($listPractice);

        $currentListPractice = array_slice($listPractice, 0, 5);

        $currentListPractice = $this->replaceText($currentListPractice);

        $this->set(['currentListPractice' => $currentListPractice, 'languageConstants' => $this->languageConstants]);

    }

    /**
     * Тренировка "Периодическая"
     *
     * получаем список упражненийдля данного возраста
     * мешаем список и выбираем первые 7 упражнений
     * в файл передаёт массив упражнений для тренировки
     */
    public function actionPeriodic() {

        $listPractice = $this->searchPractice();
        $listPractice = array_slice($listPractice, 0, -3);
        shuffle($listPractice);

        $currentListPractice = array_slice($listPractice, 0, 7);

        $currentListPractice = $this->replaceText($currentListPractice);

        $this->set(['currentListPractice' => $currentListPractice, 'languageConstants' => $this->languageConstants]);

    }

    /**
     * Тренировка "Оптимальная"
     *
     * получаем список упражненийдля данного возраста
     * перемешиваем список и выбираем первые 12 упражнений
     * в файл передаёт массив упражнений для тренировки
     */
    public function actionOptimal() {

        $listPractice = $this->searchPractice();

        shuffle($listPractice);

        $currentListPractice = array_slice($listPractice, 0, 12);

        $currentListPractice = $this->replaceText($currentListPractice);

        $this->set(['currentListPractice' => $currentListPractice, 'languageConstants' => $this->languageConstants]);

    }

    /**
     * Тренировка "Интенсивная"
     *
     * получаем список упражненийдля данного возраста
     * делаем копию списка
     * перемешиваем оба списка и выбираем 24 упражнения
     * в файл передаёт массив упражнений для тренировки
     */
    public function actionIntense() {

        $tmpPractice = $this->searchPractice();
        $tmpPractice2 = $tmpPractice;
        shuffle($tmpPractice);
        shuffle($tmpPractice2);

        $listPractice = array_merge($tmpPractice, $tmpPractice2);

        $currentListPractice = array_slice($listPractice, 0, 24);

        $currentListPractice = $this->replaceText($currentListPractice);

        $this->set(['currentListPractice' => $currentListPractice, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionRememberWords() {
        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionDigitalSearch() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionEvenOdd() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionFieldView() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionLetterSearch() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionReadingAccelerator() {

        $titles = [];

        // в зависимости от возраста пользователя выбираем в библиотеке все файлы с текстами
        if ($this->userAge <= 9 && $this->userAge !== 0) {
            $listFiles = glob('data/' . $this->lang . '/lib/baby/*.xml');
            $numberPart = 4;
        } else {
            $listFiles = glob('data/' . $this->lang . '/lib/*.xml');
            $numberPart = 3;
        }

        // формируем массив с названиями текстов
        foreach ($listFiles as $currentFile) {
            $tmpStr = explode('/', $currentFile);
            $tmpStr = $tmpStr[$numberPart];
            $tmpStr = explode('.', $tmpStr, -1);
            $tmpStr = $tmpStr[0];
            $titles[] = $tmpStr;
        }

        $keyFile = 0;

        if (isset($_SESSION['user']['accelerator']) && $_SESSION['user']['accelerator'] !== '') {

            $keyFile = array_search($_SESSION['user']['accelerator'], $titles);

            $keyFile++;

            if ($keyFile === count($titles)) {

                $keyFile = 0;

            }

        }

        if ($this->userAge <= 9 && $this->userAge !== 0) {
            $dataText = simplexml_load_file('data/' . $this->lang . '/lib/baby/' . $titles[$keyFile] . '.xml');
        } else {
            $dataText = simplexml_load_file('data/' . $this->lang . '/lib/' . $titles[$keyFile] . '.xml');
        }

        $dataText = json_decode(json_encode($dataText));

        $this->set(['dataText' => $dataText, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionReadingGrade() {

        $titles = [];

        // в зависимости от возраста пользователя выбираем в библиотеке все файлы с текстами
        if ($this->userAge <= 9 && $this->userAge !== 0) {
            $listFiles = glob('data/'. $this->lang . '/lib/baby/*.xml');
            $numberPart = 4;
        } else {
            $listFiles = glob('data/' . $this->lang . '/lib/*.xml');
            $numberPart = 3;
        }

        // формируем массив с названиями текстов
        foreach ($listFiles as $currentFile) {
            $tmpStr = explode('/', $currentFile);
            $tmpStr = $tmpStr[$numberPart];
            $tmpStr = explode('.', $tmpStr, -1);
            $tmpStr = $tmpStr[0];
            $titles[] = $tmpStr;
        }

        $keyFile = 0;

        if (isset($_SESSION['user']['reading_grade']) && $_SESSION['user']['reading_grade'] !== '') {

            $keyFile = array_search($_SESSION['user']['reading_grade'], $titles);

            $keyFile++;

            if ($keyFile === count($titles)) {

                $keyFile = 0;

            }

        }

        if ($this->userAge <= 9 && $this->userAge !== 0) {
            $dataText = simplexml_load_file('data/' . $this->lang . '/lib/baby/' . $titles[$keyFile] . '.xml');
        } else {
            $dataText = simplexml_load_file('data/' . $this->lang . '/lib/' . $titles[$keyFile] . '.xml');
        }

        $dataText = json_decode(json_encode($dataText));

        $this->set(['dataText' => $dataText, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionRememberNumber() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionChainWords() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionRunningWords() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionSchulte() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionStroop() {
        $this->set(['languageConstants' => $this->languageConstants]);
    }

    /**
     *
     */
    public function actionTextSearch() {

        $titles = [];

        // выбираем в библиотеке все файлы с текстами
        $listFiles = glob('data/' . $this->lang . '/lib/*.xml');

        // формируем массив с названиями текстов
        foreach ($listFiles as $currentFile) {
            $tmpStr = explode('/', $currentFile);
            $tmpStr = $tmpStr['3'];
            $tmpStr = explode('.', $tmpStr, -1);
            $tmpStr = $tmpStr[0];
            $titles[] = $tmpStr;
        }

        $keyFile = 0;

        if (isset($_SESSION['user']['text_search']) && $_SESSION['user']['text_search'] !== '') {

            $keyFile = array_search($_SESSION['user']['text_search'], $titles);

            $keyFile++;

            if ($keyFile === count($titles)) {

                $keyFile = 0;

            }

        }

        $dataText = simplexml_load_file('data/' . $this->lang . '/lib/' . $titles[$keyFile] . '.xml');
        $dataText = json_decode(json_encode($dataText));

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'dataText' => $dataText, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionWordSearch() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

    /**
     *
     */
    public function actionWordsTwins() {

        $this->currentTraining();
        $this->set(['timePractice' => $this->timeTraining, 'languageConstants' => $this->languageConstants]);

    }

}