<?php
/**
 *
 */

namespace app\controllers;


use app\models\Admin;
use app\models\Guest;
use app\models\User;

class UserController extends AppController {

    /**
     * Регистрация пользователя как гостя
     * (при нажатии кнопки "Попробовать")
     * на данный момент скрыта такая опция
     */
    public function actionSignup() {

        if ( !empty($_POST) ) {

            $user = new Guest();
            $data = $_POST;

            $user->loadAttributes($data);

            if ( !$user->validate($user->attributes) ) {

                $user->getErrors();
                redirect();

            }

            $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
            $user->attributes['guest_ip'] = ip2long( $_SERVER['REMOTE_ADDR'] );

            $res = $user->saveUser();

            if ( !$res ) {

                redirect();

            }

            foreach ( $user->attributes as $key => $value ) {

                if ( $key != 'password' ) $_SESSION['user'][$key] = $value;

            }

            $_SESSION['user']['role'] = 'guest';

            redirect('/main/page');

        }

        $this->set(['languageConstants' => $this->languageConstants]);

    }

    /**
     * Вход в тренажёр
     */
    public function actionLogin() {

        if ( !empty($_POST) ) {

            $user = "";

            switch ($_POST['role']) {
                case 'pupil': $user = new User();
                break;
                case 'guest': $user = new Guest();
                break;
                case 'teacher': $user = new Admin();
            }

            if ( !isset($user) ) redirect('/');

            if ( $user->login() ) {

                if ( isset($_SESSION['user']['access']) && $_SESSION['user']['access'] === 'no' ) {

                    redirect('/genom');

                }

                redirect('/choice');

            }

        }

        $this->set(['languageConstants' => $this->languageConstants]);

    }

    /**
     * Выход пользователя из тренажёра
     */
    public function actionLogout() {

        if ( isset($_SESSION['user']) ) unset($_SESSION['user']);

        if (isset($_COOKIE['_user_rem'])) {
            setcookie("_user_rem", "", time() - 3600, "/");
        }

        if (isset($_COOKIE['_user_role'])) {
            setcookie("_user_role", "", time() - 3600, "/");
        }

        redirect('/');

    }

}