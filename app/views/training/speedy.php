<?php
/**
 * Индексный файл тренировки "Быстрая"
 *
 * На каждое упражнение отводится по 1 минуте
 * После окончания упражнения, к очередному переход осуществляется по ссылке в конце упражнения
 * В SessionStorage сохряняется упражнения и порядок их следования
 *
 * @var $currentListPractice - массив с упражнениями для отработки
 * @var $languageConstants
 */

?>

<main class="training-page">
    <div class="container flex-block">
        <div class="training-index">
            <h4 class="orange-text-dark"><?= $languageConstants['PRACTICEDEXERCISES'] ?></h4>
            <ul>
                <?php foreach ($currentListPractice as $item) : ?>
                <li class="<?= $item[1] ?>"><?= $item[0] ?></li>
                <?php endforeach; ?>
            </ul>

            <a href="/training/remember-words?training=speedy&number=1" class="btn orange-bg-light"><?= $languageConstants['START'] ?></a>
        </div>
    </div>
</main>

<script>
    var tmp = JSON.stringify(<?= json_encode($currentListPractice) ?>);
    sessionStorage.setItem('speedy', tmp);
</script>