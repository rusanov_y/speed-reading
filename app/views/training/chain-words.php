<?php
/**
 * Упражнение Цепь слов
 *
 * @var $languageConstants
 */

?>

<main class="training-basic practice-page remember-words">
    <div class="setting-wrapper hidden-block">
        <p>
            <span id="setting-level">1</span>
            <span id="setting-time">7000</span>
        </p>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper remember-words flex-block">
                <div class="flex-block">
                    <div class="info-block" id="info-block"></div>
                    <ul class="control-block"></ul>
                </div>
                <ul class="tbl-wrapper" id="tbl-wrapper"></ul>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <h4><?= $languageConstants['TRAININGRESULT'] ?></h4>
                    <p class="result-practice"></p>
                    <h4>На ответы затрачено</h4>
                    <p class="time-answer"></p>
                </div>
                <div class="training-next">
                    <a href="" class="return-list"><?= $languageConstants['NEXTEXERCISE'] ?></a>
                </div>
            </div>
            <div class="current-level" id="current-level"></div>
        </div>
</main>

<script src="/js/training/basic.js"></script>
<script src="/js/training/remember-words.js"></script>
