<?php
/**
 * Упражнение Поиск букв
 *
 * @var $timePractice - integer время, отводимое на выполнение упражнения
 * @var $languageConstants
 */

?>

<main class="training-basic practice-page letter-search">
    <div class="setting-wrapper hidden-block">
        <p>
            <span id="setting-level"><?= (isset($_SESSION['user']['letter_search'])) ? $_SESSION['user']['letter_search'] : 1; ?></span>
            <span id="setting-time"><?= $timePractice; ?></span>
        </p>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper flex-block">
                <div>
                    <h3 id="currentAction"><?= $languageConstants['FIND'] ?>:</h3>
                    <p id="currentElementClick"></p>
                </div>
                <table class="tbl-wrapper" id="tbl-wrapper">
                    <tbody></tbody>
                </table>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <h4><?= $languageConstants['WORKOUTEFFICIENCY'] ?></h4>
                    <p class="result-practice"></p>
                </div>
                <div class="chart-wrapper">
                    <h4><?= $languageConstants['YOURPACEEXECUTION'] ?></h4>
                    <div class="ct-chart ct-minor-seventh"></div>
                </div>
                <div class="training-next">
                    <a href="" class="return-list"><?= $languageConstants['NEXTEXERCISE'] ?></a>
                </div>
            </div>
            <div class="current-level" id="current-level"></div>
        </div>
    </div>
</main>

<script src="/js/training/basic.js"></script>
<script src="/js/training/letter-search.js"></script>
