<?php
/**
 * Упражнение "Поиск в тексте"
 *
 * @var $timePractice - integer время, отводимое на выполнение упражнения
 * @var $dataText - данные по тексту, полученные из файла
 * @var $languageConstants
 */

$text = "";

foreach ($dataText->text->p as $value) {

    $text .= "<p>" . $value . "</p>";

}

?>

<main class="training-basic practice-page text-search">
    <div class="setting-wrapper hidden-block">
        <p>
            <span id="setting-level"><?= (isset($_SESSION['user']['digital_search'])) ? $_SESSION['user']['digital_search'] : 1; ?></span>
            <span id="setting-time"><?= $timePractice; ?></span>
            <span id="setting-title"><?= $dataText->title; ?></span>
        </p>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>

            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper text-search">
                <div>
                    <h3 id="currentAction"><?= $languageConstants['FIND'] ?>:</h3>
                    <p id="info-block"></p>
                </div>
                <div class="text-wrapper" id="text-wrapper"><div></div></div>

            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div class="training-next">
                    <a href="" class="return-list"><?= $languageConstants['NEXTEXERCISE'] ?></a>
                </div>
            </div>
            <div class="text-iso" id="text-iso"><?= $text ?></div>
        </div>
    </div>
</main>

<script src="/js/training/basic.js"></script>
<script src="/js/training/text-search.js"></script>
