<?php
/**
 * Индексный файл "Тренировка Периодическая"
 *
 * @var $currentListPractice
 * @var $languageConstants
 */

?>

<main class="training-page">
    <div class="container flex-block">
        <div class="training-index">
            <h4 class="orange-text-dark"><?= $languageConstants['PRACTICEDEXERCISES'] ?></h4>
            <ul>
                <?php foreach ($currentListPractice as $item) : ?>
                    <li class="<?= $item[1] ?>"><?= $item[0] ?></li>
                <?php endforeach; ?>
            </ul>
                        <a href="/training/<?= $currentListPractice[0][1] ?>?training=periodic&number=1" class="btn orange-bg-light"><?= $languageConstants['START'] ?></a>
        </div>
    </div>
</main>

<script>
    var tmp = JSON.stringify(<?= json_encode($currentListPractice) ?>);
    sessionStorage.setItem('periodic', tmp);
</script>
