<?php
/**
 * Упражнение Бегущие слова
 *
 * @var $languageConstants
 */

?>

<main class="training-basic practice-page stroop">
    <div class="setting-wrapper hidden-block">
        <p>
            <span id="setting-color">3</span>
        </p>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper stroop" id="practice-wrapper">
                <ul>
                    <li class="card-1" id="card-1">
                        <table class="stroop-card"><tbody></tbody></table>
                        <hr>
                        <table class="tbl-wrapper" id="btn-card1"><tbody></tbody></table>
                    </li>
                    <li class="card-2" id="card-2">
                        <table class="stroop-card"><tbody></tbody></table>
                        <hr>
                        <table class="tbl-wrapper" id="btn-card2"><tbody></tbody></table>
                    </li>
                    <li class="card-3" id="card-3">
                        <table class="stroop-card"><tbody></tbody></table>
                        <hr>
                        <table class="tbl-wrapper" id="btn-card3"><tbody></tbody></table>
                    </li>
                </ul>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <h4><?= $languageConstants['TRAININGRESULT'] ?></h4>
                    <h5>Карточка № 1</h5>
                    <p class="result-practice"></p>
                    <h5>Карточка № 2</h5>
                    <p class="result-practice"></p>
                    <h5>Карточка № 3</h5>
                    <p class="result-practice"></p>
                </div>
                <div class="training-next">
                    <a href="" class="return-list"><?= $languageConstants['NEXTEXERCISE'] ?></a>
                </div>
            </div>
            <div class="current-level" id="current-level"></div>
        </div>
    </div>
</main>

<script src="/js/training/stroop.js"></script>
