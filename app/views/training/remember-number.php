<?php
/**
 * Упражнение Запомни число
 *
 * @var $timePractice - integer время, отводимое на выполнение упражнения
 * @var $languageConstants
 */

?>

<main class="training-basic practice-page remember-number">
    <div class="setting-wrapper hidden-block">
        <p>
            <span id="setting-level"><?= (isset($_SESSION['user']['remember_number'])) ? $_SESSION['user']['remember_number'] : 1; ?></span>
            <span id="setting-time"><?= $timePractice; ?></span>
        </p>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper remember-number flex-block">
                <div class="flex-block">
                    <ul id="info-block"></ul>
                </div>
                <table class="tbl-wrapper" id="tbl-wrapper">
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                    </tr>
                    <tr>
                        <td colspan="2">0</td>
                        <td data-role="clear"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <h4><?= $languageConstants['TRAININGRESULT'] ?></h4>
                    <p class="result-practice"></p>
                </div>
                <div class="training-next">
                    <a href="" class="return-list"><?= $languageConstants['NEXTEXERCISE'] ?></a>
                </div>
            </div>
            <div class="current-level" id="current-level"></div>
        </div>
    </div>
</main>

<script src="/js/training/basic.js"></script>
<script src="/js/training/remember-number.js"></script>
