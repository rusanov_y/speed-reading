<?php
/**
 * Упражнение Бегущие слова
 *
 * @var $timePractice - integer время, отводимое на выполнение упражнения
 * @var $languageConstants
 */

?>

<main class="training-basic practice-page running-words">
    <div class="setting-wrapper hidden-block">
        <p>
            <span id="setting-level"><?= (isset($_SESSION['user']['run_words'])) ? $_SESSION['user']['run_words'] : 1; ?></span>
            <span id="setting-time"><?= $timePractice; ?></span>
        </p>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper running-words flex-block">
                <div class="flex-block" id="info-block">
                    <p data-line="1"></p>
                    <p data-line="2"></p>
                    <p data-line="3"></p>
                    <p data-line="4"></p>
                    <p data-line="5"></p>
                </div>
                <table class="tbl-wrapper" id="tbl-wrapper">
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <h4><?= $languageConstants['TRAININGRESULT'] ?></h4>
                    <p class="result-practice"></p>
                </div>
                <div class="training-next">
                    <a href="" class="return-list"><?= $languageConstants['NEXTEXERCISE'] ?></a>
                </div>
            </div>
            <div class="current-level" id="current-level"></div>
        </div>
    </div>
</main>

<script src="/js/training/basic.js"></script>
<script src="/js/training/running-words.js"></script>