<?php
/**
 * Упражнение Оценка чтения
 *
 * @var $titles
 * @var $dataText
 * @var $languageConstants
 */

$text = "";

foreach ((array)$dataText->text->p as $value) {
    $text .= "<p>$value</p>";
}

$arrSpeedText = [50, 60, 70, 80, 90, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000];

if (isset($_SESSION['user']['speed_grade'])) {
    $currentUserSpeed = $_SESSION['user']['speed_grade'];
} else {
    $currentUserSpeed = 50;
}

foreach ($arrSpeedText as $value) {
    if ($currentUserSpeed > $value) {
        $newSpeed = $value;
    } else {
        $newSpeed = $value;
        break;
    }
}
?>

<main class="training-basic practice-page accelerator">
    <div class="setting-wrapper hidden-block">
        <p>
            <span id="setting-title"><?= $dataText->title ?></span>
            <span id="setting-speed"><?= $newSpeed ?></span>
        </p>
    </div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start"></div>
            <div class="practice-wrapper accelerator">
                <div class="info-block" id="info-block">
                    <div data-words="<?= $dataText->words?>">
                        <?= $text ?>
                    </div>

                    <div class="curtain"></div>
                </div>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div class="training-next">
                    <a href="" class="return-list"><?= $languageConstants['NEXTEXERCISE'] ?></a>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="/js/training/reading-accelerator.js"></script>
