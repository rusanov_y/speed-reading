<?php
/**
 * Исходная страница выбора вида тренировки
 *
 * @var $languageConstants
 */

?>

<main class="training-page">
    <div class="container flex-block">
        <div class="training-index">
            <a href="training/speedy" class="btn orange-bg-light"><?= $languageConstants['FAST'] ?> (5 <?= $languageConstants['MINUTES'] ?>)</a>
            <a href="training/periodic" class="btn orange-bg-light"><?= $languageConstants['PERIODIC'] ?> (10 <?= $languageConstants['MINUTES'] ?>)</a>
            <a href="training/optimal" class="btn orange-bg-light"><?= $languageConstants['OPTIMAL'] ?> (30 <?= $languageConstants['MINUTES'] ?>)</a>
            <a href="training/intense" class="btn orange-bg-light"><?= $languageConstants['INTENCE'] ?> (60 <?= $languageConstants['MINUTES'] ?>)</a>
        </div>
    </div>
</main>
