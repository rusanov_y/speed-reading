<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 04.09.19
 * Time: 17:33
 */
?>

<main class="no-fixed-page bg-1">
    <div class="container flex-block">
        <div class="form-guest" id="form-guest">
            <h2>Регистрация</h2>
            <form action="" method="post">
                <div class="field-group field-group-text">
                    <p><input type="text" name="login" placeholder="Ваш логин"></p>
                    <p class="info-field"></p>
                </div>
                <div class="field-group field-group-text">
                    <p><input type="password" name="pass" placeholder="Ваш пароль"></p>
                    <p class="info-field"></p>
                </div>
                <input type="hidden" name="do" value="guest">
                <div class="field-group">
                    <input class="btn orange-bg-light" type="submit" value="Войти">
                </div>
                <p class="orange-text-dark" style="font-weight: 700; text-align: center;">* Гостевой доступ предоставляется на 2 суток</p>
            </form>
        </div>
    </div>
</main>