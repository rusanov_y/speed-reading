<?php
/**
 * @var $languageConstants
 */

?>
<main class="index-page flex-block">
    <div class="btn-body flex-block">
        <a class=" btn about-training" href="faq"><?= $languageConstants['ABOUT'] ?></a>
<!--        <a class="btn btn-register" href="user/signup">--><?//= $languageConstants['TEST'] ?><!--</a>-->
        <a class="btn orange-bg-light" href="user/login"><?= $languageConstants['COMEIN'] ?></a>
    </div>
    <div class="index-bg"></div>
</main>

