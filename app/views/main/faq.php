<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 04.09.19
 * Time: 18:14
 */
?>

<main class="no-fixed-page faq-page">
    <div class="container">
        <a class="link-back" onclick="javascript:history.back()">&larr; Назад</a>
        <h1>Основные возможности тренажёра</h1>

        <p>Для того, чтобы Вам легче было освоиться с тренажёром, изучить его основные функции, мы подготовили ряд видео уроков, на которых рассказали, какие функции есть в тренажёре, их предназначение и как ими пользоваться.</p>


    </div>

</main>