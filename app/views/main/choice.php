<?php
/**
 * @var $languageConstants
 */

?>

<main class="job-level">

    <div class="container">
        <div class="flex-block">
                <a href="/training" class="btn orange-bg-light"><?= $languageConstants['TRAINING'] ?></a>
                <a href="/practice" class="btn orange-bg-light"><?= $languageConstants['PRACTICE'] ?></a>
        </div>
    </div>

</main>

<script>

    // кроссплатформенное создание XMLHttpRequest
    function getXmlHttp(){
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    }

    function getUserAge() {

        var req = getXmlHttp();
        var sendData = "age=true";
        req.open('POST', '/choice', true);
        req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.onreadystatechange = function() {
            if (req.readyState === 4) {
                if(req.status === 200) {

                    sessionStorage.setItem('userAge', req.responseText);

                }
            }
        };
        req.send(sendData);


    }

    getUserAge();

</script>