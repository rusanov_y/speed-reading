<?php
/**
 * @var $content
 */
?>
<!doctype html>
<html lang=ru>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="none"/>
    <title>Админпанель тренажёра по скорочтению</title>
    <link rel="rel" type="image/x-icon" href="/i/favicon.ico">
    <link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="/i/apple-touch-favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<header class="header-admin">
    <div class="container">
        <p>
            Здравствуйте, <?= $_SESSION['user']['name'] ?>!
            <a class="" href="/genom/logout">Выйти</a>
        </p>
    </div>
</header>

<?= $content ?>

<footer class="footer-admin">
    <div class="container flex-block">
        <p>© 2016-2019 GEN.OM</p>
<!--        <p><a class="btn" href="/training/levels">Выйти с админ.панели</a></p>-->
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>