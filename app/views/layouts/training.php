<?php
/**
 * Шаблон страницы для тренировок и упражнений
 *
 * @var $content - переменную куда передаёт содержание страницы
 */

?>

<!doctype html>
<html lang=ru>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="none"/>
    <title>Тренажёр по скорочтению genom.club</title>
    <link rel="rel" type="image/x-icon" href="/i/favicon.ico">
    <link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="/i/apple-touch-favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Ubuntu+Mono:400&display=swap&subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<header class="header-training">
    <div class="container flex-block">
        <a class="btn-back" onclick="javascript:window.history.back();">&larr; Назад</a>
        <a title="Выйти" href="/user/logout" class="nav-link">Выйти</a>
    </div>
</header>

<?= $content ?>

<link  rel="stylesheet" href="/css/chartist.min.css">

<script src="/js/main.js" async></script>
<script src="/js/chartist.min.js" async></script>
</body>
</html>
