<?php
/**
 * Общий шаблон страниц сайта
 * @var $content
 */

$lang = $_SESSION['lang'];

?>

<!doctype html>
<html lang=ru>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="none"/>
    <title>Тренажёр по скорочтению genom.club</title>
    <link rel="rel" type="image/x-icon" href="/i/favicon.ico">
    <link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="/i/apple-touch-favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="/css/main.var1.css">
</head>
<body>
<header class="header-main" id="headerMain">
    <nav class="flex-block">
        <div class="navbar-logo-wrap">
            <div class="index-top-logo" id="nb-logo"><a href="/"></a></div>
        </div>
        <input type="checkbox" class="top-menu-checkbox" id="top-menu-checkbox">
        <label class="label-top-menu-checkbox" for="top-menu-checkbox"></label>
        <div class="top-menu-body" id="headerMainContent">
            <form id="localization-form" action="" method="post" style="margin-right: 15px">
                <select style="border: none; background: none">
                    <option value="ru" <?= ($lang === 'ru') ? 'selected' : ''; ?>>русcкий</option>
                    <!--<option value="kz" <?/*= ($lang === 'kz') ? 'selected' : ''; */?>>қазақ</option>
                    <option value="ua" <?/*= ($lang === 'ua') ? 'selected' : ''; */?>>український</option>-->
                </select>
            </form>
            <ul class="navbar-nav hd-info ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="tel:+77279783979">+7 727 978 39 79</a>
                    <a class="nav-link" href="tel:+77074899559">+7 707 489 95 59</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="mailto:info@genom.club">info@genom.club</a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<?= $content ?>

<footer class="footer-base" id="footerMain">
    <div class="footer-bottom flex-block">
        <div class="ft-logo-wrap">
            <div class="ft-logo"></div>
        </div>
        <div class="ft-tm-txt">
            <p>© 2016-<?= date('Y') ?> GEN.OM</p>
        </div>
        <?php if ( isset($_SESSION['user']) && isset($_SESSION['user']['adm_role']) && $_SESSION['user']['adm_role'] === 'teacher' ) : ?>
            <div class="admin-btn-body">
                <a href="/genom" class="btn admin-btn">Админ.панель</a>
            </div>
        <?php endif; ?>
    </div>
</footer>

<script src="/js/main.js"></script>
</body>
</html>
