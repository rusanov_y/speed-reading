<?php
/**
 * Страница выбора упражнения
 *
 * @var $languageConstants
 */

?>

<main class="practice-page practice-index no-fixed-page">
    <div class="container">
        <div class="flex-block">
            <a href="practice/schulte" class="btn orange-bg-light schulte"><?= $languageConstants['SCHULTETABLE'] ?></a>
            <a href="practice/letter-search" class="btn orange-bg-light search-letter"><?= $languageConstants['SEARCHLETTERS'] ?></a>
            <a href="practice/digital-search" class="btn orange-bg-light search-digit"><?= $languageConstants['SEARCHDIGITAL'] ?></a>
            <a href="practice/even-odd" class="btn orange-bg-light even-odd"><?= $languageConstants['EVENODD'] ?></a>
            <a href="practice/remember-number" class="btn orange-bg-light remember-number"><?= $languageConstants['REMEMBERNUMBER'] ?></a>
            <a href="practice/running-words" class="btn orange-bg-light running-words"><?= $languageConstants['RUNWORDS'] ?></a>
            <a href="practice/word-search" class="btn orange-bg-light word-search"><?= $languageConstants['SEARCHWORDS'] ?></a>
            <a href="practice/words-twins" class="btn orange-bg-light words-twins"><?= $languageConstants['TWINWORDS'] ?></a>
            <a href="practice/field-view" class="btn orange-bg-light field-view"><?= $languageConstants['FIELDVIEW'] ?></a>
            <a href="practice/central-point" class="btn orange-bg-light central-point"><?= $languageConstants['CENTREDOT'] ?></a>
            <a href="practice/remember-words" class="btn orange-bg-light remember-words"><?= $languageConstants['REMEMBERWORDS'] ?></a>
            <a href="practice/chain-words" class="btn orange-bg-light chain-words"><?= $languageConstants['CHAINWORDS'] ?></a>
            <a href="practice/reading-grade" class="btn orange-bg-light reading-grade"><?= $languageConstants['READINGGRADE'] ?></a>
            <a href="practice/reading-accelerator" class="btn orange-bg-light reading-accelerator"><?= $languageConstants['READINGACCELERATOR'] ?></a>
            <a href="practice/text-search" class="btn orange-bg-light text-search"><?= $languageConstants['TEXTSEARCH'] ?></a>
            <a href="practice/stroop" class="btn orange-bg-light stroop"><?= $languageConstants['STROOPTEST'] ?></a>
        </div>
    </div>
</main>
