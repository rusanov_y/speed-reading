<?php
/**
 * Упражнение Цепь слов
 *
 * @var $languageConstants array
 * @var $infoPractice string
 */

?>

<main class="training-basic practice-page remember-words">
    <div class="setting-wrapper">
        <form action="" id="setting-form">
            <label class="lbl-setting-time"  for="setting-time"></label>
            <select name="setting-time" id="setting-time">
                <option value="3000" >3 сек.</option>
                <option value="5000" >5 сек.</option>
                <option value="7000">7 сек.</option>
                <option value="10000" selected>10 сек.</option>
                <option value="15000">15 сек.</option>
                <option value="20000">20 сек.</option>
                <option value="25000">25 сек.</option>
                <option value="30000">30 сек.</option>
                <option value="35000">35 сек.</option>
                <option value="40000">40 сек.</option>
                <option value="45000">45 сек.</option>
                <option value="50000">50 сек.</option>
                <option value="55000">55 сек.</option>
                <option value="60000">60 сек.</option>
            </select>
            <label class="lbl-setting-level" for="setting-level"></label>
            <select name="setting-level" id="setting-level">
                <option value="1" >10 <?= $languageConstants['WORDS'] ?></option>
                <option value="2" >20 <?= $languageConstants['WORDS'] ?></option>
                <option value="3" >30 <?= $languageConstants['WORDS'] ?></option>
                <option value="4" >40 <?= $languageConstants['WORDS'] ?></option>
                <option value="5" >50 <?= $languageConstants['WORDS'] ?></option>
            </select>
            <button type="button" class="practice-info" onclick="openPracticeInfo()"></button>
        </form>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper remember-words flex-block">
                <div class="flex-block">
                    <div class="info-block" id="info-block"></div>
                    <ul class="control-block"></ul>
                </div>
                <ul class="tbl-wrapper" id="tbl-wrapper"></ul>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <h4><?= $languageConstants['TRAININGRESULT'] ?></h4>
                    <p class="result-practice"></p>
                    <h4><?= $languageConstants['ANSWERSPENT'] ?></h4>
                    <p class="time-answer"></p>
                </div>
                <div>
                    <button class="btn orange-bg-light repeat" onclick="repeatPractice();"><?= $languageConstants['REPEAT'] ?></button>
                    <a href="/practice" class="return-list"><?= $languageConstants['TOLISTEXERCISES'] ?></a>
                </div>
            </div>
            <div class="current-level" id="current-level"></div>
        </div>
    </div>
    <section class="practice-info-wrapper flex-block" id="practice-info-wrapper">
        <div class="container">
            <h3 class="orange-text-dark">Цепь слов</h3>
            <div><?= $infoPractice ?></div>
            <button onclick="closePracticeInfo()"><?= $languageConstants['ALLCLEAR'] ?></button>
        </div>
    </section>
</main>

<script src="/js/practice/basic.js"></script>
<script src="/js/practice/chain-words.js"></script>
