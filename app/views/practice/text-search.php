<?php
/**
 * Упражнение "Поиск в тексте"
 *
 * @var $titles array
 * @var $dataText array
 * @var $languageConstants array
 * @var $infoPractice string
 */

$text = "";

foreach ($dataText->text->p as $value) {

    $text .= "<p>" . $value . "</p>";

}

?>

<main class="training-basic practice-page text-search">
    <div class="setting-wrapper" id="setting-wrapper">
        <form action="" id="setting-form">
            <label class="lbl-setting-title"  for="setting-title"></label>
            <select class="setting-title" name="setting-title" id="setting-title">
                <?php foreach ($titles as $value) {
                    if ($dataText->title === $value) {
                        echo "<option value='{$value}' selected>{$value}</option>";
                    } else {
                        echo "<option value='{$value}'>{$value}</option>";
                    }
                } ?>
            </select>
            <label class="lbl-setting-time"  for="setting-time"></label>
            <select name="setting-time" id="setting-time">
                <option value="30000">30 сек.</option>
                <option value="60000">1 мин.</option>
                <option value="90000">1.5 мин.</option>
                <option value="120000" selected>2 мин.</option>
                <option value="180000">3 мин.</option>
                <option value="300000">5 мин.</option>
                <option value="420000">7 мин.</option>
                <option value="600000">10 мин.</option>
                <option value="900000">15 мин.</option>
            </select>
            <label class="lbl-setting-level" for="setting-level"></label>
            <select name="setting-level" id="setting-level">
                <option value="1" <?= ((isset($_SESSION['user']['level_text']) && $_SESSION['user']['level_text'] == NULL) || !isset($_SESSION['user']['level_text']) ) ? 'selected' : ''; ?>>1</option>
                <option value="2" <?= (isset($_SESSION['user']['level_text']) && ($_SESSION['user']['level_text'] == 2)) ? 'selected' : ''; ?>>2</option>
                <option value="3" <?= (isset($_SESSION['user']['level_text']) && ($_SESSION['user']['level_text'] == 3)) ? 'selected' : ''; ?>>3</option>
                <option value="4" <?= (isset($_SESSION['user']['level_text']) && ($_SESSION['user']['level_text'] == 4)) ? 'selected' : ''; ?>>4</option>
                <option value="5" <?= (isset($_SESSION['user']['level_text']) && ($_SESSION['user']['level_text'] == 5)) ? 'selected' : ''; ?>>5</option>
                <option value="6" <?= (isset($_SESSION['user']['level_text']) && ($_SESSION['user']['level_text'] == 6)) ? 'selected' : ''; ?>>6</option>
                <option value="7" <?= (isset($_SESSION['user']['level_text']) && ($_SESSION['user']['level_text'] == 7)) ? 'selected' : ''; ?>>7</option>
                <option value="8" <?= (isset($_SESSION['user']['level_text']) && ($_SESSION['user']['level_text'] == 8)) ? 'selected' : ''; ?>>8</option>
                <option value="9" <?= (isset($_SESSION['user']['level_text']) && ($_SESSION['user']['level_text'] == 9)) ? 'selected' : ''; ?>>9</option>
                <option value="10" <?= (isset($_SESSION['user']['level_text']) && ($_SESSION['user']['level_text'] == 10)) ? 'selected' : ''; ?>>10</option>
            </select>
            <button type="button" class="practice-info" onclick="openPracticeInfo()"></button>
        </form>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>

            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper text-search">
                <div>
                    <h3 id="currentAction"><?= $languageConstants['FIND'] ?>:</h3>
                    <p id="info-block"></p>
                </div>
                <div class="text-wrapper" id="text-wrapper"><div></div></div>

            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <button class="btn orange-bg-light repeat" onclick="repeatPractice();"><?= $languageConstants['REPEAT'] ?></button>
                    <a href="/practice" class="return-list"><?= $languageConstants['TOLISTEXERCISES'] ?></a>
                </div>
            </div>
            <div class="text-iso" id="text-iso"><?= $text ?></div>
        </div>
    </div>
    <section class="practice-info-wrapper flex-block" id="practice-info-wrapper">
        <div class="container">
            <h3 class="orange-text-dark">Поиск в тексте</h3>
            <div><?= $infoPractice ?></div>
            <button onclick="closePracticeInfo()"><?= $languageConstants['ALLCLEAR'] ?></button>
        </div>
    </section>
</main>

<script src="/js/practice/basic.js"></script>
<script src="/js/practice/text-search.js"></script>
