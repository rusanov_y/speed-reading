<?php
/**
 * Упражнение Бегущие слова
 *
 * @var $languageConstants array
 * @var $infoPractice string
 */

?>

<main class="training-basic practice-page stroop">
    <div class="setting-wrapper">
        <form action="" id="setting-form">
            <label class="lbl-setting-color" for="setting-color"></label>
            <select name="setting-color" id="setting-color">
                <option value="3">3 <?= $languageConstants['COLOR'] ?></option>
                <option value="4">4 <?= $languageConstants['COLOR'] ?></option>
                <option value="5">5 <?= $languageConstants['COLORS'] ?></option>
            </select>
            <button type="button" class="practice-info" onclick="openPracticeInfo()"></button>
        </form>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper stroop" id="practice-wrapper">
                <ul>
                    <li class="card-1" id="card-1">
                        <table class="stroop-card"><tbody></tbody></table>
                        <hr>
                        <table class="tbl-wrapper" id="btn-card1"><tbody></tbody></table>
                    </li>
                    <li class="card-2" id="card-2">
                        <table class="stroop-card"><tbody></tbody></table>
                        <hr>
                        <table class="tbl-wrapper" id="btn-card2"><tbody></tbody></table>
                    </li>
                    <li class="card-3" id="card-3">
                        <table class="stroop-card"><tbody></tbody></table>
                        <hr>
                        <table class="tbl-wrapper" id="btn-card3"><tbody></tbody></table>
                    </li>
                </ul>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <h4><?= $languageConstants['TRAININGRESULT'] ?></h4>
                    <h5><?= $languageConstants['CARDNO1'] ?></h5>
                    <p class="result-practice"></p>
                    <h5><?= $languageConstants['CARDNO2'] ?></h5>
                    <p class="result-practice"></p>
                    <h5><?= $languageConstants['CARDNO3'] ?></h5>
                    <p class="result-practice"></p>
                </div>
                <div>
                    <a href="/practice" class="return-list"><?= $languageConstants['TOLISTEXERCISES'] ?></a>
                </div>
            </div>
            <div class="current-level" id="current-level"></div>
        </div>
    </div>
    <section class="practice-info-wrapper flex-block" id="practice-info-wrapper">
        <div class="container">
            <h3 class="orange-text-dark">Тест Струпа</h3>
            <div><?= $infoPractice ?></div>
            <button onclick="closePracticeInfo()"><?= $languageConstants['ALLCLEAR'] ?></button>
        </div>
    </section>
</main>

<script src="/js/practice/basic.js"></script>
<script src="/js/practice/stroop.js"></script>
