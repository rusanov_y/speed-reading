<?php
/**
 * Упражнение Оценка чтения
 *
 * @var $titles array
 * @var $dataText array
 * @var $languageConstants array
 * @var $infoPractice string
 */

$text = "";

foreach ((array)$dataText->text->p as $value) {
    $text .= "<p>$value</p>";
}

$arrSpeedText = [50, 60, 70, 80, 90, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000];

if (isset($_SESSION['user']['speed_grade'])) {
    $currentUserSpeed = $_SESSION['user']['speed_grade'];
} else {
    $currentUserSpeed = 50;
}

foreach ($arrSpeedText as $value) {
    if ($currentUserSpeed > $value) {
        $newSpeed = $value;
    } else {
        $newSpeed = $value;
        break;
    }
}
?>

<main class="training-basic practice-page accelerator">
    <div class="setting-wrapper" id="setting-wrapper">
        <form action="" id="setting-form">
            <label class="lbl-setting-title"  for="setting-title"></label>
            <select class="setting-title" name="setting-title" id="setting-title">
                <?php foreach ($titles as $value) {
                    if ($dataText->title === $value) {
                        echo "<option value='{$value}' selected>{$value}</option>";
                    } else {
                        echo "<option value='{$value}'>{$value}</option>";
                    }
                } ?>
            </select>
            <label class="lbl-setting speed" for="setting-speed"></label>
            <select class="setting-speed" name="setting-speed" id="setting-speed">
                <?php foreach ($arrSpeedText as $value) :
                    if ($newSpeed === $value) {
                        echo "<option value='{$value}' selected>{$value} " . $languageConstants['WORDSMIN'] . "</option>";
                    } else {
                        echo "<option value='{$value}'>{$value} " . $languageConstants['WORDSMIN'] . "</option>";
                    }
                endforeach; ?>
            </select>
            <button type="button" class="practice-info" onclick="openPracticeInfo()"></button>
        </form>
    </div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start"></div>
            <div class="practice-wrapper accelerator">
                <div class="info-block" id="info-block">
                    <div data-words="<?= $dataText->words?>">
                        <?= $text ?>
                    </div>

                    <div class="curtain"></div>
                </div>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <a href="/practice" class="return-list"><?= $languageConstants['TOLISTEXERCISES'] ?></a>
                </div>
            </div>
        </div>
    </div>
    <section class="practice-info-wrapper flex-block" id="practice-info-wrapper">
        <div class="container">
            <h3 class="orange-text-dark">Ускоритель чтения</h3>
            <div><?= $infoPractice ?></div>
            <button onclick="closePracticeInfo()"><?= $languageConstants['ALLCLEAR'] ?></button>
        </div>
    </section>
</main>

<script src="/js/practice/reading-accelerator.js"></script>
