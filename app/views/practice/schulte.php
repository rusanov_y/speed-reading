<?php
/**
 * страница тренировки по таблицам Шульте
 *
 * @var $languageConstants array
 * @var $infoPractice string текст информации об упражнении
 */

?>

<main class="training-basic practice-page schultz-page">
    <div class="setting-wrapper">
        <form action="" id="schulte-form">
            <label class="lbl-setting-time"  for="setting-time"></label>
            <select name="setting-time" id="setting-time">
                <option value="30000">0.5 мин.</option>
                <option value="60000">1 мин.</option>
                <option value="90000">1.5 мин.</option>
                <option value="120000" selected>2 мин.</option>
                <option value="180000">3 мин.</option>
                <option value="300000">5 мин.</option>
                <option value="300000">7 мин.</option>
                <option value="300000">10 мин.</option>
                <option value="300000">15 мин.</option>
            </select>
            <label class="lbl-setting-symbol" for="setting-symbol"></label>
            <select name="setting-symbol" id="setting-symbol">
                <option value="number" selected>1,2,3...</option>
                <option value="letters">A,B,C...</option>
            </select>
            <label class="lbl-setting-level" for="setting-level"></label>
            <select name="setting-level" id="setting-level">
                <option value="1" <?= ((isset($_SESSION['user']['schulte']) && $_SESSION['user']['schulte'] == NULL) || !isset($_SESSION['user']['schulte']) ) ? 'selected' : ''; ?>>1</option>
                <option value="2" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 2)) ? 'selected' : ''; ?>>2</option>
                <option value="3" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 3)) ? 'selected' : ''; ?>>3</option>
                <option value="4" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 4)) ? 'selected' : ''; ?>>4</option>
                <option value="5" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 5)) ? 'selected' : ''; ?>>5</option>
                <option value="6" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 6)) ? 'selected' : ''; ?>>6</option>
                <option value="7" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 7)) ? 'selected' : ''; ?>>7</option>
                <option value="8" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 8)) ? 'selected' : ''; ?>>8</option>
                <option value="9" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 9)) ? 'selected' : ''; ?>>9</option>
                <option value="10" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 10)) ? 'selected' : ''; ?>>10</option>
                <option value="11" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 11)) ? 'selected' : ''; ?>>11</option>
                <option value="12" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 12)) ? 'selected' : ''; ?>>12</option>
                <option value="13" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 13)) ? 'selected' : ''; ?>>13</option>
                <option value="14" <?= (isset($_SESSION['user']['schulte']) && ($_SESSION['user']['schulte'] == 14)) ? 'selected' : ''; ?>>14</option>
            </select>
            <button type="button" class="practice-info" onclick="openPracticeInfo()"></button>
        </form>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper flex-block">
                <div>
                    <h3 id="currentAction"><?= $languageConstants['CLICK'] ?>:</h3>
                    <p id="currentElementClick"></p>
                </div>
                <table class="tbl-wrapper center-point" id="tbl-wrapper">
                    <tbody></tbody>
                </table>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <h4><?= $languageConstants['WORKOUTEFFICIENCY'] ?></h4>
                    <p class="result-practice"></p>
                </div>
                <div class="chart-wrapper">
                    <h4><?= $languageConstants['YOURPACEEXECUTION'] ?></h4>
                    <div class="ct-chart ct-minor-seventh"></div>
                </div>
                <div>
                    <button class="btn orange-bg-light repeat" onclick="repeatPractice();"><?= $languageConstants['REPEAT'] ?></button>
                    <a href="/practice" class="return-list"><?= $languageConstants['TOLISTEXERCISES'] ?></a>
                </div>
            </div>
            <div class="current-level" id="current-level"></div>
        </div>
    </div>
    <section class="practice-info-wrapper flex-block" id="practice-info-wrapper">
        <div class="container">
            <h3 class="orange-text-dark">Таблица Шульте</h3>
            <div>
                <?= $infoPractice ?>
            </div>
            <button onclick="closePracticeInfo()"><?= $languageConstants['ALLCLEAR'] ?></button>
        </div>
    </section>
</main>

<script src="/js/practice/basic.js"></script>
<script src="/js/practice/schulte.js"></script>