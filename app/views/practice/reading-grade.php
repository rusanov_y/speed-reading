<?php
/**
 * Упражнение Оценка чтения
 *
 * @var $titles array
 * @var $dataText array
 * @var $languageConstants array
 * @var $infoPractice string
 */

$text = "";

foreach ((array)$dataText->text->p as $value) {
    $text .= "<p>$value</p>";
}

$questions = $dataText->questions->question_body;
shuffle($questions);
$questions = array_slice($questions, 0, 5);

?>

<main class="training-basic practice-page reading-grade">
    <div class="setting-wrapper" id="setting-wrapper">
        <form action="" id="setting-form">
            <label class="lbl-setting-title"  for="setting-title"></label>
            <select class="setting-title" name="setting-title" id="setting-title">
                <?php foreach ($titles as $value) {
                    if ($dataText->title === $value) {
                        echo "<option value='{$value}' selected>{$value}</option>";
                    } else {
                        echo "<option value='{$value}'>{$value}</option>";
                    }
                } ?>
            </select>
            <button type="button" class="practice-info" onclick="openPracticeInfo()"></button>
        </form>
    </div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start"></div>
            <div class="practice-wrapper reading-grade">
                <div id="info-block">
                    <div>
                        <?= $text ?>
                    </div>
                </div>
                <div class="btn-body">
                    <button class="btn orange-bg-light" onclick="nextText();"><?= $languageConstants['FARTHER'] ?></button>
                </div>
            </div>
            <div class="control-block" id="control-block">
                <ul>
                    <?php
                    $i = 0;
                    foreach ($questions as $item) :

                        ?>
                    <li>
                        <p class="orange-text-dark"><?= $languageConstants['QUESTION'] ?> <?= $i + 1 ?> <?= $languageConstants['FROM'] ?> 5</p>
                        <h4><?= $item->question ?></h4>
                        <div>
                        <?php
                        $successAnswer = $item->answers->answer[0];
                        $arrTmp = $item->answers->answer;
                        shuffle($arrTmp);

                        foreach ($arrTmp as $value) {
                        ?>
                            <button class="btn" data-answer="<?= $successAnswer ?>" data-number="<?= $i + 1 ?>" onclick="answerClick(this)"><?= $value ?></button>
                        <?php } ?>
                        </div>

                    </li>
                    <?php $i++; endforeach; ?>
                </ul>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div class="result-reading" id="result-reading">
                    <h4><?= $languageConstants['READSPEED'] ?>:<br><span data-countwords="<?= $dataText->words ?>"></span> <?= $languageConstants['WORDSMIN'] ?>.</h4>
                    <p><?= $languageConstants['CORRECTANSWERS'] ?> <span></span> <?= $languageConstants['FROM'] ?> 5 (<span></span>%)</p>
                    <p></p>
                </div>
                <div>
                    <a href="/practice" class="return-list"><?= $languageConstants['TOLISTEXERCISES'] ?></a>
                </div>
            </div>
        </div>
    </div>
    <section class="practice-info-wrapper flex-block" id="practice-info-wrapper">
        <div class="container">
            <h3 class="orange-text-dark">Оценка чтения</h3>
            <div><?= $infoPractice ?></div>
            <button onclick="closePracticeInfo()"><?= $languageConstants['ALLCLEAR'] ?></button>
        </div>
    </section>
</main>

<script src="/js/practice/reading-grade.js"></script>
