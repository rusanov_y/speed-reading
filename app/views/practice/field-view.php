<?php
/**
 * Упражнение Поиск слова
 *
 * @var $languageConstants array
 * @var $infoPractice string
 */

?>

<main class="training-basic practice-page field-view">
    <div class="setting-wrapper">
        <form action="" id="setting-form">
            <label class="lbl-setting-time"  for="setting-time"></label>
            <select name="setting-time" id="setting-time">
                <option value="30000">30 сек.</option>
                <option value="60000" selected>1 мин.</option>
                <option value="90000">1.5 мин.</option>
                <option value="120000">2 мин.</option>
                <option value="180000">3 мин.</option>
                <option value="300000">5 мин.</option>
                <option value="300000">7 мин.</option>
                <option value="300000">10 мин.</option>
                <option value="300000">15 мин.</option>
            </select>
            <label class="lbl-setting-level" for="setting-level"></label>
            <select name="setting-level" id="setting-level">
                <option value="1" <?= ((isset($_SESSION['user']['field_view']) && $_SESSION['user']['field_view'] == NULL) || !isset($_SESSION['user']['field_view']) ) ? 'selected' : ''; ?>>1</option>
                <option value="2" <?= (isset($_SESSION['user']['field_view']) && ($_SESSION['user']['field_view'] == 2)) ? 'selected' : ''; ?>>2</option>
                <option value="3" <?= (isset($_SESSION['user']['field_view']) && ($_SESSION['user']['field_view'] == 3)) ? 'selected' : ''; ?>>3</option>
                <option value="4" <?= (isset($_SESSION['user']['field_view']) && ($_SESSION['user']['field_view'] == 4)) ? 'selected' : ''; ?>>4</option>
                <option value="5" <?= (isset($_SESSION['user']['field_view']) && ($_SESSION['user']['field_view'] == 5)) ? 'selected' : ''; ?>>5</option>
                <option value="6" <?= (isset($_SESSION['user']['field_view']) && ($_SESSION['user']['field_view'] == 6)) ? 'selected' : ''; ?>>6</option>
                <option value="7" <?= (isset($_SESSION['user']['field_view']) && ($_SESSION['user']['field_view'] == 7)) ? 'selected' : ''; ?>>7</option>
                <option value="8" <?= (isset($_SESSION['user']['field_view']) && ($_SESSION['user']['field_view'] == 8)) ? 'selected' : ''; ?>>8</option>
                <option value="9" <?= (isset($_SESSION['user']['field_view']) && ($_SESSION['user']['field_view'] == 9)) ? 'selected' : ''; ?>>9</option>
            </select>
            <button type="button" class="practice-info" onclick="openPracticeInfo()"></button>
        </form>
    </div>
    <div class="timeline" id="timeline"></div>
    <div class="container">
        <div class="flex-block">
            <div class="start-btn-wrapper active" id="start-wrapper">
                <button class="btn orange-bg-light" onclick="clickStart();"><?= $languageConstants['START'] ?></button>
            </div>
            <div class="counter-start" id="counter-start">3</div>
            <div class="practice-wrapper field-view flex-block">
                <div class="flex-block">
                    <table class="info-block" id="info-block">
                        <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <table class="tbl-wrapper col-2" id="tbl-wrapper">
                    <tbody>
                    <tr>
                        <td data-value='0'><?= $languageConstants['NOTSAME'] ?></td>
                        <td data-value='1'><?= $languageConstants['SAME'] ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="finish-practice flex-block" id="finish-practice">
                <h3><?= $languageConstants['EXERCISECOMPLETED'] ?></h3>
                <div>
                    <h4><?= $languageConstants['TRAININGRESULT'] ?></h4>
                    <p class="result-practice"></p>
                    <p></p>
                </div>
                <div>
                    <button class="btn orange-bg-light repeat" onclick="repeatPractice();"><?= $languageConstants['REPEAT'] ?></button>
                    <a href="/practice" class="return-list"><?= $languageConstants['TOLISTEXERCISES'] ?></a>
                </div>
            </div>
            <div class="current-level" id="current-level"></div>
        </div>
    </div>
    <section class="practice-info-wrapper flex-block" id="practice-info-wrapper">
        <div class="container">
            <h3 class="orange-text-dark">Поле зрения</h3>
            <div><?= $infoPractice ?></div>
            <button onclick="closePracticeInfo()"><?= $languageConstants['ALLCLEAR'] ?></button>
        </div>
    </section>
</main>

<script src="/js/practice/basic.js"></script>
<script src="/js/practice/field-view.js"></script>
