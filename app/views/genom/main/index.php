<?php
/**
 * Главная страница админпанели.
 * @var $countAdmins integer - количество администраторов
 * @var $countActiveAdmins integer - количество активированных администраторов
 * @var $coachsCenter integer - количество преподавателей genom
 * @var $directorsCenter integer - количество директоров других центров
 * @var $otherCenter integer - количество преподавателей не genom
 * @var $countUsers integer - количество учеников
 * @var $activeUsers integer - количество активированных учеников
 */
?>

<main class="main-admin admin-index">

    <?php include __DIR__ . "/../blocks/_aside.php" ?>

    <section>
        <div class="container general-info">

            <h2>Данные на <?= date('d.m.Y') ?></h2>

            <!--    Информация для администраторов     -->
            <?php if ( $_SESSION['user']['role'] === '1' ) : ?>
                <h3>Директоров центров, преподавателей</h3>
                <ul class="flex-block">
                    <li>
                        <p>Всего<br><span><?= $countAdmins ?></span></p>
                        <p style="font-size: smaller">(активировано - <?= $countActiveAdmins ?>)</p>
                    </li>
                    <li>
                        <p>Преподавателей Genom<br><span><?= $coachsCenter ?></span></p>
                    </li>
                    <li>
                        <p>Директоров центров<br><span><?= $directorsCenter ?></span></p>
                    </li>
                    <li>
                        <p>Преподавателей других центров<br><span><?= $otherCenter ?></span></p>
                    </li>
                </ul>

                <h3>Учеников</h3>
                <ul class="flex-block">
                    <li>
                        <p>Всего<br><span><?= $countUsers ?></span></p>
                        <p style="font-size: smaller">( активировано - <?= $activeUsers ?> )</p>
                    </li>
                </ul>

            <?php endif; ?>

            <!--    Информация для преподвателей genom     -->
            <?php if ( $_SESSION['user']['role'] === '2' ) : ?>

                <h3>Учеников</h3>
                <ul class="flex-block">
                    <li>
                        <p>Всего<br><span><?= $countUsers ?></span></p>
                        <p>активировано - <?= $activeUsers ?></p>
                    </li>
                </ul>

            <?php endif; ?>

            <!--    Информация для директоров центров     -->
            <?php if ( $_SESSION['user']['role'] === '3' ) : ?>

                <div class="flex-block">
                    <div>
                        <p>Окончание подписки: <span><?= date('d.m.Y', $_SESSION['user']['date_end']) ?></span></p>
                        <p><a href="https://shop.genom.club/simulator?id-user=<?= $_SESSION['user']['id'] ?>&trainer=speed-reading" class="btn orange-bg-light">Оплатить</a></p>
                    </div>
                    <p>Максимальное  количество учащихся - <span><?= $_SESSION['user']['count_users'] ?></span></p>
                </div>

                <?php if ( isset($_SESSION['user']['no_entry']) ) { ?>
                    <div class="error-info">Срок Вашей подписки закончился. Для продолжения пользования тренажёром оплатите продление подписки </div>
                <?php } ?>

                <?php if ( isset($_SESSION['user']['no_add_users']) ) { ?>
                    <div class="error-info">Вы не можете больше добавлять учеников. Для увеличения квоты обратитесь к администратору.</div>
                <?php } ?>

                <h3>Преподавателей</h3>
                <ul class="flex-block">
                    <li>
                        <p>Всего<br><span><?= $otherCenter ?></span></p>
                        <p style="font-size: smaller">( активировано - <?= $countActiveAdmins ?> )</p>
                    </li>
                </ul>

                <h3>Учеников</h3>
                <ul class="flex-block">
                    <li>
                        <p>Всего<br><span><?= $countUsers ?></span></p>
                        <p style="font-size: smaller">( активировано - <?= $activeUsers ?> )</p>
                    </li>
                </ul>

            <?php endif; ?>

            <!--    Информация для преподавателей не genom     -->
            <?php if ( $_SESSION['user']['role'] === '4' && !$_SESSION['user']['parent_id'] ) : ?>

                <div class="flex-block">
                    <div>
                        <p>Окончание подписки: <span><?= date('d.m.Y', $_SESSION['user']['date_end']) ?></span></p>
                        <p><a href="https://shop.genom.club/simulator?id-user=<?= $_SESSION['user']['id'] ?>&trainer=speed-reading" class="btn">Оплатить</a></p>
                    </div>
                    <p>Максимальное  количество учащихся - <span><?= $_SESSION['user']['count_users'] ?></span></p>
                </div>

                <?php if ( $_SESSION['user']['date_end'] < time() ) { ?>
                    <div class="error-info">Срок Вашей подписки закончился. Для продолжения пользования тренажёром оплатите продление подписки </div>
                <?php } ?>

                <?php if ( isset($_SESSION['user']['no_add_users']) ) { ?>
                    <div class="error-info">Вы не можете больше добавлять учеников. Для увеличения квоты обратитесь к администратору.</div>
                <?php } ?>

                <h3>Учеников</h3>
                <ul class="flex-block">
                    <li>
                        <p>Всего<br><span><?= $countUsers ?></span></p>
                        <p style="font-size: smaller">( активировано - <?= $activeUsers ?> )</p>
                    </li>
                </ul>

            <?php endif; ?>

            <!--    Информация для преподавателей других центров, у которых зарегистрирован директор     -->
            <?php if ( $_SESSION['user']['role'] === '4' && isset($_SESSION['user']['parent_id']) ) : ?>

                <h3>Учеников</h3>
                <ul class="flex-block">
                    <li>
                        <p>Всего<br><span><?= $countUsers ?></span></p>
                        <p style="font-size: smaller">( активировано - <?= $activeUsers ?> )</p>
                    </li>
                </ul>

            <?php endif; ?>


        </div>
    </section>
</main>