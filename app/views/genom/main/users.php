<?php
/**
 *
 * @var $users array
 * @var $arrCoach array
 * @var $pagination object
 */

?>

<main class="main-admin admin-index">

    <?php include __DIR__ . "/../blocks/_aside.php" ?>

    <section>
        <div class="container">

            <div class="search-body">
                <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="get">
                    <input type="submit" value="">
                    <input type="hidden" name="role" value="<?= $_GET['role'] ?>">
                    <input type="text" name="search" value="">
                </form>
            </div>
            <div class="basic-info">

                <?php if ( isset($_SESSION['errors']) ) : ?>
                    <div class="error-info"><?= $_SESSION['errors'] ?></div>
                    <?php
                endif;
                unset($_SESSION['errors']);
                ?>

                <?php if ( $_GET['role'] === 'admin' ) : ?>

                    <div class="flex-block">
                        <a href="/genom/user/create?role=admin" class="btn orange-bg-light">Добавить</a>
                    </div>

                    <table class="tbl-info">
                        <thead>
                        <tr>
                            <th>Активность</th>
                            <th>ФИО</th>
                            <th>Логин</th>
                            <th>Действия</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach ($users as $item) : ?>
                            <tr>
                                <td><?= ( $item['active'] === '1' ) ? "да" : "нет"; ?></td>
                                <td><?= $item['name'] ?></td>
                                <td><?= $item['login'] ?></td>
                                <td>
                                    <a href="/genom/user/view?id=<?= $item['id'] ?>&amp;role=admin" class="view-button"></a>
                                    <a href="/genom/user/edit?id=<?= $item['id'] ?>&amp;role=admin" class="edit-button"></a>
                                    <a href="/genom/user/delete?id=<?= $item['id'] ?>&amp;role=admin" class="delete-button"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>

                <?php endif; ?>

                <?php if ( $_GET['role'] === 'genom' ) : ?>

                    <div class="flex-block">
                        <a href="/genom/user/create?role=genom" class="btn orange-bg-light">Добавить</a>
                    </div>

                    <table class="tbl-info">
                        <thead>
                        <tr>
                            <th>Активность</th>
                            <th>ФИО</th>
                            <th>Логин</th>
                            <th>Действия</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach ($users as $item) : ?>
                            <tr>
                                <td><?= ( $item['active'] === '1' ) ? "да" : "нет"; ?></td>
                                <td><?= $item['name'] ?></td>
                                <td><?= $item['login'] ?></td>
                                <td>
                                    <a href="/genom/user/view?id=<?= $item['id'] ?>&amp;role=genom" class="view-button"></a>
                                    <a href="/genom/user/edit?id=<?= $item['id'] ?>&amp;role=genom" class="edit-button"></a>
                                    <a href="/genom/user/delete?id=<?= $item['id'] ?>&amp;role=genom" class="delete-button"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>

                <?php endif; ?>

                <?php if ( $_GET['role'] === 'director' ) : ?>

                    <div class="flex-block">
                        <a href="/genom/user/create?role=director" class="btn orange-bg-light">Добавить</a>
                    </div>

                    <table class="tbl-info">
                        <thead>
                        <tr>
                            <th>Активность</th>
                            <th>ФИО</th>
                            <th>Логин</th>
                            <th>Оплачено до</th>
                            <th>Кол-во учеников</th>
                            <th>Действия</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach ($users as $item) : ?>
                            <tr>
                                <td><?= ( $item['active'] === '1' ) ? "да" : "нет"; ?></td>
                                <td><?= $item['name'] ?></td>
                                <td><?= $item['login'] ?></td>
                                <td><?= date('d.m.Y H:i', $item['date_end']) ?></td>
                                <td><?= $item['count_users'] ?></td>
                                <td>
                                    <a href="/genom/user/view?id=<?= $item['id'] ?>&amp;role=director" class="view-button"></a>
                                    <a href="/genom/user/edit?id=<?= $item['id'] ?>&amp;role=director" class="edit-button"></a>
                                    <a href="/genom/user/delete?id=<?= $item['id'] ?>&amp;role=director" class="delete-button"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>

                <?php endif; ?>

                <?php if ( $_GET['role'] === 'coach' ) : ?>

                    <div class="flex-block">
                        <a href="/genom/user/create?role=coach" class="btn orange-bg-light">Добавить</a>
                        <?php if ( $pagination->countPage > 1 ) : ?>
                            <?= $pagination ?>
                        <?php endif; ?>
                    </div>

                    <table class="tbl-info">
                        <thead>
                        <tr>
                            <th>Активность</th>
                            <th>ФИО</th>
                            <th>Логин</th>
                            <th>Окончание подписки</th>
                            <th>Мах. кол-во учеников</th>
                            <th>Действия</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach ($users as $item) : ?>
                            <tr>
                                <td><?= ( $item['active'] === '1' ) ? "да" : "нет"; ?></td>
                                <td><?= $item['name'] ?></td>
                                <td><?= $item['login'] ?></td>
                                <td><?= ( $item['parent_id'] ) ? "&mdash;" : date('d.m.Y H:i', $item['date_end']) ?></td>
                                <td><?= ( $item['parent_id'] ) ? "&mdash;" : $item['count_users'] ?></td>
                                <td>
                                    <a href="/genom/user/view?id=<?= $item['id'] ?>&amp;role=coach" class="view-button"></a>
                                    <a href="/genom/user/edit?id=<?= $item['id'] ?>&amp;role=coach" class="edit-button"></a>
                                    <a href="/genom/user/delete?id=<?= $item['id'] ?>&amp;role=coach" class="delete-button"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>

                <?php endif; ?>

                <?php if ( $_GET['role'] === 'user' ) : ?>

                    <div class="flex-block">
                        <?php if ( !isset($_SESSION['user']['no_add_users']) ) { ?>

                            <a href="/genom/user/create?role=user" class="btn orange-bg-light">Добавить</a>

                        <?php } ?>

                        <?php if ( $pagination->countPage > 1 ) : ?>

                            <?= $pagination ?>

                        <?php endif; ?>

                    </div>

                    <table class="tbl-info">
                        <thead>
                        <tr>
                            <th>Активность</th>
                            <th>ФИО</th>
                            <th>Логин</th>
                            <th>Преподаватель</th>
                            <th>Регистрация</th>
                            <th>Действия</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach ($users as $item) : ?>
                            <tr>
                                <td><?= ( $item['active'] === '1' ) ? "да" : "нет"; ?></td>
                                <td><?= $item['name'] ?></td>
                                <td><?= $item['login'] ?></td>
                                <td><?= ( array_key_exists($item['id_teacher'], $arrCoach) ) ? $arrCoach[$item['id_teacher']]['name'] : "&mdash;" ?>

                                </td>
                                <td><?= date( "d.m.Y", strtotime($item['create_at']) ) ?></td>
                                <td>
                                    <a href="/genom/user/edit?id=<?= $item['id'] ?>&amp;role=user" class="edit-button"></a>
                                    <a href="/genom/user/delete?id=<?= $item['id'] ?>&amp;role=user" class="delete-button"></a>
                                    <button class="homework" id="homework" onclick="homeworkInfo(<?= $item['id'] ?>);"></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>

                <?php endif; ?>

            </div>

        </div>
    </section>
</main>
