<?php
/**
 * Добавление нового текста для упражнений
 */
?>

<main class="main-admin admin-index">

    <?php include __DIR__ . "/../blocks/_aside.php" ?>

    <section class="materials">

        <div class="container">

            <h2>Добавление нового текста</h2>

            <?php if (isset($error)) : ?>
            <div style="padding: 10px;font-style: italic; text-align: center; color: #800000; background: rgba(128, 0, 0, 0.2);"><?= $error ?></div>
            <?php endif; ?>

            <?php if (isset($success)) : ?>
                <div style="padding: 10px;font-style: italic; text-align: center; color: #007700; background: rgba(0, 119, 0, 0.2);">Данные успешно сохранены.</div>
            <?php endif; ?>

            <form class="practice-text" method="post">
                <div>
                    <label>Язык</label>
                    <select name="lang">
                        <option value="ru" <?= ($_GET['lang'] === 'ru') ? 'selected' : ''?>>Русский</option>
                        <option value="kz" <?= ($_GET['lang'] === 'kz') ? 'selected' : ''?>>Казахский</option>
                        <option value="ua" <?= ($_GET['lang'] === 'ua') ? 'selected' : ''?>>Украинский</option>
                    </select>
                </div>
                <div>
                    <label>Для какого возраста</label>
                    <select name="age">
                        <option value="7" <?= (isset($_POST['age']) && $_POST['age'] === '7') ? 'selected' : ''?>>7-9 лет</option>
                        <option value="10" <?= (isset($_POST['age']) && $_POST['age'] === '10') ? 'selected' : ''?>>10 лет и старше</option>
                    </select>
                </div>
                <div>
                    <label>Название</label>
                    <input type="text" name="title" value="<?= (isset($_POST['title'])) ? $_POST['title'] : ''?>">
                </div>
                <div>
                    <label>Количество слов</label>
                    <input type="number" name="count-words" value="<?= (isset($_POST['count-words'])) ? $_POST['count-words'] : ''?>" required>
                </div>
                <div>
                    <label>Cодержание<br>
                        <span>
                            - Каждый новый абзац должен быть с новой строки.<br>
                            - Перед абзацем пробелы ставить не надо.
                        </span>
                    </label>
                    <textarea name="content-text" required><?= (isset($_POST['content-text'])) ? $_POST['content-text'] : ''?></textarea>
                </div>
                <div>
                    <label>Вопросы для проверки.<br>
                        <span>
                            - Должно быть <strong>не менее 5 вопросов</strong> по тексту.<br>
                            - Каждый вопрос вводится с новой строки.<br>
                            - Ответы на вопрос вводятся сразу после вопроса.<br>
                            - Перед ответом должен стоять дефис (-).<br>
                            - Правильный ответ должен стоять на первом месте.
                        </span>
                    </label>
                    <textarea name="questions" required><?= (isset($_POST['questions'])) ? $_POST['questions'] : ''?></textarea>
                </div>
                <div>
                    <input type="submit"  class='btn' onClick="history.back(); return false;" value="Отменить">
                    <input type="submit" class="btn orange-bg-light" value="Запомнить">
                </div>
            </form>

        </div>

    </section>

</main>
