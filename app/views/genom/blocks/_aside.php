<?php
?>
<aside>
    <input type="checkbox" id="view-menu"><label for="view-menu"></label>
    <div class="logo-wrapper">
        <a href="/">
            <img src="/i/logo.svg" alt="">
        </a>
    </div>

    <div class="admin-menu-body">
        <nav>
            <ul>
                <?php if ( $_SESSION['user']['role'] === '1' ) : ?>
                    <li><a href="/genom">Главная</a></li>
                    <li><a href="/genom/users?role=admin">Администраторы</a></li>
                    <li><a href="/genom/users?role=genom">Преподаватели genom</a></li>
                    <li><a href="/genom/users?role=director">Директора центров</a></li>
                    <li><a href="/genom/users?role=coach">Преподаватели</a></li>
                    <li><a href="/genom/users?role=user">Ученики</a></li>
                    <li>
                        <details>
                            <summary>Материалы</summary>
                            <details>
                                <summary>Тексты</summary>
                                <ul>
                                    <li><a href="/genom/materials/texts?lang=ru">Русский</a></li>
                                    <li><a href="/genom/materials/texts?lang=kz">Казахский</a></li>
                                    <li><a href="/genom/materials/texts?lang=ua">Украинский</a></li>
                                </ul>
                            </details>
                            <ul>
                                <li><a href="/genom/materials/remember-words">Запомни слова (слова)</a></li>
                                <li><a href="/genom/materials/word-search">Поиск слова (слова)</a></li>
                                <li><a href="/genom/materials/chain-words">Цепь слов (слова)</a></li>
                                <li><a href="/genom/materials/running-words">Бегущие слова (слова)</a></li>
                                <li><a href="/genom/materials/words-twins">Слова-близнецы</a></li>
                            </ul>
                        </details>
                    </li>
                <?php endif; ?>

                <?php if ( $_SESSION['user']['role'] === '3' ) : ?>
                    <li><a href="/genom">Главная</a></li>
                    <li><a href="/genom/users?role=coach">Преподаватели</a></li>
                    <li><a href="/genom/users?role=user">Ученики</a></li>
                <?php endif; ?>

                <?php if ( $_SESSION['user']['role'] === '2' || $_SESSION['user']['role'] === '4' ) : ?>
                    <li><a href="/genom">Главная</a></li>
                    <li><a href="/genom/users?role=user">Ученики</a></li>
                <?php endif; ?>
            </ul>
        </nav>
    </div>
</aside>