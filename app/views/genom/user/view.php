<?php
/**
 * @var $user array;
 * @var $labels array;
 */

?>

<main class="main-admin admin-index">

    <?php include __DIR__ . "/../blocks/_aside.php" ?>

    <section class="view-user-wrapper">
        <div class="container">

            <h1><span style="font-size: smaller">Просмотр данных:</span> <?= $user['name']?></h1>

            <p>
                <a href="/genom/user/edit?id=<?= $user['id'] ?>&role=<?= $_GET['role'] ?>" class="btn orange-bg-light">Редактировать</a>
            </p>

            <ul class="view-user">
                <?php foreach ( $user as $key => $value ) : ?>
                <li>
                    <?php if ( $key == 'date_end') $value = date('d.m.Y H:i',$value) ?>
                    <?php if ( $key == 'active' && $value == 0 ) { $value ='не активно'; } else if ( $key == 'active' && $value == 1 ) { $value = 'активно'; } ?>
                    <p class="orange-text-light"><?= $labels[$key] ?></p>
                    <p><?= ($value) ? $value : "&mdash;&mdash;&mdash;" ?></p>
                </li>
                <?php endforeach; ?>
            </ul>

        </div>
    </section>


</main>