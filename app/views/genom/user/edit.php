<?php
/**
 *
 * @var $user array
 * @var $role string
 * @var $coach array
 */


?>

<main class="main-admin admin-index">

    <?php include __DIR__ . "/../blocks/_aside.php" ?>

    <section>
        <div class="container">

            <h1>Редактирование данных <?= $user['name']?></h1>

            <form class="frm-user-info" id="frm-user-info" action="/genom/user/edit?role=<?= $_GET['role'] ?>" method="post">

                <?php if ( isset($_SESSION['errors']) ) : ?>
                    <div class="error-info"><?= $_SESSION['errors'] ?></div>
                    <?php
                endif;
                unset($_SESSION['errors']);
                ?>

                <p>
                    <label for="frm-act">Активность</label>
                    <input type="checkbox" name="active" id="frm-act" <?= ($user['active'] === '1') ? 'checked' :''; ?>>
                </p>
                <p>
                    <label for="frm-name">Имя</label>
                    <input type="text" name="name" id="frm-name" value="<?= $user['name'] ?>">
                </p>
                <p>
                    <label for="frm-login">Логин</label>
                    <input type="text" name="login" id="frm-login" value="<?= $user['login'] ?>">
                </p>
                <p>
                    <label for="edit_password">Изменить пароль</label>
                    <input type="checkbox" name="edit_password" id="edit_password">
                </p>
                <p>
                    <label for="frm-pass">Новый пароль</label>
                    <input type="password" name="pass" id="frm-pass" disabled>
                </p>

                <?php if ( $role !== 'user' && $_SESSION['user']['role'] === "1" ) : ?>
                <p>
                    <label for="role">Привилегии</label>
                    <select name="role" id="role">
                        <option value="1" <?= ($user['role'] === '1') ? 'selected' :''; ?>>Администратор</option>
                        <option value="2" <?= ($user['role'] === '2') ? 'selected' :''; ?>>Преподаватель genom</option>
                        <option value="3" <?= ($user['role'] === '3') ? 'selected' :''; ?>>Другой центр (директор)</option>
                        <option value="4" <?= ($user['role'] === '4') ? 'selected' :''; ?>>Преподаватель</option>
                    </select>
                </p>
                <?php endif; ?>

                <?php if ( $role !== 'user' && $role !== 'admin' && $_SESSION['user']['role'] === "1" ) : ?>
                <p>
                    <label for="frm_coach">Уч. центр (директор)</label for="frm_coach">
                    <select name="parent_id" id="parent_id">
                        <option value="">нет</option>
                        <?php foreach ( $coach as $key => $value) : ?>
                            <option value="<?= $key ?>" <?= ($user['parent_id'] == $key) ? 'selected' :''; ?>><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <?php endif; ?>

                <?php if ( $role === 'coach' || $role === "director" ) { ?>
                    <p>
                        <label for="frm-count">Максимальное количество учеников</label>
                        <input type="number" name="count_users" value="<?= $user['count_users'] ?>" id="frm-count">
                    </p>

                    <p>
                        <label for="frm-payment">Сумма оплаты (в рос. рублях)</label>
                        <input type="text" name="pay_rent" value="<?= $user['pay_rent'] ?>" id="frm-payment">
                    </p>

                    <p>
                        <label for="frm-date-end">Дата окончания подписки</label>
                        <input type="datetime-local" name="date_end" value="<?= date('Y-m-d', $user['date_end']) . 'T' . date('H:i', $user['date_end']) ?>" id="frm-date-end">
                    </p>

                    <p>
                        <label for="frm-email">E-mail</label>
                        <input type="email" name="email" id="frm-email" value="<?= $user['email'] ?>">
                    </p>

                    <p>
                        <label for="frm-phone">Телефон</label>
                        <input type="text" name="phone" value="<?= $user['phone'] ?>" id="frm-phone">
                    </p>

                <?php } ?>

                <?php if ( ($role === 'user' && $_SESSION['user']['role'] === '1') || ($role === 'user' && $_SESSION['user']['role'] === '3') ) : ?>
                    <p>
                        <label for="frm_coach">Преподаватель</label for="frm_coach">
                        <select name="id_teacher" id="id_teacher">
                            <?php foreach ( $coach as $key => $value) : ?>
                                <option value="<?= $key ?>" <?= ($user['id_teacher'] == $key) ? 'selected' :''; ?>><?= $value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </p>
                <?php endif; ?>

                <?php if ( $role === 'user' ) : ?>
                    <p>
                        <label for="frm-birthday">Дата рождения</label>
                        <input type="date" name="birthday" value="<?= (!$user['birthday']) ? date('Y-m-d') : $user['birthday']; ?>" id="frm-birthday">
                    </p>
                <?php endif; ?>

                <?php if ( ($role === 'user' && $_SESSION['user']['role'] === '4') || ($role === 'user' && $_SESSION['user']['role'] === '2') ) : ?>
                    <input type="hidden" name="id_teacher" value="<?= $_SESSION['user']['id'] ?>">
                <?php endif; ?>

                <?php if ( $role === 'coach' && $_SESSION['user']['role'] === '4') : ?>
                    <input type="hidden" name="parent_id" value="<?= $_SESSION['user']['id'] ?>">
                <?php endif; ?>

                <p>
                    <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
                    <input type="submit"  class='btn orange-bg-light' value="Сохранить">
                    <input type="submit"  class='btn' onClick="history.back(); return false;" value="Отменить">
                </p>
            </form>

        </div>
    </section>

</main>