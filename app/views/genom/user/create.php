<?php
/**
 *
 * @var $coach
 * @var $arrCoach
 */

switch ( $_GET['role'] ) {

    case 'admin': $role = 1;
    break;
    case 'genom': $role = 2;
        break;
    case 'director': $role = 3;
        break;
    case 'coach': $role = 4;
        break;
    case 'user': $role = 10;
        break;
}

if ( !empty($coach) ) {

    foreach ($coach as $key => $item) {

        $arrCoach[$key] = $item['name'];

    }

} else {

    $arrCoach = '';

}

?>
<main class="main-admin admin-index">

    <?php include __DIR__ . "/../blocks/_aside.php" ?>

    <section>
        <div class="container">

            <h1>Новый участник</h1>

            <form class="frm-user-info" id="frm-user-info" action="/genom/user/create" method="post">

                <?php if ( isset($_SESSION['errors']) ) : ?>
                    <div class="error-info"><?= $_SESSION['errors'] ?></div>
                    <?php
                endif;
                unset($_SESSION['errors']);
                ?>

                <p>
                    <label for="frm-act">Активность</label>
                    <input type="checkbox" name="active" id="frm-act">
                </p>
                <p>
                    <label for="frm-name">Имя*</label>
                    <input type="text" name="name" id="frm-name">
                </p>
                <p>
                    <label for="frm-login">Логин*</label>
                    <input type="text" name="login" id="frm-login">
                </p>
                <p>
                    <label for="frm-pass">Пароль*</label>
                    <input type="password" name="pass" id="frm-pass">
                </p>

                <?php if ( $role === 3 ) { ?>
                <p>
                    <label for="frm-count">Максимальное количество учеников</label>
                    <input type="number" name="count_users" value="50" id="frm-count">
                </p>

                <p>
                    <label for="frm-payment">Сумма оплаты (в рос. рублях)</label>
                    <input type="text" name="pay_rent" value="500" id="frm-payment">
                </p>

                <p>
                    <label for="frm-date-end">Дата окончания подписки</label>
                    <input type="datetime-local" name="date_end" value="<?= date('Y-m-d') . 'T' . date('H:i') ?>" id="frm-date-end">
                </p>

                <p>
                    <label for="frm-email">E-mail</label>
                    <input type="email" name="email" id="frm-email" value="">
                </p>

                <p>
                    <label for="frm-phone">Телефон</label>
                    <input type="text" name="phone" value="" id="frm-phone">
                </p>

                <?php } ?>

                <?php if ( ($role === 10 && $_SESSION['user']['role'] === '1') || ($role === 10 && $_SESSION['user']['role'] === '3') ) : ?>
                    <p>
                        <label for="frm_coach">Преподаватель</label for="frm_coach">
                        <select name="id_teacher" id="id_teacher">
                            <option value="<?= $_SESSION['user']['id'] ?>"><?= $_SESSION['user']['name'] ?></option>
                            <?php
                            if ( $arrCoach !== '' ) {
                                foreach ($arrCoach as $key => $value) : ?>
                                    <option value="<?= $key ?>"><?= $value ?></option>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </select>
                    </p>
                <?php endif; ?>

                <?php if ( $role === 10 ) : ?>
                    <p>
                        <label for="frm-birthday">Дата рождения</label>
                        <input type="date" name="birthday" value="<?= date('Y-m-d') ?>" id="frm-birthday">
                    </p>
                <?php endif; ?>

                <?php if ( $role === 10 && ($_SESSION['user']['role'] === '4' || $_SESSION['user']['role'] === '2') ) : ?>
                    <input type="hidden" name="id_teacher" value="<?= $_SESSION['user']['id'] ?>">
                <?php endif; ?>

                <?php if ( $role === 4 && $_SESSION['user']['role'] === '3') : ?>
                    <input type="hidden" name="parent_id" value="<?= $_SESSION['user']['id'] ?>">
                <?php endif; ?>

                <?php if ( $role === 4 && $_SESSION['user']['role'] === '1' ) : ?>
                    <p>
                        <label for="frm_coach">Уч. центр (директор)</label for="frm_coach">
                        <select name="parent_id" id="parent_id">
                            <option value="">нет</option>
                            <?php foreach ( $arrCoach as $key => $value) : ?>
                                <option value="<?= $key ?>"><?= $value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </p>
                <?php endif; ?>

                <p>
                    <input type="hidden" name="role" value="<?= $role ?>">
                    <input type="submit"  class='btn orange-bg-light' value="Сохранить">
                    <input type="submit"  class='btn' onClick="history.back(); return false;" value="Отменить">
                </p>
            </form>

        </div>
    </section>

</main>