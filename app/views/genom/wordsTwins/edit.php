<?php
/**
 * Редактирование / добавление слов для упражнения "Слова-близнецы"
 *
 * @var $currentLang array массив с вариантами слов на выбранном языке
 *
 */

$words = implode(',', $words);
?>

<main class="main-admin admin-index">

    <?php include __DIR__ . "/../blocks/_aside.php" ?>

    <section class="materials">

        <div class="container">

            <h2>Добавление/удаление слов на (<?= $currentLang[1] ?> языке</span></h2>
            <p>(упражнение &laquo;Слова-близнецы&raquo;)</p>

            <?php if (isset($error)) : ?>
                <div style="padding: 10px;font-style: italic; text-align: center; color: #800000; background: rgba(128, 0, 0, 0.2);"><?= $error ?></div>
            <?php endif; ?>

            <?php if (isset($success)) : ?>
                <div style="padding: 10px;font-style: italic; text-align: center; color: #007700; background: rgba(0, 119, 0, 0.2);">Данные успешно сохранены.</div>
            <?php endif; ?>

            <form class="practice-text" method="post">
                <div>
                    <label>Язык: <?= $currentLang[0] ?></label>
                    <input type="hidden" name="lang" value="<?= $_GET['lang'] ?>">
                </div>
                <div>
                    <label>Слова<br>
                        <span>
                            - Новые пары слов добавляются в конце списка, через запятую без пробелов.<br>
                            - Между словами в пареставиться символ "-".<br>
                            - Удаляя пары слов старайтесь  не оставляйть лишних запятых и пробелов.<br>
                            - Для исключения повторов слов, перед запоминанием их, производится проверка на повтор.
                        </span>
                    </label>
                    <textarea name="words" required><?= $words ?></textarea>
                </div>
                <div>
                    <input type="submit"  class='btn' onClick="history.back(); return false;" value="Отменить">
                    <input type="submit" class="btn orange-bg-light" value="Запомнить">
                </div>
            </form>

        </div>

    </section>

</main>

