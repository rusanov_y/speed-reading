<?php
/**
 * начальная страница упражнения "Цепь слов"
 *
 * @var $languages
 */

?>

<main class="main-admin admin-index">

    <?php include __DIR__ . "/../blocks/_aside.php" ?>

    <section class="materials">

        <div class="container">

            <h1>Слова для упражнений &laquo;Цепь слов&raquo;</h1>

            <section>
                <?php foreach ($arrWords as $key => $value) : ?>
                <details>
                    <summary>На <?= $languages[$key][1] ?> языке </summary>
                    <div class="basic-info">
                        <a href="/genom/chain-words/edit?lang=<?= $key ?>" class="btn orange-bg-light create-btn">Редактировать</a>
                        <div class="words-columns">
                            <ul>
                            <?php foreach ($value as $item) : ?>
                                <li><?= $item ?></li>
                            <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </details>
                <?php endforeach; ?>
            </section>
        </div>
    </section>
</main>