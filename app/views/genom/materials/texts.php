<?php
/**
 * Вывод названий текстов
 *
 * @var $titles array
 * @var $titles7 array
 */

?>

<main class="main-admin admin-index">

    <?php include __DIR__ . "/../blocks/_aside.php" ?>

    <section class="materials">

        <div class="container">

            <h1>Тексты для упражнений</h1>

            <a href="/genom/texts/create?lang=<?= $_GET['lang'] ?>" class="btn orange-bg-light create-btn">Добавить</a>
            <section>
                <details>
                    <summary>Для 7-9 лет</summary>
                    <div class="basic-info">

                        <table class="tbl-info">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Действия</th>
                            </tr>
                            </thead>

                            <tbody>

                            <?php foreach ($titles7 as $key => $item) : ?>
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= $item ?></td>
                                <td>
                                    <a href="/genom/texts/edit?title=<?= $item ?>&amp;age=7<?= (isset($_GET['lang'])) ? '&amp;lang=' . $_GET['lang'] : '' ?>" class="edit-button"></a>
                                    <a href="/genom/texts/delete?title=<?= $item ?>&amp;age=7<?= (isset($_GET['lang'])) ? '&amp;lang=' . $_GET['lang'] : '' ?>" class="delete-button" onclick="return confirm ('Вы точно хотите удалить этот файл?'); "></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </details>
                <details>
                    <summary>10 лет и старше</summary>
                    <div class="basic-info">

                        <table class="tbl-info">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Действия</th>
                            </tr>
                            </thead>

                            <tbody>

                            <?php foreach ($titles as $key => $item) : ?>
                                <tr>
                                    <td><?= $key + 1 ?></td>
                                    <td><?= $item ?></td>
                                    <td>
                                        <a href="/genom/texts/edit?title=<?= $item ?>&amp;age=10<?= (isset($_GET['lang'])) ? '&amp;lang=' . $_GET['lang'] : '' ?>" class="edit-button"></a>
                                        <a href="/genom/texts/delete?title=<?= $item ?>&amp;age=10<?= (isset($_GET['lang'])) ? '&amp;lang=' . $_GET['lang'] : '' ?>"  class="delete-button"></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </details>

            </section>

        </div>

    </section>


</main>
