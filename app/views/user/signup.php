<?php

?>

<main class="no-fixed-page bg-1">
    <div class="container flex-block">
        <div class="form-guest" id="form-guest">
            <h2>Регистрация</h2>
            <?php if ( isset($_SESSION['errors']) ) : ?>
                <div class="error-info"><?= $_SESSION['errors'] ?></div>
                <?php
            endif;
            unset($_SESSION['errors']);
            ?>
            <form action="" method="post">
                <div class="field-group field-group-text">
                    <p><input type="text" name="login" placeholder="Ваш логин"></p>
                    <p class="info-field"></p>
                </div>
                <div class="field-group field-group-text">
                    <p><input type="password" name="password" placeholder="Ваш пароль"></p>
                    <p class="info-field"></p>
                </div>
                <div class="field-group">
                    <input class="btn orange-bg-light" type="submit" value="Войти">
                </div>
                <p class="orange-text-dark" style="font-weight: 700; text-align: center;">* Гостевой доступ предоставляется на 2 суток</p>
            </form>
        </div>
    </div>
</main>


<!--<main>

    <div style="width: 1000px; margin: 35px auto 0">

        <h2>SIGNUP</h2>

        <form action="/user/signup" method="post">
            <div style="margin-bottom: 15px">
                <label for="login">Login</label>
                <input name="login" type="text" id="login" value="<?/*= isset($_SESSION['form_data']['login']) ? htmlspecialchars($_SESSION['form_data']['login']) : '' */?>">
            </div>
            <div style="margin-bottom: 15px">
                <label for="password">Password</label>
                <input name="password" type="password" id="password">
            </div>
            <div style="margin-bottom: 15px">
                <label for="name">Name</label>
                <input name="name" type="text" id="name" value="<?/*= isset($_SESSION['form_data']['name']) ? htmlspecialchars($_SESSION['form_data']['name']) : '' */?>">
            </div>
            <div style="margin-bottom: 15px">
                <label for="email">E-mail</label>
                <input name="email" type="text" id="email" value="<?/*= isset($_SESSION['form_data']['email']) ? htmlspecialchars($_SESSION['form_data']['email']) : '' */?>">
            </div>
            <div style="margin-bottom: 15px">
                <input type="submit" value="Зарегистрироваться">
            </div>
        </form>

        <?php /*if ( isset($_SESSION['form_data']) ) unset($_SESSION['form_data']); */?>

    </div>

</main>-->
