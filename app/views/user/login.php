<?php
/**
 * @var $languageConstants
 */

?>

<main class="no-fixed-page login-page">
    <div class="container flex-block">
        <div class="form-login" id="form-login">
            <h2><?= $languageConstants['ENTRY'] ?></h2>
            <?php if ( isset($_SESSION['errors']) ) : ?>
            <div class="error-info"><?= $_SESSION['errors'] ?></div>
            <?php
                endif;
                unset($_SESSION['errors']);
            ?>
            <form action="/user/login" method="post">
                <div class="field-group field-group-text">
                    <p><input type="text" name="login" placeholder="<?= $languageConstants['YOURLOGIN'] ?>"></p>
                    <p class="info-field"></p>
                </div>
                <div class="field-group field-group-text">
                    <p><input type="password" name="password" placeholder="<?= $languageConstants['YUORPASS'] ?>"></p>
                    <p class="info-field"></p>
                </div>
                <div class="field-group field-group-checkbox">
                    <input type="checkbox" name="remember" id="remember">
                    <label for="remember"><?= $languageConstants['REMEMBERME'] ?></label>
                </div>
                <div class="field-group field-group-checkbox">
                    <input type="radio" name="role" id="pupil" value="pupil" checked>
                    <label for="pupil"><?= $languageConstants['IAMSTUDENT'] ?></label>
                </div>
                <div class="field-group field-group-checkbox">
                    <input type="radio" name="role" id="teacher" value="teacher">
                    <label for="teacher"><?= $languageConstants['IAMCOACH'] ?></label>
                </div>
                <div class="field-group field-group-checkbox">
                    <input type="radio" name="role" id="guest" value="guest">
                    <label for="guest"><?= $languageConstants['ITRY'] ?></label>
                </div>
                <div class="field-group">
                    <input class="btn orange-bg-light" type="submit" value="<?= $languageConstants['COMEIN'] ?>">
                </div>
            </form>
        </div>
    </div>
</main>