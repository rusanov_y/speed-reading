<?php
/**
 * удаление данных в таблице "ens_homework" с результатами домашнего задания, которые старше 10 дней от сегодняшней даты
 */

$link = mysqli_connect("localhost", "p-18786_trenajer", "gOju2%43", "p-18786_trenajer");

if (!$link) {
    echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
    echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

mysqli_query($link, "DELETE FROM `ens_homework` WHERE `date_home` < DATE_SUB(NOW(), INTERVAL 10 DAY)");