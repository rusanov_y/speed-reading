<?php
/**
 *
 */

use yurus\core\Router;

define('DEBUG', 1);

define('WWW', __DIR__);
define('CORE', dirname(__DIR__) . '/vendor/yurus/core');
define('ROOT', dirname(__DIR__));
define('APP', dirname(__DIR__) . '/app');
define('LAYOUT', 'default');

require '../vendor/yurus/libs/functions.php';
require '../vendor/autoload.php';

$url = $_SERVER['QUERY_STRING'];

new \yurus\core\App();
/**
 * маршрутизация для админской части
 */
Router::add('^genom$', ['controller' => 'Main', 'action' => 'index', 'prefix' => 'genom']);
Router::add('^genom/logout$', ['controller' => 'Main', 'action' => 'logout', 'prefix' => 'genom']);
Router::add('^genom/users$', ['controller' => 'Main', 'action' => 'users', 'prefix' => 'genom']);
Router::add('^genom/?(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$', ['prefix' => 'genom']);

Router::add('^login$', ['controller' => 'Main', 'action' => 'login']);
Router::add('^signup$', ['controller' => 'Main', 'action' => 'signup']);
Router::add('^faq$', ['controller' => 'Main', 'action' => 'faq']);
Router::add('^choice$', ['controller' => 'Main', 'action' => 'choice']);

// дефолтные маршруты пользовательской части
Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');

Router::dispatch($url);