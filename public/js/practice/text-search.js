/**
 * Обработка действий в упражнении Поиск букв
 *
 * @lang string - выбранный язык
 * @var arrLetters array - массив значений букв выводимых в таблице
 * @var arrLevels array - настройки уровней: 0 - размер поля, 1 - кол-во слов для поиска при одном показе текста,
 * 2 - размер шрифта, 3 - высота строки, 4 - время отводимое на прохождение уровня в миллисекундах
 * @var settingForm DOM-element - форма с настройками тренировки
 * @var startWrapper DOM-element - блок кнопки Старт
 * @var tblWrapper DOM-element - блок с таблицей
 * @var startWrapper DOM-element - блок счётчика перед началом показа
 * @var currentAction DOM-element - строка где показывается, что надо сделать (Нажмите на кнопку, нажмите на букву)
 * @var infoBlock DOM-element - строка где показывается какие слова найти
 * @var practiceWrapper DOM-element - блок упражнения
 * @var levelPractice integer - текущий уровень сложности
 * @var countSuccess integer -
 * @var countError integer -
 * @var rememberLevelStart integer - уровень, с которого началась тренировка
 * @var currentSearchWords array - текущие слова для поиска
 * @var arrWords array - все слова
 * @var arrLetters array - алфавит
 * @var arrPos array
 */

'use strict';

var arrLevels = {
    1: [200, 4, 18, 25, 20000],
    2: [250, 4, 18, 25, 16000],
    3: [300, 5, 18, 25, 20000],
    4: [400, 5, 18, 25, 17500],
    5: [264, 6, 16, 22, 24000],
    6: [308, 6, 16, 22, 21000],
    7: [266, 6, 14, 19, 21000],
    8: [304, 6, 14, 19, 18000],
    9: [256, 7, 12, 16, 24500],
    10: [288, 7, 12, 16, 21000]
};

var timePractice,
    levelPractice,
    countSignText = 0,
    currentSearchWords = [],
    numberBeginParagraph = [],
    arrWords = [];

var settingWrapper = document.getElementById('setting-wrapper');
var settingForm = document.getElementById('setting-form');
var titleBody = document.getElementById('setting-title');
var textWrapper = document.getElementById('text-wrapper');
var textIso = document.getElementById('text-iso');
var practiceWrapper = textWrapper.parentNode;
var currentAction = practiceWrapper.querySelector('#currentAction');
var infoBlock = practiceWrapper.querySelector('#info-block');
var currentText = textWrapper.querySelector('div');
var timePracticeBody = settingForm.querySelector('[name=setting-time]');
var levelPracticeBody = settingForm.querySelector('[name=setting-level]');

timePractice = timePracticeBody.value;
levelPractice = levelPracticeBody.value

timePracticeBody.addEventListener('change', function (e) {
    timePractice = e.target.value;
    timePractice = +timePractice;
});

levelPracticeBody.addEventListener('change', function (e) {
    levelPractice = e.target.value;
    levelPractice = +levelPractice;
});

levelPractice = +levelPractice;
rememberLevelStart = levelPractice;
timePractice = +timePractice;

practiceWrapper.style.marginLeft = ((-1) * practiceWrapper.offsetWidth / 2) + 'px';


/**
 * Получение данных текста
 * - количество слов (включая предлоги, тире) в тексте
 * - массив всех слов
 * - массив номеров слов, с которых начанается каждый параграф
 */
function textData() {

    var paragraphsText = textIso.querySelectorAll('p');
    var tmpArray = [];
    arrWords.length = 0;
    numberBeginParagraph.length = 0;

    var c = 0;
    paragraphsText.forEach(function (value, key) {

        var currentValue = value.textContent;
        tmpArray[key] = currentValue.split(' ');

        arrWords = arrWords.concat(tmpArray[key]);

        if (key !== 0) {
            c += tmpArray[key-1].length;
            numberBeginParagraph.push(c);
        }

    });

    countSignText = arrWords.length;

}

textData();

// получение нового текста, при изменении в настройках
function getText(idText) {

    var req = getXmlHttp();
    var sendLetters = "new_title=" + idText;
    req.open('POST', '/practice/text-search', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {

                var arrNewData = JSON.parse(req.responseText);
                var str = "";

                for (var i = 0; i < arrNewData['text']['p'].length; i++) {

                    str += "<p>" + arrNewData['text']['p'][i] + "</p>";
                }

                textIso.innerHTML = str;

                textData();
            }
        }
    };
    req.send(sendLetters);

}

titleBody.addEventListener('change', function (e) {

    getText(e.target.value);

});

/**
 * Подготовка текста перед показом
 * - устанавливаем параметры блока, в зависимости от уровня
 * - разбиваем текст по словам
 * - удаляем спецсимволы
 * - оборачиваем слова в span
 *
 */
function blockFilling() {

    var rmd,
        word;

    textWrapper.style.width = arrLevels[levelPractice][0] + 'px';
    textWrapper.style.height = arrLevels[levelPractice][0] + 'px';
    textWrapper.style.fontSize = arrLevels[levelPractice][2] + 'px';
    textWrapper.style.marginTop = Math.floor((practiceWrapper.offsetHeight - infoBlock.offsetHeight - 21 - arrLevels[levelPractice][0]) / 2) + 'px';

    // выбираем позицию с которой будет показываться текст
    do {

        rmd = Math.floor(Math.random() * (countSignText - 40) );

    } while (arrWords[rmd] === '-');


    // проходим циклом по массиву всех слов текста до тех пор пока высота текста не будет больше
    // высоты блока
    while (currentText.offsetHeight < arrLevels[levelPractice][0] + arrLevels[levelPractice][3]) {

        // проверяем длину слова, если больше 4, то запоминаем, оборачиваем в span
        // запятые, точки выносим за пределы span
        if (arrWords[rmd].length < 4) {

            word = arrWords[rmd] + " ";

        } else {

            // выбираем первый и последний символ текущего слова
            var currentWord = arrWords[rmd];
            var firstLetter = currentWord[0];
            var endLetter = currentWord[currentWord.length - 1];
            var startWord = '';
            var finishWord = '';

            // если первый символ является спецсимволом,
            // то отделяем его от слова
            switch(firstLetter) {
                case '"':
                case '(':
                case '«':
                    currentWord = currentWord.slice(1);
                    startWord = firstLetter;
                    break;
            }

            // если последний символ является спецсимволом
            // отделяем его от слова
            switch(endLetter) {
                case ')':
                case '"':
                case '»':
                    currentWord = currentWord.slice(0, -1);
                    finishWord = endLetter;
                    break;
                case '.':
                case ';':
                case ':':
                case ',':
                case '!':
                case '?':
                    currentWord = currentWord.slice(0, -1);
                    finishWord = endLetter;

                    // если последний спецсимвол  в слове
                    endLetter = currentWord[currentWord.length - 1];

                    switch (endLetter) {
                        case ')':
                        case '"':
                        case '»':
                            currentWord = currentWord.slice(0, -1);
                            finishWord = endLetter + finishWord;
                            break;
                        case '.':
                            currentWord = currentWord.slice(0, -1);
                            finishWord = endLetter + finishWord;

                            endLetter = currentWord[currentWord.length - 1];

                            if (endLetter === ".") {
                                currentWord = currentWord.slice(0, -1);
                                finishWord = endLetter + finishWord;
                            }
                            break;
                    }
                    break;
            }

            word = startWord + "<span>" + currentWord + "</span>" + finishWord + " ";
            currentSearchWords.push(currentWord);

        }

        var strText = currentText.innerHTML;

        if (numberBeginParagraph.indexOf(rmd) === -1) {

            currentText.innerHTML = strText + word;

        } else {

            if (currentText.offsetHeight === 0) {

                currentText.innerHTML = strText + '<span>&nbsp;&nbsp;</span>' + word;

            } else {

                currentText.innerHTML = strText + '<br><span>&nbsp;&nbsp;</span>' + word;

            }

        }

        rmd++;

    }

    currentSearchWords = currentSearchWords.slice(0, -1);
    shuffle(currentSearchWords);

}

/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    settingWrapper.style.display = 'none';

    practiceStart();
    setTimeout(finishPractice, timePractice);
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 */
function practiceStart() {

    currentText.innerHTML = '';
    currentSearchWords.length = 0;

    blockFilling();

    exercise();

}

/**
 * Показ упражнения
 *
 * @var startTime integer время начала показа
 * @var finishTime integer время, когда найдено последнее слово
 * @var travelTime integer время за которое были найдены все слова
 * @var collectionWordsForSearch collection коллекция слов в инфоблоке
 */
function exercise() {

    var startTime,
        finishTime,
        travelTime,
        errorAnswer = false;

    // скрываем блок обратного отсчёта
    // показываем блок упражнения
    startWrapper.classList.remove('active');
    practiceWrapper.style.zIndex = '0';

    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    infoBlock.innerHTML = currentSearchWords[0];
    timeLine.classList.add('active');

    startTime = Date.now();

    var i = 0;

    // обрабатываем нажатие на слова
    textWrapper.onclick = function (e) {

        var elem = e.target;

        if (elem.textContent !== infoBlock.innerHTML) {

            errorAnswer = true;

        }

        i++;

        if (i < arrLevels[levelPractice][1]) {

            infoBlock.innerHTML = currentSearchWords[i];

        } else {

            finishTime = Date.now();
            travelTime = finishTime - startTime;

            if (errorAnswer === false && travelTime <= arrLevels[levelPractice][4]) {

                levelPractice++;

            } else {

                countError++;

                if (countError === 2) {

                    levelPractice--;
                    countError = 0;

                }

            }

            return practiceStart();

        }

    };

}

/**
 * действия после окончания времени тренировки
 */
function finishPractice() {

    var dataRemember = "level=" + levelPractice + '&title=' + titleBody.value;

    rememberLevel('text-search', dataRemember);
    practiceWrapper.style.zIndex = '-2';
    document.getElementById('finish-practice').classList.add('active');

}
