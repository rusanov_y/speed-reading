/**
 * Обработка действий в упражнении Поиск букв
 *
 * @const arrLetters array - массив значений букв выводимых в таблице
 * @const arrLevels array - многомерный массив в котором указываются настройки уровней: 0 - кол-во ячеек в таблице, 1 - кол-во столбцов, 2 - количество знаков, 3 - размер шрифта, 4 - время на прохождение уровня
 * @var settingForm DOM-element - форма с настройками тренировки
 * @var startWrapper DOM-element - блок кнопки Старт
 * @var tblWrapper DOM-element - блок с таблицей
 * @var counterStart DOM-element - блок счётчика перед началом показа
 * @var currentAction DOM-element - строка где показывается, что надо сделать (Нажмите на кнопку, нажмите на букву)
 * @var currentElementClick DOM-element - строка где показывается какую цифру(букву) сейчас надо нажать
 * @var practiceWrapper DOM-element - блок упражнения
 */


'use strict';

var arrLevels = {
    1: [4, 4, 5, 40, 15000],
    2: [4, 5, 5, 34, 15000],
    3: [4, 6, 5, 32, 15000],
    4: [5, 5, 5, 34, 15000],
    5: [5, 6, 5, 32, 15000],
    6: [5, 7, 5, 30, 15000],
    7: [5, 8, 5, 28, 15000],
    8: [6, 7, 5, 30, 15000],
    9: [6, 8, 5, 28, 15000],
    10: [6, 9, 5, 26, 15000]
};

var countLetters = 0,
    controlTimeTable = 0,
    coupleLetter,
    countElements,
    currentLetter = [],
    arrCoupleLetter = [],
    arrResultTable = [],
    arrLetters = [],
    arrPos = [];

var ageUser = sessionStorage.getItem('userAge');
var settingForm = document.getElementById('setting-form');
var settingWrapper = settingForm.parentElement;
var tblBody = tblWrapper.querySelector('tbody');
var currentAction = practiceWrapper.querySelector('#currentAction');
var currentElementClick = practiceWrapper.querySelector('#currentElementClick');
var timePracticeBody = settingForm.querySelector('[name=setting-time]');
var levelPracticeBody = settingForm.querySelector('[name=setting-level]');
var coupleLetterBody = settingForm.querySelector('[name=couple-letter]');

timePractice = timePracticeBody.value;
levelPractice = levelPracticeBody.value;
coupleLetter = coupleLetterBody.value;

timePractice = +timePractice;
levelPractice = +levelPractice;
rememberLevelStart = levelPractice;

timePracticeBody.addEventListener('change', function (e) {
    var tp = e.target;
    timePractice = tp.value;
    timePractice = +timePractice;
});

levelPracticeBody.addEventListener('change', function (e) {
    var lp = e.target;
    levelPractice = lp.value;
    levelPractice = +levelPractice;
});

coupleLetterBody.addEventListener('change', function (e) {
    var cl = e.target;
    cl = cl.value;
    coupleLetter = cl;
    arrLevels = {
        1: [4, 4, 8, 40, 15000],
        2: [4, 5, 8, 34, 15000],
        3: [4, 6, 10, 32, 15000],
        4: [5, 5, 10, 34, 15000],
        5: [5, 6, 12, 32, 15000],
        6: [5, 7, 12, 30, 15000],
        7: [5, 8, 15, 28, 15000],
        8: [6, 7, 15, 30, 15000],
        9: [6, 8, 17, 28, 15000],
        10: [6, 9, 19, 26, 15000]
    };
    getCoupleLetter(cl);
});

/**
 * Получение алфавита
 */
function getAlphabet() {

    var req = getXmlHttp();
    var sendData = "alphabet=true";
    req.open('POST', '/practice/letter-search', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                arrLetters = req.responseText.split(',');
            }
        }
    };
    req.send(sendData);

}

getAlphabet();

/**
 * Получение данных для определённых пар букв
 */
function getCoupleLetter(c) {

    var req = getXmlHttp();
    var sendData = "couple=" + c;
    req.open('POST', '/practice/letter-search', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                var strCouple = req.responseText;
                arrCoupleLetter = strCouple.split(';');
            }
        }
    };
    req.send(sendData);

}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    countLetters = arrLevels[levelPractice][2];
    controlTimeTable = arrLevels[levelPractice][4];
    countElements = arrLevels[levelPractice][0] * arrLevels[levelPractice][1];

    // удаляем классы, помогающие придать ячейкам одинаковый размер
    tblWrapper.classList.remove('col-' + (arrLevels[levelPractice][2] - 1));
    tblWrapper.classList.remove('col-' + arrLevels[levelPractice][2]);
    tblWrapper.classList.remove('col-' + (arrLevels[levelPractice][2] + 1));

    // копируем исходный массив
    var arrSort = arrLetters.slice();

    if (coupleLetter !== 'no') {

        var numberCurrentLetter = Math.floor(Math.random() * arrCoupleLetter.length);

        if (arrCoupleLetter[numberCurrentLetter].length > 1) {
            currentLetter = arrCoupleLetter[numberCurrentLetter].split(',');
        } else {
            currentLetter = arrCoupleLetter[numberCurrentLetter];
        }

    } else {

        var q = Math.floor(Math.random() * 20);

        if (q < 14) {
            numberCurrentLetter = Math.floor(Math.random() * (arrSort.length + 1));
            currentLetter = arrSort.splice(numberCurrentLetter, 1);
        } else {
            numberCurrentLetter = Math.floor(Math.random() * (arrSort.length + 1));
            currentLetter = arrSort.splice(numberCurrentLetter, 1);
            var numberCurrentLetter2 = Math.floor(Math.random() * (arrSort.length + 1));
            currentLetter = currentLetter.concat(arrSort.splice(numberCurrentLetter2, 1));
        }

    }

    // сортируем массив в случайном порядке
    shuffle(arrSort);

    tableFilling(arrSort);

}

/**
 * Заполнение таблицы значениями
 * - сортируем массив со значениями в случайном порядке
 * - в зависимости от уровня сложности определяем количество ячеек в строке (кол-во div),
 * - также вставляются, при необходимости, ячейки с пустыми значениями
 * - в цикле добавляем ячейки (div) в блок с таблицей
 *
 * @param arrSort - отсортированный в случайном порядке массив значений
 */
function tableFilling(arrSort) {

    // очищаем старые данные
    tblBody.innerHTML = '';

    // добавляем таблице класс по количеству ячеек
    tblWrapper.classList.add('col-' + arrLevels[levelPractice][0]);

    var arrTemp = arrSort.slice(0, countElements);

    // Выбор случайных позиций массива и замена в них значений на букву, которую ищет пользователь
    var k;

    function insertLetter() {

        for (var n = 0; n < countLetters;) {
            k = Math.floor(Math.random() * countElements);

            if (arrPos.indexOf(k) === -1) {
                arrPos[n] = k;
                n++;
            }
        }

        arrPos.sort(function(a, b) { return a - b; });

        for (var c = 0; c < countLetters - 1; c++) {
            if (arrPos[c] + 1 === arrPos[c + 1]) {
                if (arrPos[c] + 2 === arrPos[c + 2]) {
                    insertLetter();
                    break;
                }
            }
        }
    }

    insertLetter();

    for (var j = 0; j < countLetters; j++) {
        if (currentLetter.length > 1) {
            q = Math.floor(Math.random() * 20);
            arrTemp[arrPos[j]] = (q > 10) ? currentLetter[0] : currentLetter[1];
        } else {
            arrTemp[arrPos[j]] = currentLetter[0];
        }
    }

    // заполняем таблицу рядами и ячейками
    var n = 0;
    for (var q = 0; q < arrLevels[levelPractice][1]; q++) {
        var row = document.createElement('tr');
        tblBody.append(row);
        for (var r = 0; r < arrLevels[levelPractice][0]; r++) {
            var newTd = document.createElement('td');
            newTd.innerHTML = arrTemp.pop();
            if (newTd.innerHTML === currentLetter[0] || newTd.innerHTML === currentLetter[1]) {
                newTd.setAttribute('id', 'el-' + n);
                n++;
            }
            row.append(newTd);
        }
    }

    exercise();

}

/**
 * Показ упражнения
 * - скрываем блок со счётчиком
 * - показываем блок с таблицей
 * - выводим в верхнем блоке первый элемент, на который надо нажать
 * - запоминаем время начала показа
 * - отслеживаем событие клика по таблице
 * - при клике определяем по какому элементу таблицы был клик
 * - сравниваем значение элемента с текущим значением массива
 * - при совпадении переходим к следующему элементу (увеличиваем счётчик)
 * - проверяем это был последний элемент или нет
 * - если не последний элемент, показываем, какой следующий элемент надо нажать
 * - если последний, то запоминаем время окончания показа
 * - высчитываем время, затраченное на прохождение
 * - если время меньше 20 сек, то увеличиваем уровень сложности (после того как пользователь
 * два раза пройдёт текущий уровень быстрее 20 сек)
 * - если время больше 20 сек., то уменьшаем уровень сложности.
 * - перезапускаем показ перемешав элементы.
 *
 * @var startTime
 * @var finishTime
 * @var travelTime
 */
function exercise() {

    var startTime,
        finishTime,
        travelTime;

    arrPos.length = 0;

    settingWrapper.style.display = 'none';

    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    tblWrapper.style.fontSize = arrLevels[levelPractice][3] + "px";
    counterStart.classList.remove('active');
    tblWrapper.parentNode.classList.add('active');
    tblWrapper.style.opacity = '1';
    currentElementClick.innerHTML = currentLetter;
    timeLine.classList.add('active');

    startTime = Date.now();

    var i = 0;

    // обработка клика по элементам таблицы с буквами
    tblWrapper.onclick = function (e) {

        // элемент по которому произошёл клик
        var el = e.target;

        // проверка налижия такого элемента в массиве с элементами, которые надо найти
        if ( currentLetter.indexOf(el.innerHTML) !== -1 ) {

            // проверяем, был клие на этом элементе или нет
            if ( arrPos.indexOf(el.id) === -1 ) {
                //если клика на этом элементе не было, заносим его в массив с номерами элементов, на которых уже был клик
                arrPos[i] = el.id;
                //увеличиваем счётчик, правильно нажатых элементов
                i++;
                //скрываем элемент
                el.style.opacity = '0';

                // проверяем какой по счёту элемент нажат
                if (i === countLetters) {
                    // если по счёту это заключительный элемент, то
                    // фиксируем время окончания
                    // высчитываем, какое время потрачено на прохождение таблицы
                    finishTime = Date.now();
                    travelTime = finishTime - startTime;
                    // заносим в таблицу результатов время прохождения таблицы и скорость
                    arrResultTable.push([travelTime, countLetters / (travelTime / 1000)]);
                    // проверяем время прохождения таблицы, меньше контрольного или нет
                    if (travelTime < controlTimeTable) {
                        // время меньше контрольного прохождения
                        // увеличиваем счётчик пройденных подряд таблиц меньше данного времени
                        countSuccess++;
                        // обнуляем счётчик с количеством пройденных таблиц боьше контрольеого времени
                        countError = 0;
                        // если пройдено 5 раз подряд быстрее контрольного времени
                        if ( countSuccess > 4 ) {

                            // проеряем, чтобы был не последний уровень
                            if (levelPractice !== Object.keys(arrLevels).length) {
                                // обнуляем счётчик успешного прохождения таблиц
                                countSuccess = 0;
                                // переходим на следующий уровень
                                levelPractice++;
                                // сообщаем пользователю, что он перешёл на следующий уровень
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#007700';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }

                    } else {
                        // время больше контрольного прохождения
                        // увеличиваем счётчик пройденных подряд таблиц больше контрольного времени
                        countError++;
                        // обнуляем счётчик пройденных подряд таблиц меньше контрольного времени
                        countSuccess = 0;
                        // если пройдено подряд 3 раза больше контрольного времени
                        if (countError > 2) {
                            // обнуляем счётчик пройденных подряд таблиц больше контрольного времени
                            countError = 0;
                            //если не 1 уровень
                            if (levelPractice !== 1) {
                                //переходим на уровень ниже
                                levelPractice--;
                                // сообщаем пользователю о переходе на более низкий уровень
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#dd0000';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }
                    }
                    // запускаем новую таблицу
                    return practiceStart();
                }

            }

        }

    };

}

/**
 * Обработка результатов выполнения упражнения
 * - запоминание уровня, на котором остановил пользователь
 * - вычисление баллов за упражнение
 * - построение диаграммы
 */
function finishPractice() {

    if (coupleLetter === 'no') {
        rememberLevel('letter-search');
    }

    var arrLabel = [],
        arrValue = [],
        t = 0,
        ef = 0;

    arrResultTable.forEach(function (item, index) {
        arrLabel.push(index + 1);
        arrValue.push(item[1].toFixed(2));
        t += item[0];
    });

    ef = Math.round(t / arrResultTable.length);

    var blockEfficiency = finishPracticeBody.querySelector('p.result-practice');

    if (ef <= 5000) {
        blockEfficiency.innerHTML = '5 баллов';
    }

    if ( (ageUser < 11 && (ef > 5000 && ef <= 9000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 5000 && ef <= 8000)) || ((ageUser >= 12 || !ageUser) && (ef > 5000 && ef <= 7000)) ) {
        blockEfficiency.innerHTML = '4 балла';
    }

    if ( (ageUser < 11 && (ef > 9000 && ef <= 14000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 8000 && ef <= 12000)) || ((ageUser >= 12 || !ageUser) && (ef > 7000 && ef <= 11000)) ) {
        blockEfficiency.innerHTML = '3 балла';
    }

    if ( (ageUser < 11 && (ef > 14000 && ef <= 20000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 12000 && ef <= 17000)) || ((ageUser >= 12 || !ageUser) && (ef > 11000 && ef <= 15000)) ) {
        blockEfficiency.innerHTML = '2 балла';
    }

    if ( (ageUser < 11 && ef > 20000) || ((ageUser >= 11 && ageUser < 12) && ef > 17000) || ((ageUser >= 12 || !ageUser) && ef > 15000) ) {
        blockEfficiency.innerHTML = '1 балл';
    }

    new Chartist.Line('.ct-chart', {
        labels: arrLabel,
        series: [
            arrValue
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 20,
            left: 0
        },
        // Настройки X-оси
        axisX: {
            // Отключаем сетку для этой оси
            showGrid: false
        },
        // настройки Y-оси
        axisY: {
            showGrid: false
        }
    });

    practiceWrapper.classList.remove('active');
    finishPracticeBody.classList.add('active');
}

/**
 * действия при нажатии кнопки повторить
 */
function repeatPractice() {

    timeLine.style.mozTransitionDuration = '0ms';
    timeLine.style.oTransitionDuration = '0ms';
    timeLine.style.transitionDuration = '0ms';

    settingWrapper.style.display = 'block';
    timeLine.classList.remove('active');

    levelPractice = rememberLevelStart;
    countSuccess = 0;
    countError = 0;
    finishPracticeBody.classList.remove('active');
    startWrapper.classList.add('active');

}


