/**
 * Обработка действий в упражнении Поиск букв
 *
 * @const arrLetters array - массив значений букв выводимых в таблице
 * @const arrLevels array - многомерный массив в котором указываются настройки уровней: 0 - кол-во ячеек в таблице, 1 - кол-во столбцов, 2 - количество знаков, 3 - размер шрифта, 4 - кол-во цветных элементов
 * @var settingForm DOM-element - форма с настройками тренировки
 * @var startWrapper DOM-element - блок кнопки Старт
 * @var tblWrapper DOM-element - блок с таблицей
 * @var counterStart DOM-element - блок счётчика перед началом показа
 * @var currentAction DOM-element - строка где показывается, что надо сделать (Нажмите на кнопку, нажмите на букву)
 * @var currentElementClick DOM-element - строка где показывается какую цифру(букву) сейчас надо нажать
 * @var practiceWrapper DOM-element - блок упражнения
 */


'use strict';

var arrLevels = {
    1: [6, 6, 5, 32, 15000],
    2: [6, 7, 5, 30, 15000],
    3: [6, 8, 5, 28, 15000],
    4: [6, 9, 5, 26, 15000],
    5: [6, 10, 5, 24, 15000],
    6: [7, 9, 5, 26, 15000],
    7: [7, 10, 5, 24, 15000],
    8: [7, 11, 5, 22, 15000]
};

var currentDigital,
    countElements,
    arrPos = [],
    arrResultTable = [];

var ageUser = sessionStorage.getItem('userAge');
var settingForm = document.getElementById('setting-form');
var settingWrapper = settingForm.parentElement;
var tblBody = tblWrapper.querySelector('tbody');
var currentAction = practiceWrapper.querySelector('#currentAction');
var currentElementClick = practiceWrapper.querySelector('#currentElementClick');
var timePracticeBody = settingForm.querySelector('[name=setting-time]');
var levelPracticeBody = settingForm.querySelector('[name=setting-level]');

timePractice = timePracticeBody.value;
levelPractice = levelPracticeBody.value;

timePracticeBody.addEventListener('change', function (e) {
    timePractice = e.target.value;
    timePractice = +timePractice;
});

levelPracticeBody.addEventListener('change', function (e) {
    levelPractice = e.target.value;
    levelPractice = +levelPractice;
});

levelPractice = +levelPractice;
rememberLevelStart = levelPractice;
timePractice = +timePractice;

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    countElements = arrLevels[levelPractice][0] * arrLevels[levelPractice][1];

    // удаляем классы, помогающие придать ячейкам одинаковый размер
    tblWrapper.classList.remove('col-' + (+arrLevels[levelPractice][2] - 1));
    tblWrapper.classList.remove('col-' + arrLevels[levelPractice][2]);
    tblWrapper.classList.remove('col-' + (+arrLevels[levelPractice][2] + 1));

    currentDigital = Math.floor(100 + Math.random() * 9900);

    if (currentDigital / 1000 < 1) {
        currentDigital = String(currentDigital);
        currentDigital = "0" + currentDigital;
    } else {
        currentDigital = String(currentDigital);
    }

    tableFilling();

    exercise();
}

/**
 * Заполнение таблицы значениями
 * - сортируем массив со значениями в случайном порядке
 * - в зависимости от уровня сложности определяем количество ячеек в строке (кол-во div),
 * - также вставляются, при необходимости, ячейки с пустыми значениями
 * - в цикле добавляем ячейки (div) в блок с таблицей
 *
 */
function tableFilling() {

    // очищаем старые данные
    tblBody.innerHTML = '';
    arrPos.length = 0;

    // добавляем таблице класс по количеству ячеек
    tblWrapper.classList.add('col-' + arrLevels[levelPractice][0]);


    countElements = arrLevels[levelPractice][0] * arrLevels[levelPractice][1];

    // во временный массив заносим числа от 0 до 9, в таком количестве
    // чтобы было больше, чем пустых ячеек в таблице
    var arrTemp = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    for (var j = 0; arrTemp.length < countElements; j++) {

        arrTemp = arrTemp.concat([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
    }

    arrTemp = shuffle(arrTemp);

    //определяем строки и позицию числа.
    while (arrPos.length < arrLevels[levelPractice][2]) {
        var rand = Math.floor(Math.random() * arrLevels[levelPractice][1]);
        if (arrPos.indexOf(rand) === -1) {
            arrPos.push(rand);
        }
    }

    arrPos.sort(function(a, b) { return a - b; });

    var objPos = {};
    for (var n = 0; n < arrPos.length; n++) {
        objPos[arrPos[n]] = Math.floor(Math.random() * (arrLevels[levelPractice][0] - 3));
    }

    for (var q = 0; q < arrLevels[levelPractice][1]; q++) {
        var row = document.createElement('tr');
        tblBody.append(row);
        var r = 0;
        while (r < arrLevels[levelPractice][0]) {

            var newTd = document.createElement('td');
            newTd.innerHTML = arrTemp.pop();
            row.append(newTd);
            r++;
        }
    }


    var arrRow = tblBody.rows;
    for (var w = 0; w < arrRow.length; w++) {
        if (objPos[w] !== undefined) {
            var cellsRow = arrRow[w].cells;
            var posInsert = objPos[w];
            cellsRow[posInsert].innerHTML = currentDigital[0];
            cellsRow[posInsert].setAttribute('data-number', w);
            cellsRow[posInsert + 1].innerHTML = currentDigital[1];
            cellsRow[posInsert + 1].setAttribute('data-number', w);
            cellsRow[posInsert + 2].innerHTML = currentDigital[2];
            cellsRow[posInsert + 2].setAttribute('data-number', w);
            cellsRow[posInsert + 3].innerHTML = currentDigital[3];
            cellsRow[posInsert + 3].setAttribute('data-number', w);
        }
    }

}

/**
 * Показ упражнения
 * - скрываем блок со счётчиком
 * - показываем блок с таблицей
 * - выводим в верхнем блоке первый элемент, на который надо нажать
 * - запоминаем время начала показа
 * - отслеживаем событие клика по таблице
 * - при клике определяем по какому элементу таблицы был клик
 * - сравниваем значение элемента с текущим значением массива
 * - при совпадении переходим к следующему элементу (увеличиваем счётчик)
 * - проверяем это был последний элемент или нет
 * - если не последний элемент, показываем, какой следующий элемент надо нажать
 * - если последний, то запоминаем время окончания показа
 * - высчитываем время, затраченное на прохождение
 * - если время меньше 20 сек, то увеличиваем уровень сложности (после того как пользователь
 * два раза пройдёт текущий уровень быстрее 20 сек)
 * - если время больше 20 сек., то уменьшаем уровень сложности.
 * - перезапускаем показ перемешав элементы.
 *
 * @var startTime
 * @var finishTime
 * @var travelTime
 */
function exercise() {

    var startTime,
        finishTime,
        travelTime;

    arrPos.length = 0;

    settingWrapper.style.display = 'none';
    timeLine.style.display = 'block';

    timeLine.style.webkitTransitionDuration = timePractice + 'ms';
    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    tblWrapper.style.fontSize = arrLevels[levelPractice][3] + "px";
    counterStart.classList.remove('active');
    tblWrapper.parentNode.classList.add('active');
    tblWrapper.style.opacity = '1';
    currentElementClick.innerHTML = currentDigital;
    timeLine.classList.add('active');

    startTime = Date.now();

    var i = 0;
    tblWrapper.onclick = function (e) {

        var el = e.target;
        if ( el.dataset.number !== undefined ) {

            if ( arrPos.indexOf(el.dataset.number) === -1 && arrPos.indexOf(el.dataset.number) !== 'no-opacity' ) {

                arrPos[i] = el.dataset.number;
                i++;
                var clickElements = tblBody.querySelectorAll('td[data-number="' + el.dataset.number + '"]');
                for (var c = 0; c < clickElements.length; c++) {
                    clickElements[c].style.opacity = '0';
                    clickElements[c].removeAttribute('data-number');
                }

                if (i === arrLevels[levelPractice][2]) {
                    finishTime = Date.now();
                    travelTime = finishTime - startTime;
                    arrResultTable.push([travelTime, arrLevels[levelPractice][2] / (travelTime / 1000)]);

                    if ( travelTime < arrLevels[levelPractice][4]) {
                        countSuccess++;

                        if ( countSuccess > 4) {

                            if ( levelPractice !== Object.keys(arrLevels).length ) {
                                countSuccess = 0;
                                levelPractice++;
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#007700';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }

                    } else {
                        countSuccess = 0;
                        countError++;

                        if ( countError > 2 ) {
                            countError = 0;

                            if ( levelPractice !== 1 ) {
                                levelPractice--;
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#dd0000';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }
                    }

                    return practiceStart();
                }

            }

        }
    };

}

function finishPractice() {

    rememberLevel('digital-search');

    var arrLabel = [],
        arrValue = [],
        t = 0,
        ef = 0;

    arrResultTable.forEach(function (item, index) {
        arrLabel.push(index + 1);
        arrValue.push(item[1].toFixed(2));
        t += item[0];
    });

    ef = Math.round(t / arrResultTable.length);
console.log(ef);
    var blockEfficiency = finishPracticeBody.querySelector('p.result-practice');

    if (ef <= 5000) {
        blockEfficiency.innerHTML = '5 баллов';
    }

    if ( (ageUser < 11 && (ef > 5000 && ef <= 9000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 5000 && ef <= 8000)) || ((ageUser >= 12 || !ageUser) && (ef > 5000 && ef <= 7000)) ) {
        blockEfficiency.innerHTML = '4 балла';
    }

    if ( (ageUser < 11 && (ef > 9000 && ef <= 14000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 8000 && ef <= 12000)) || ((ageUser >= 12 || !ageUser) && (ef > 7000 && ef <= 11000)) ) {
        blockEfficiency.innerHTML = '3 балла';
    }

    if ( (ageUser < 11 && (ef > 14000 && ef <= 20000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 12000 && ef <= 17000)) || ((ageUser >= 12 || !ageUser) && (ef > 11000 && ef <= 15000)) ) {
        blockEfficiency.innerHTML = '2 балла';
    }

    if ( (ageUser < 11 && ef > 20000) || ((ageUser >= 11 && ageUser < 12) && ef > 17000) || ((ageUser >= 12 || !ageUser) && ef > 15000) ) {
        blockEfficiency.innerHTML = '1 балл';
    }

    new Chartist.Line('.ct-chart', {
        labels: arrLabel,
        series: [
            arrValue
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 20,
            left: 0
        },
        // Настройки X-оси
        axisX: {
            // Отключаем сетку для этой оси
            showGrid: false
        },
        // настройки Y-оси
        axisY: {
            showGrid: false
        }
    });

    practiceWrapper.classList.remove('active');
    document.getElementById('finish-practice').classList.add('active');
}

/**
 * действия при нажатии кнопки повторить
 */
function repeatPractice() {

    timeLine.style.webkitTransitionDuration = '0ms';
    timeLine.style.mozTransitionDuration = '0ms';
    timeLine.style.oTransitionDuration = '0ms';
    timeLine.style.transitionDuration = '0ms';

    settingWrapper.style.display = 'block';
    timeLine.classList.remove('active');

    levelPractice = rememberLevelStart;
    countSuccess = 0;
    countError = 0;
    document.getElementById('finish-practice').classList.remove('active');
    startWrapper.classList.add('active');

}
