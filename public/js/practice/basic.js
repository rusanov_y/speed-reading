/**
 * Общие функции и переменные упражнений
 *
 */

var levelPractice,
    timePractice,
    countSuccess = 0,
    countError = 0,
    rememberLevelStart;

var startWrapper = document.getElementById('start-wrapper');
if (document.getElementById('tbl-wrapper')) {
    var tblWrapper = document.getElementById('tbl-wrapper');
    var practiceWrapper = tblWrapper.parentNode;
}
// var infoBlock = document.getElementById('info-block');
var counterStart = document.getElementById('counter-start');
var timeLine = document.getElementById('timeline');
var finishPracticeBody = document.getElementById('finish-practice');
var currentLevelBody = document.getElementById('current-level');

/**
 * кроссплатформенное создание XMLHttpRequest
 *
 * @returns {*}
 */
function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

/**
 * Действия по нажатию кнопки Старт
 * - убираем блок с кнопкой
 * - активируем блок со счётчиком
 * - запускаем с показом цифр
 * - после показа цифры 3 запускаем функцию начала упражнения
 */
function clickStart() {

    startWrapper.classList.remove('active');
    counterStart.classList.add('active');

    var i = 3;
    setTimeout(function run() {
        counterStart.innerHTML = i;

        if (+counterStart.style.opacity === 0) {
            counterStart.style.opacity = '1';
        } else {
            counterStart.style.opacity = '0';
            i--;
        }
        if (i > 0) {
            setTimeout(run, 500);
        } else {
            setTimeout(function() {
                counterStart.textContent = '1';
                startCounter();
            }, 500);
        }
    }, 500);

}

/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    practiceStart();
    setTimeout(finishPractice, timePractice);

}

/**
 * Сортировка массива в случайном порядке
 *
 * @param arr передаваемый для сортировки массив
 * @returns arr отсортированный массив
 */
function shuffle(arr){
    var j, temp;
    for(var i = arr.length - 1; i > 0; i--){
        j = Math.floor(Math.random()*(i + 1));
        temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
    return arr;
}

/**
 * Запоминание уровня на котором остановился пользователь
 *
 */
/*async function rememberLevel() {

 fetch('/practice/schulte', {
 method: 'POST',
 headers: { 'Content-type': 'application/x-www-form-urlencoded' },
 body: "level=" + levelPractice,
 })
 .then(response => response.text())
 .then(results => console.log(results));

 }*/
function rememberLevel(fileName, data) {

    if (!data) {
        data = "level=" + levelPractice;
    }

    var req = getXmlHttp();
    var sendLevel = data;
    req.open('POST', '/practice/' + fileName, true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                console.log(req.responseText);

            }
        }
    };
    req.send(sendLevel);

}
