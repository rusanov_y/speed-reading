/**
 * "Оценка чтения"
 */

'use strict';

var lang = 'ru';

var idText,
    heightInfoBody,
    speed,
    timeTransition,
    countPage,
    currentTextPage = 1,
    countWordText,
    countLine,
    countLinePage;

var settingWrapper = document.getElementById('setting-wrapper');
var titleBody = document.getElementById('setting-title');
var settingSpeed = document.getElementById('setting-speed');
var startWrapper = document.getElementById('start-wrapper');
var infoBlock = document.getElementById('info-block');
var practiceWrapper = infoBlock.parentNode;
var textBody = infoBlock.children[0];
var counterStart = document.getElementById('counter-start');
var curtainBody = infoBlock.querySelector('div.curtain');

idText = titleBody.value;

// кроссплатформенное создание XMLHttpRequest
function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function getText(lang, idText) {

    var req = getXmlHttp();
    var sendLetters = "accelerator=true&title=" + idText;
    req.open('POST', '/practice/reading-grade', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {

                var arrNewData = JSON.parse(req.responseText);
                var str = "";

                for (var i = 0; i < arrNewData['text']['p'].length; i++) {
                    str += "<p>" + arrNewData['text']['p'][i] + "</p>";
                }

                textBody.innerHTML = str;
                countWordText = arrNewData['words'];

            }
        }
    };
    req.send(sendLetters);

}

var elementMain = document.querySelector('main');
practiceWrapper.style.height = elementMain.offsetHeight - 15 + "px";
heightInfoBody = elementMain.offsetHeight - 25;
heightInfoBody = Math.floor(Math.floor(heightInfoBody / 22) * 22);
infoBlock.style.height = heightInfoBody + "px";
countLinePage = heightInfoBody / 22;
countWordText = +textBody.dataset.words;
speed = settingSpeed.value;

titleBody.addEventListener('change', function (e) {

    getText(lang, e.target.value);

});

settingSpeed.addEventListener('change', function (e) {

    speed = e.target.value;
});

/**
 * Действия по нажатию кнопки Старт
 * - перемешиваем массив со словами
 * - убираем блок с кнопкой
 * - активируем блок со счётчиком
 * - запускаем обратный отсчёт
 * - после показа цифры 1 запускаем функцию начала упражнения
 */
function clickStart() {

    countPage = Math.ceil(textBody.offsetHeight / heightInfoBody);
    countLine = textBody.offsetHeight / 22;

    timeTransition = Math.round(countWordText / speed * 60 / countLine * 1000);

    startWrapper.classList.remove('active');
    counterStart.classList.add('active');

    var i = 3;
    setTimeout(function run() {
        counterStart.innerHTML = i;

        if (+counterStart.style.opacity === 0) {
            counterStart.style.opacity = 1;
        } else {
            counterStart.style.opacity = 0;
            i--;
        }
        if (i > 0) {
            setTimeout(run, 500);
        } else {
            setTimeout(function() {
                counterStart.innerHTML = 1;
                startCounter();
            }, 500);
        }
    }, 500);

}

/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    var elm = settingWrapper.nextElementSibling;
    elm.style.height = "100%";
    settingWrapper.style.display = 'none';

    var widthBody = practiceWrapper.offsetWidth;
    var widthCurtainLine = widthBody * 2 + 200;

    var bgLine = 'linear-gradient(to right, rgba(255,255,255, .8) 0, rgba(255,255,255, .8) ' + widthBody + 'px, rgba(255,255,255, 0) ' + (widthBody + 25) + 'px, rgba(255,255,255, 0) ' + (widthBody + 175) + 'px, rgba(255,255,255, .8) ' + (widthBody + 200) + 'px, rgba(255,255,255, .8) ' + widthCurtainLine + 'px)';

    // заполняем занавес строками
    for (var i = 0; i < countLine; i++) {
        var curLine = document.createElement('p');
        curLine.innerHTML = '&nbsp';
        curLine.style.width = widthCurtainLine + 'px';
        curLine.style.left = (widthCurtainLine - practiceWrapper.offsetWidth) * (-1) + 'px';
        curLine.style.background = bgLine;
        curtainBody.append(curLine);
    }

    practiceStart();
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    // скрываем блок обратного отсчёта
    // показываем блок упражнения
    counterStart.classList.remove('active');
    practiceWrapper.style.zIndex = 0;

    var rate = (practiceWrapper.offsetWidth + 200) / 200;
    var collectionLineCurtain = curtainBody.querySelectorAll('p');
    var l = 0;

    setTimeout(function runLine() {

        collectionLineCurtain[l].style.webkitTransitionDuration = timeTransition + (timeTransition / rate) + 'ms';
        collectionLineCurtain[l].style.mozTransitionDuration = timeTransition + (timeTransition / rate) + 'ms';
        collectionLineCurtain[l].style.oTransitionDuration = timeTransition + (timeTransition / rate) + 'ms';
        collectionLineCurtain[l].style.transitionDuration = timeTransition + (timeTransition / rate) + 'ms';

        collectionLineCurtain[l].style.left = 0;

        l++;

        if (l < countLine) {

            if (l === currentTextPage * countLinePage + 1) {

                curtainBody.style.top = currentTextPage * heightInfoBody * (-1) + 'px';
                textBody.style.top = currentTextPage * heightInfoBody * (-1) + 'px';
                currentTextPage++;

            }

            setTimeout(runLine, timeTransition);

        } else {

            setTimeout(function () {
                finishPractice();
            }, timeTransition);

        }

    }, 300);

}

function finishPractice() {

    rememberLevel();

    practiceWrapper.style.zIndex = -1;
    document.getElementById('finish-practice').classList.add('active');

}

/**
 * действия при нажатии кнопки повторить
 */
function repeatPractice() {
    levelPractice = rememberLevelStart;
    timeSuccess = 0;
    timeError = 0;
    document.getElementById('finish-practice').classList.remove('active');
    startWrapper.classList.add('active');
}

/**
 * Сортировка массива в случайном порядке
 *
 * @param arr передаваемый для сортировки массив
 * @returns arr отсортированный массив
 */
function shuffle(arr){
    var j, temp;
    for(var i = arr.length - 1; i > 0; i--){
        j = Math.floor(Math.random()*(i + 1));
        temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
    return arr;
}

/**
 * Запоминание последнего уровня
 *
 */
function rememberLevel() {

    var req = getXmlHttp();
    var sendLevel = "level=" + titleBody.value;
    req.open('POST', '/practice/reading-accelerator', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(sendLevel);

}
