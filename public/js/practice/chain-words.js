/**
 * Обработка показа примеров упражнения Цепь слов
 *
 * @var lang string - язык упражнения
 * @var arrLevels object - объект в котором указываются настройки уровней: 0 - количество слов;
 * 1 - количество символов; 2 - время показа слов
 * @var levelPractice integer - текущий уровень сложности
 * @var rememberLevelStart integer - уровень сложности с которого пользователь начал упражнение
 * @var countSuccess integer - количество данных подряд правильных ответов
 * @var countError integer - количество данных подряд неправильных ответов
 * @var arrPos array - массив с номерами слов, которые надо запомнить
 * @var arrWords array - массив всех используемых в упражнении слов
 * @var settingForm DOM-element - форма с настройками тренировки
 * @var startWrapper DOM-element - блок кнопки Старт
 * @var tblWrapper DOM-element - блок таблицы с вариантами ответа
 * @var practiceWrapper DOM-element - блок в котором идёт показ упражнения
 * @var infoBlock DOM-element - блок в которм выводятся слова для запоминания
 * @var counterStart DOM-element - блок счётчика обратного отсчёта перед началом показа
 * @var timePractice integer - время на выполнение упражнения
 * @var levelPracticeBody DOM-element - раскрывающийся список с доступными уровнями сложности
 */

'use strict';

var arrLevels = {
    1: [10, 24, 24],
    2: [20, 24, 24],
    3: [30, 20, 20],
    4: [40, 18, 18],
    5: [50, 18, 18]
};

var timePractice,
    arrPos = [],
    arrResultTable = [0, 0],
    arrWords = [];

var settingForm = document.getElementById('setting-form');
var settingWrapper = settingForm.parentElement;
var infoBlock = practiceWrapper.querySelector('#info-block');
var controlBlock = practiceWrapper.querySelector('ul.control-block');
var counterStart = document.getElementById('counter-start');
var levelPracticeBody = settingForm.querySelector('[name=setting-level]');
var timePracticeBody = settingForm.querySelector('[name=setting-time]');

timePractice = timePracticeBody.value;
levelPractice = levelPracticeBody.value;

// изменение значения времени выполнения упражнения пользователем
timePracticeBody.addEventListener('change', function (e) {
    timePractice = e.target.value;
    timePractice = +timePractice;
});


// изменение пользователем уровня сложности
levelPracticeBody.addEventListener('change', function (e) {
    levelPractice = e.target.value;
    levelPractice = +levelPractice;
});

timePractice = +timePractice;
levelPractice = +levelPractice;

// получение массива слов
function getWords() {

    var req = getXmlHttp();
    var sendLevel = "words=true";
    req.open('POST', '/practice/chain-words', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                arrWords = req.responseText.split(',');
                shuffle(arrWords);
            }
        }
    };
    req.send(sendLevel);

}

getWords();

/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    practiceStart();

}

/**
 * Старт выполнения упражнения
 *
 * @var minDigital integer - минимальное число для запоминания для данного уровня
 * @var maxDigital integer - максимальное число для запоминания для данного уровня
 */
function practiceStart() {

    //
    arrPos.length = 0;

    // выбираем слова для показа

    // заполняем массив со словами для показа
    // проходим в цикле по количеству слов
    for (var i = 0; i < arrLevels[levelPractice][0];) {

        // случайное число от 0 до количества слов в общем массиве слов
        var randomDigital = Math.floor(Math.random() * arrWords.length);

        // проверяем была эта цифра при предыдущем проходе или нет
        if (arrPos.indexOf(randomDigital) === -1 ) {

            arrPos.push(randomDigital);
            i++;

        }

    }

    // заполняем блоки таблицы с вариантами и контроля правильности
    blockFilling();

}

/**
 * Заполнение значениями блока вывода числа для запоминания
 */
function blockFilling() {
    // очищаем блок от введённых ранее данных
    infoBlock.innerHTML = "";

    // под каждое слово создаём блок (li)
    // с атрибутом 'data-word', которому присваиваем порядковый номер
    for (var i = 0; i < arrLevels[levelPractice][0]; i++) {
        var li = document.createElement('li');
        li.setAttribute('data-word', arrWords[arrPos[i]]);
        controlBlock.append(li);
    }

    var arrTmp = [];
    arrTmp = arrPos.slice();

    // перемешиваем массив
    shuffle(arrTmp);

    // заполняем блок со спискои слов для расстановки
    for (i = 0; i < arrLevels[levelPractice][0]; i++) {
        li = document.createElement('li');
        li.innerHTML = arrWords[arrTmp[i]];
        tblWrapper.append(li);
    }

    exercise();

        // прячем блок с обратным отсчётом
        // показываем блок показа упражнения
    settingWrapper.style.display = 'none';
    counterStart.classList.remove('active');
    practiceWrapper.classList.add('active');

}

/**
 * Показ упражнения
 *
 * @var errorDigital integer указатель на то, что одна из цифр была указана неверно
 * @var arrClickDigital collection коллекция полей, в которых выводится число для запоминания
 */
function exercise() {

    var collectionList,
        startTime,
        finishTime,
        i = 0;

    // через время, установленное для запоминания в настройках уровня,
    // скрываем слова для запоминания и показываем варианты ответов
    function viewWords() {

        setTimeout(function () {
            timeLine.style.webkitTransitionDuration = timePractice + 'ms';
            timeLine.style.mozTransitionDuration = timePractice + 'ms';
            timeLine.style.oTransitionDuration = timePractice + 'ms';
            timeLine.style.transitionDuration = timePractice + 'ms';

            infoBlock.innerHTML = arrWords[arrPos[i]];
            infoBlock.style.opacity = '1';
            timeLine.classList.add('active');
        }, 305);

        if (i < arrLevels[levelPractice][0]) {
            setTimeout(function () {
                timeLine.style.webkitTransitionDuration = '0ms';
                timeLine.style.mozTransitionDuration = '0ms';
                timeLine.style.oTransitionDuration = '0ns';
                timeLine.style.transitionDuration = '0ms';

                infoBlock.style.opacity = "0";
                timeLine.classList.remove('active');
                i++;
                viewWords();
            }, 300 + timePractice);
        } else {
            practiceWrapper.classList.add('control');
            timeLine.classList.remove('active');
            timeLine.style.opacity = '0';
            infoBlock.style.display = 'none';
            controlBlock.style.display = 'block';
            tblWrapper.style.display = 'block';
            tblWrapper.style.fontSize = arrLevels[levelPractice][1] + 'px';
            tblWrapper.style.opacity = '1';
            if (document.documentElement.clientWidth > 419 && levelPractice > 3) {
                tblWrapper.classList.add('col');
                controlBlock.classList.add('str');
            }

            if (document.documentElement.clientWidth > 599) {
                tblWrapper.classList.add('col');
                controlBlock.classList.add('str');
            }
            controlBlock.style.fontSize = arrLevels[levelPractice][2] + 'px';
            collectionList = controlBlock.querySelectorAll('li');
            startTime = new Date();
        }

    }

    viewWords();

    // счётчик слов
    var z = 0;
    var arrClickElem = [];
    // обработка нажатия кнопок в таблице вариантов ответа
    tblWrapper.onclick = function (e) {
        var clickElem = e.target;
        if(clickElem.localName === 'li') {
        console.log(clickElem.nodeName);
            if (arrClickElem.indexOf(clickElem.innerHTML) === -1) {
                arrClickElem.push(clickElem.innerHTML);
                collectionList[z].classList.add('active');
                collectionList[z].innerHTML = clickElem.innerHTML;
                collectionList[z].setAttribute('data-word', arrWords[arrPos[z]]);
                clickElem.style.opacity = '0';
                z++;

                if (z === arrLevels[levelPractice][0]) {
                    finishTime = new Date();
                    controlBlock.style.opacity = '0';
                    collectionList.forEach(function (item) {
                        if (item.dataset.word === item.innerHTML) {
                            item.classList.add('success');
                            arrResultTable[0] = arrResultTable[0] + 1;
                        } else {
                            item.classList.add('error')
                        }
                    });

                    setTimeout(function () {
                        controlBlock.style.opacity = '1';
                    }, 1000);

                    setTimeout(function () {
                        finishPractice(finishTime - startTime);
                    }, 3000);
                }
            }
        }
    };
}

/**
 * показ блока окончания упражнения
 *
 * - запускаем функцию запоминания уровня, на котором остановился пользователь
 * - скрываем блок показа упражнения
 * - показываем блок окончания упражнения
 */
function finishPractice(timeAnswer) {

    var blockResult = finishPracticeBody.querySelectorAll('p');

    blockResult[0].innerHTML = Math.round(arrResultTable[0] * 100 / +arrLevels[levelPractice][0]) + ' баллов';
    blockResult[1].innerHTML = Math.round(timeAnswer / 1000) + ' сек.';
    practiceWrapper.classList.remove('active');
    finishPracticeBody.classList.add('active');
}

/**
 * действия при нажатии кнопки "Повторить"
 *
 * - текущему значению уровня присваиваем изначальное значение
 * - обнуляем значения счётчика правильных и неправильных ответов
 * - скрываем блок окончания упражнения
 * - показываем блок с кнопкой "Старт"
 */
function repeatPractice() {

    timeLine.style.webkitTransitionDuration = '0ms';
    timeLine.style.mozTransitionDuration = '0ms';
    timeLine.style.oTransitionDuration = '0ms';
    timeLine.style.transitionDuration = '0ms';

    settingWrapper.style.display = 'block';
    timeLine.style.opacity = "1";
    timeLine.classList.remove('active');

    tblWrapper.innerHTML = '';
    tblWrapper.style.opacity = '';
    tblWrapper.style.display = '';
    tblWrapper.classList.remove('col');
    controlBlock.style.display = '';
    controlBlock.innerHTML = "";
    controlBlock.classList.remove('str');
    infoBlock.style.display = '';

    finishPracticeBody.classList.remove('active');
    startWrapper.classList.add('active');

}


