/**
 * Обработка показа примеров упражнения Бегущая строка
 *
 * При загрузке страницы устанавливаем язык, на котором будет показываться тренажёр
 * инициализируется объект с настройками упражнения;
 * инициализуруются глобальные переменные упражнения;
 * запрашиваем и получаем в виде строки слова для показа
 * тут же строку преобразуем в массив
 *
 * @var lang string - язык;
 * @var arrLevels objekt - объект в котором указываются настройки уровней: 0 - скорость показа слов (слов/мин)
 * @var levelPractice integer - текущий уровень сложности
 * @var rememberLevelStart integer - запоминание уровня с которого началась тренировка
 * @var words string - строка с используемыми словами, полученная с сервера
 * @var arrWords array - массив используемых слов
 * @var successAnswer string - правильный ответ
 * @var filledOut boolean - указатель заполненности блока со словами
 * @var positionArrWords integer - указатель, показывающий где остановились в выборе слов
 * @var positionBlock integer - указатель, показывающий позицию следующего слова для показа в блоке показа слов
 * @var currentLine integer - номер текущей строки блока показа слов
 * @var currentLength integer - текущая длина строки
 * @var arrAnswer array - массив слов для показа в качестве ответов
 * @var arrViewElem array - массив, с позициями слов для показа
 * @var widthLetters integer - ширина символа
 * @var countSuccess array - количество данных подряд правильных ответов
 * @var countError array - количество данных подряд неправильных ответов
 */

'use strict';

var words,
    heightInfoBody,
    textPractice;

var settingForm = document.getElementById('setting-form');
var settingWrapper = settingForm.parentElement;
var infoBlock = document.getElementById('info-block');
var practiceWrapper = infoBlock.parentNode;
var timePractice = settingForm.querySelector('[name=setting-time]').value;
var timePracticeBody = settingForm.querySelector('[name=setting-time]');

function getLetters() {

    var req = getXmlHttp();
    var sendLetters = "central=true";
    req.open('POST', '/practice/central-point', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                textPractice = req.responseText;
            }
        }
    };
    req.send(sendLetters);

}

getLetters();

timePracticeBody.addEventListener('change', function (e) {
    timePractice = e.target.value;
    timePractice = +timePractice;
});

timePractice = +timePractice;

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    infoBlock.innerHTML = textPractice;

    var elementMain = document.querySelector('main');
    practiceWrapper.style.height = elementMain.offsetHeight - 15 + "px";
    heightInfoBody = elementMain.offsetHeight - 25;
    heightInfoBody = Math.floor(Math.floor(heightInfoBody / 22) * 22);
    infoBlock.style.height = heightInfoBody + "px";

    settingWrapper.style.display = 'none';

    // скрываем блок обратного отсчёта
    // показываем блок упражнения
    counterStart.classList.remove('active');
    practiceWrapper.classList.add('active');

}

/**
 * показ блока окончания упражнения
 */
function finishPractice() {

    practiceWrapper.classList.remove('active');
    document.getElementById('finish-practice').classList.add('active');

}

/**
 * действия при нажатии кнопки повторить
 */
function repeatPractice() {

    settingWrapper.style.display = 'block';

    levelPractice = rememberLevelStart;
    countSuccess = 0;
    countError = 0;
    document.getElementById('finish-practice').classList.remove('active');
    startWrapper.classList.add('active');

}

