/**
 * Обработка показа примеров упражнения Таблица Шульте
 *
 * @var arrNumber array - массив значений цифр выводимых в таблице
 * @var arrLetters array - массив значений букв выводимых в таблице
 * @var arrLevels array - многомерный массив в котором указываются настройки уровней: 0 - кол-во выводимых символов;
 * 1 - кол-во ячеек в таблице, 2 - кол-во столбцов
 * @var settingForm DOM-element - форма с настройками тренировки
 * @var startWrapper DOM-element - блок кнопки Старт
 * @var tblWrapper DOM-element - блок с таблицей
 * @var counterStart DOM-element - блок счётчика перед началом показа
 * @var currentAction DOM-element - строка где показывается, что надо сделать (Нажмите на кнопку, нажмите на букву)
 * @var currentElementClick DOM-element - строка где показывается какую цифру(букву) сейчас надо нажать
 * @var practiceWrapper DOM-element - блок упражнения
 */

'use strict';

var arrLetters,
    symbolType,
    countTables = 0,
    arrResultTable = [],
    arrNumber = [1, 2, 3, 4, 5, 6, 7, 8 , 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],
    ageUser = sessionStorage.getItem('userAge');

var arrLevels = {
    1: [9, 3, 3, 46],
    2: [10, 3, 4, 42],
    3: [12, 3, 4, 42],
    4: [12, 4, 4, 38],
    5: [14, 4, 4, 38],
    6: [16, 4, 4, 38],
    7: [16, 4, 5, 34],
    8: [18, 4, 5, 34],
    9: [20, 4, 5, 34],
    10: [21, 5, 5, 30],
    11: [23, 5, 5, 30],
    12: [25, 5, 5, 30],
    13: [25, 5, 5, 30, 3],
    14: [25, 5, 5, 30, 6]
};

var settingForm = document.getElementById('schulte-form');
var settingWrapper = settingForm.parentElement;
var tblBody = tblWrapper.querySelector('tbody');
var currentElementClick = practiceWrapper.querySelector('#currentElementClick');
var symbolTypeBody = settingForm.querySelector('[name=setting-symbol]');
var timePracticeBody = settingForm.querySelector('[name=setting-time]');
var levelPracticeBody = settingForm.querySelector('[name=setting-level]');

timePractice = timePracticeBody.value;
symbolType = symbolTypeBody.value;
levelPractice = settingForm.querySelector('[name=setting-level]').value;

symbolTypeBody.addEventListener('change', function (e) {
    symbolType = e.target.value;
});

timePracticeBody.addEventListener('change', function (e) {
    timePractice = e.target.value;
    timePractice = +timePractice;
});

levelPracticeBody.addEventListener('change', function (e) {
    levelPractice = e.target.value;
    levelPractice = +levelPractice;
});

levelPractice = +levelPractice;
rememberLevelStart = levelPractice;
timePractice = +timePractice;

/**
 * получаем алфавит
 * того языка, которые выбрал пользователь
 */
function getAlphabet() {

    var req = getXmlHttp();
    var data = "alphabet=true";
    req.open('POST', '/practice/schulte', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {

                arrLetters = req.responseText.split(',')

            }
        }
    };
    req.send(data);

}

getAlphabet();

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    var arrCurrent = [];
    countTables++;

    // получаем массив с необходимыми значениями
    if (symbolType === "number") {
        arrCurrent = arrNumber.slice(0, arrLevels[levelPractice][0]);
    } else  {
        arrCurrent = arrLetters.slice(0, arrLevels[levelPractice][0]);
    }

    // копируем содержимое массива в сортируемый массив
    var arrSort = arrCurrent.slice();

    // удаляем классы, помогающие придать ячейкам одинаковый размер
    tblWrapper.classList.remove('col-' + (arrLevels[+levelPractice][2] - 1));
    tblWrapper.classList.remove('col-' + arrLevels[levelPractice][2]);
    tblWrapper.classList.remove('col-' + (arrLevels[levelPractice][2] + 1));

    tableFilling(arrSort);

    exercise(arrCurrent);
}

/**
 * Заполнение таблицы значениями
 * - сортируем массив со значениями в случайном порядке
 * - в зависимости от уровня сложности определяем количество ячеек в строке (кол-во div),
 * - также вставляются, при необходимости, ячейки с пустыми значениями
 * - в цикле добавляем ячейки (div) в блок с таблицей
 *
 * @param arrSort - массив значений таблицы, который сортируется в случайном порядке
 */
function tableFilling(arrSort) {

    // сортируем массив случайным образом
    shuffle(arrSort);

    // очищаем старые данные
    tblBody.innerHTML = '';

    // заполняем таблицу новыми значениями
    var arr1 = [],
        arr2 = [],
        arr3 = [],
        countCell;
    // определяем общее количество ячеек
    countCell = arrLevels[levelPractice][1] * arrLevels[levelPractice][2];

    // если количество ячеек больше, чем количество элементов, добавляем пустые ячейки
    if (countCell - arrLevels[levelPractice][0] === 2) {
        arrSort.unshift('');
        arrSort.push('');
    } else if (countCell - arrLevels[levelPractice][0] === 4) {
        var k = arrLevels[levelPractice][2] - 2;
        arr1 = arrSort.slice(0, k);
        arr2 = arrSort.slice(k, k * (-1));
        arr3 = arrSort.slice(k * (-1));
        arr1.unshift('&nbsp;');
        arrSort = arr1.concat('&nbsp;', arr2, '&nbsp;', arr3, '&nbsp;');
    }

    // заполняем таблицу рядами и ячейками
    for (var q = 0; q < arrLevels[levelPractice][1]; q++) {
        var row = document.createElement('tr');
        tblBody.append(row);
        for (var r = 0; r < arrLevels[levelPractice][2]; r++) {
            var newTd = document.createElement('td');
            newTd.innerHTML = arrSort.pop();
            row.append(newTd);
        }
    }

    tblWrapper.classList.add('col-' + arrLevels[levelPractice][2]);

    if (levelPractice > 12) {

        var elems = tblBody.querySelectorAll('td');
        var arrTmp = [];

        for (var m = 0; m < arrLevels[levelPractice][4];) {
            var el = Math.floor( Math.random() * countCell);
            if (arrTmp.indexOf(el) === -1) {
                arrTmp[k] = el;
                elems[el].classList.add('color-1');
                m++;
            }
        }
    }

}

/**
 * Показ упражнения
 * - скрываем блок со счётчиком
 * - показываем блок с таблицей
 * - выводим в верхнем блоке первый элемент, на который надо нажать
 * - запоминаем время начала показа
 * - отслеживаем событие клика по таблице
 * - при клике определяем по какому элементу таблицы был клик
 * - сравниваем значение элемента с текущим значением массива
 * - при совпадении переходим к следующему элементу (увеличиваем счётчик)
 * - проверяем это был последний элемент или нет
 * - если не последний элемент, показываем, какой следующий элемент надо нажать
 * - если последний, то запоминаем время окончания показа
 * - высчитываем время, затраченное на прохождение
 * - если время меньше 20 сек, то увеличиваем уровень сложности (после того как пользователь
 * два раза пройдёт текущий уровень быстрее 20 сек)
 * - если время больше 20 сек., то уменьшаем уровень сложности.
 * - перезапускаем показ перемешав элементы.
 *
 * @param arrCurrent текущий массив значений таблицы
 * @var countElements количество элементов в таблице
 * @var startTime начало выполнения прохождения одной таблицы
 * @var finishTime окончание прохождения таблицы
 * @var travelTime - время за которое была пройдена таблица
 */
function exercise(arrCurrent) {

    var startTime,
        finishTime,
        travelTime,
        countElements;

    settingWrapper.style.display = 'none';

    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    counterStart.classList.remove('active');
    tblWrapper.parentNode.classList.add('active');
    tblWrapper.style.fontSize = arrLevels[levelPractice][3] + "px";
    tblWrapper.style.opacity = '1';
    currentElementClick.innerHTML = arrCurrent[0];
    countElements = arrLevels[levelPractice][0];
    timeLine.classList.add('active');

    startTime = Date.now();

    var i = 0;
    tblWrapper.onclick = function (event) {

        if (event.target.innerHTML === String(arrCurrent[i])) {

            i++;

            if (i === countElements) {
                finishTime = Date.now();
                travelTime = finishTime - startTime;
                arrResultTable.push([travelTime, countElements / (travelTime / 1000)]);

                if (travelTime < 20000) {
                    countSuccess++;
                    if ( countSuccess > 4 ) {
                        if ( levelPractice !== Object.keys(arrLevels).length ) {
                            countSuccess = 0;
                            levelPractice++;
                            currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                            currentLevelBody.style.color = '#007700';
                            currentLevelBody.classList.add('active');
                            setTimeout(function () {
                                currentLevelBody.classList.remove('active');
                            }, 2000);
                        }
                    }

                } else {
                    countSuccess = 0;
                    countError++;
                    if ( countError > 2 ) {
                        countError = 0;
                        if ( levelPractice !== 1 ) {
                            levelPractice--;
                            currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                            currentLevelBody.style.color = '#dd0000';
                            currentLevelBody.classList.add('active');
                            setTimeout(function () {
                                currentLevelBody.classList.remove('active');
                            }, 2000);
                        }
                    }
                }

                return practiceStart();
            }

            currentElementClick.innerHTML = arrCurrent[i];
        }
    };

}

/**
 * показ блока окончания упражнения
 */
function finishPractice() {
    rememberLevel('schulte');

    // вычисляем эффективность работы пользователя
    var arrLabel = [],
        arrValue = [],
        t = 0;

    arrResultTable.forEach(function (item, index) {
        arrLabel.push(index + 1);
        arrValue.push(item[1].toFixed(2));
        t += item[0];
    });

    var ef = Math.round(t / arrResultTable.length);

    var blockEfficiency = finishPracticeBody.querySelector('p.result-practice');

    if (ef <= 8000) {
        blockEfficiency.innerHTML = '5 баллов';
    }

    if ( (ageUser < 11 && (ef > 8000 && ef <= 13000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 8000 && ef <= 12000)) || ((ageUser >= 12 || ageUser === 0) && (ef > 8000 && ef <= 11000)) ) {
        blockEfficiency.innerHTML = '4 балла';
    }

    if ( (ageUser < 11 && (ef > 13000 && ef <= 18000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 12000 && ef <= 16000)) || ((ageUser >= 12 || ageUser === 0) && (ef > 11000 && ef <= 15000)) ) {
        blockEfficiency.innerHTML = '3 балла';
    }

    if ( (ageUser < 11 && (ef > 18000 && ef <= 24000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 16000 && ef <= 21000)) || ((ageUser >= 12 || ageUser === 0) && (ef > 15000 && ef <= 19000)) ) {
        blockEfficiency.innerHTML = '2 балла';
    }

    if ( (ageUser < 11 && ef > 24000) || ((ageUser >= 11 && ageUser < 12) && ef > 21000) || ((ageUser >= 12 || ageUser === 0) && ef > 19000) ) {
        blockEfficiency.innerHTML = '1 балл';
    }

    new Chartist.Line('.ct-chart', {
        labels: arrLabel,
        series: [
            arrValue
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 20,
            left: 0
        },
        // Настройки X-оси
        axisX: {
            // Отключаем сетку для этой оси
            showGrid: false
        },
        // настройки Y-оси
        axisY: {
            showGrid: false
        }
    });

    practiceWrapper.classList.remove('active');
    finishPracticeBody.classList.add('active');
}

/**
 * действия при нажатии кнопки повторить
 */
function repeatPractice() {

    timeLine.style.mozTransitionDuration = '0ms';
    timeLine.style.oTransitionDuration = '0ms';
    timeLine.style.transitionDuration = '0ms';

    settingWrapper.style.display = 'block';
    timeLine.classList.remove('active');

    levelPractice = rememberLevelStart;
    countSuccess = 0;
    countError = 0;
    finishPracticeBody.classList.remove('active');
    startWrapper.classList.add('active');
}
