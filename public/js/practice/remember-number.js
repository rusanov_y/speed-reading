/**
 * Обработка показа примеров упражнения Запомни число
 *
 * @var arrLevels array - многомерный массив в котором указываются настройки уровней: 0 - разрядность числа
 * для запоминания; 1 - скорость (время)
 * @var levelPractice integer - текущий уровень сложности
 * @var rememberLevelStart integer - уровень сложности с которого пользователь начал упражнение
 * @var currentNumber string - текущее число для запоминания, формируется как число, потом преобразуется в строку
 * @var countSuccess integer - количество подряд данных правильных ответов
 * @var countError integer - количество подряд данных неправильных ответов
 * @var settingForm DOM-element - форма с настройками тренировки
 * @var startWrapper DOM-element - блок кнопки Старт
 * @var tblWrapper DOM-element - блок таблицы с цифрами для ввода ответа
 * @var counterStart DOM-element - блок счётчика обратного отсчёта перед началом показа
 * @var infoBlock DOM-element - строка где показывается какую цифру(букву) сейчас надо нажать
 * @var practiceWrapper DOM-element - блок упражнения
 * @var timePractice integer - время на выполнение упражнения
 */

'use strict';

var arrLevels = {
    1: [3, 25],
    2: [3, 29],
    3: [4, 34],
    4: [4, 40],
    5: [5, 46],
    6: [6, 53],
    7: [7, 61],
    8: [8, 71]
};

var currentNumber,
    finishBegin = 0,
    arrResultTable = [0, 0];

var settingForm = document.getElementById('setting-form');
var settingWrapper = settingForm.parentElement;
var practiceWrapper = tblWrapper.parentNode;
var infoBlock = practiceWrapper.querySelector('#info-block');
var currentLevelBody = document.getElementById('current-level');
var timePracticeBody = settingForm.querySelector('[name=setting-time]');
var levelPracticeBody = settingForm.querySelector('[name=setting-level]');

timePractice = timePracticeBody.value;
levelPractice = levelPracticeBody.value;

// изменение значения времени выполнения упражнения пользователем
timePracticeBody.addEventListener('change', function (e) {
    timePractice = e.target.value;
    timePractice = +timePractice;
});
// изменение пользователем уровня сложности
levelPracticeBody.addEventListener('change', function (e) {
    levelPractice = e.target.value;
    levelPractice = +levelPractice;
    rememberLevelStart = levelPractice;
});

levelPractice = +levelPractice;
rememberLevelStart = levelPractice;
timePractice = +timePractice;

/**
 * Старт выполнения упражнения
 *
 * @var minDigital integer - минимальное число для запоминания для данного уровня
 * @var maxDigital integer - максимальное число для запоминания для данного уровня
 */
function practiceStart() {

    var minDigital = Math.pow(10, arrLevels[levelPractice][0] - 1);
    var maxDigital = Math.pow(10, arrLevels[levelPractice][0]);
    // получаем случайное число в заданном диапазоне
    currentNumber = Math.floor(minDigital + Math.random() * (maxDigital - minDigital));
    // т.к. каждая цифра числа будет выводиться как отдельный элемент, то преобразуем число в строку
    currentNumber = String(currentNumber);

    // заполняем блок показа числа
    blockFilling();

    // прячем блок с обратным отсчётом
    // показываем блок показа упражнения
    counterStart.classList.remove('active');
    tblWrapper.parentNode.classList.add('active');

    // мигание перед показом числа
    var i = 3;
    setTimeout(function run() {

        if (+infoBlock.style.opacity === 0) {
            infoBlock.style.opacity = '1';
        } else {
            infoBlock.style.opacity = '0';
            i--;
        }
        if (i > 0) {
            setTimeout(run, 400);
        } else {
            setTimeout(function() {
                infoBlock.style.opacity = '1';
                exercise();
            }, 400);
        }
    }, 400);

    settingWrapper.style.display = 'none';

    timeLine.style.webkitTransitionDuration = timePractice + 'ms';
    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    timeLine.classList.add('active');

}

/**
 * Заполнение значениями блока вывода числа для запоминания
 *
 */
function blockFilling() {
    // очищаем блок от введённых ранее данных
    infoBlock.innerHTML = "";

    // под каждую цифру числа создаём блок (li)
    // с атрибутом 'data-number', которому присваиваем значение соответствующей цифры
    for (var q = 0; q < arrLevels[levelPractice][0]; q++) {
        var li = document.createElement('li');
        li.innerHTML = '-';
        li.setAttribute('data-number', currentNumber[q]);
        infoBlock.append(li);
    }
}

/**
 * Показ упражнения
 *
 * @var errorDigital integer указатель на то, что одна из цифр была указана неверно
 * @var arrClickDigital collection коллекция полей, в которых выводится число для запоминания
 */
function exercise() {

    var errorDigital = 0;

    var arrClickDigital = infoBlock.querySelectorAll('li');

    // вывод на экран числа
    for (var k = 0; k < arrLevels[levelPractice][0]; k++) {
        arrClickDigital[k].innerHTML = arrClickDigital[k].getAttribute('data-number');
    }

    // через 1,5 сек прячем число, выводим вместо цифр "-"
    setTimeout(function () {
        for (var k = 0; k < arrLevels[levelPractice][0]; k++) {
            arrClickDigital[k].innerHTML = '-';
        }
    }, 1500);

    // счётчик показывающий, какую по порядку цифру вводит пользователь
    var i = 0;
    // обработка клика по таблице с числами
    tblWrapper.onclick = function (e) {

        var el = e.target;

        // проверяем, слик был по ячейке стереть или по ячейке с цифрой
        if (el.dataset && el.dataset.role === 'clear') {
            // если была нажата кнопка стереть проверяем, чтобы это была не 0 цифра
            if (i > 0) {
                // переходим к введённой предыдущей цифре
                i--;
                // удаляем её значение
                arrClickDigital[i].innerHTML = "-";
                arrClickDigital[i].removeAttribute('data-clickNumber');
            }

        } else {
            // отображаем цифру, которую нажал пользователь и запоминаем её в атрибуте
            arrClickDigital[i].setAttribute('data-clickNumber', el.innerHTML);
            arrClickDigital[i].innerHTML = el.innerHTML;
            i++;
            // проверяем нажата последняя цифра числа или нет
            if (i === arrLevels[levelPractice][0]) {
                // после ввода последгнеё цифры через 0,3 сек прячем число
                setTimeout(function () {
                    infoBlock.style.opacity = '0';
                }, 300);
                // через 0,5 сек после ввода последней цифры (0,3 сек на задержку перед скрытием
                // числа + 0,2 сек на само скрытие) проверяем введённое значение с исходным

                setTimeout(function () {
                    // сверяем каждую цифру и введённое значение с исходным
                    for (var k = 0; k < i; k++) {
                        if (arrClickDigital[k].getAttribute('data-number') === arrClickDigital[k].getAttribute('data-clickNumber')) {
                            // при совпадении присваиваем класс успеха
                            arrClickDigital[k].classList.add('success');
                        } else {
                            // при ошибке присваиваем класс ошибка
                            // увеличиваем значение указателя ошибки
                            arrClickDigital[k].classList.add('error');
                            errorDigital++;
                        }
                        // показываем исходное число.
                        // Цифры с классом успех зелёным цветом
                        // Цифры с классом Ошибка красным
                        arrClickDigital[k].innerHTML = arrClickDigital[k].getAttribute('data-number');
                    }

                    infoBlock.style.opacity = '1';

                    // проверем число на наличие ошибочных цифр
                    if (errorDigital > 0) {
                        arrResultTable[1] = arrResultTable[1] + 1;
                        // при наличии ошибочных цифр сбрасываем счётчик правильных ответов на 0
                        countSuccess = 0;
                        // проверяем, чтобы не был первый уровень
                        if (levelPractice > 1) {
                            // увеличиваем счётчик ошибочных ответов
                            countError++;
                            // проверяем количество подряд введённых ошибочных ответов
                            if (countError > 2) {
                                // если введено подряд 3 ошибочных ответа уменьшаем уровень
                                // счётчик ошибочных ответов сбрасываем на 0
                                levelPractice--;
                                countError = 0;
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#dd0000';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }
                    } else {
                        // при отсутствии ошибок сбрасываем счётчик ошибочных ответов на 0,
                        // увеличиваем счётчик првильных ответов
                        countError = 0;
                        countSuccess++;
                        arrResultTable[0] = arrResultTable[0] + 1;
                        // проверяем количество правильно подряд введённых ответов
                        if (countSuccess > 4) {
                            // если введено подряд 5 правильных ответа увеличиваем уровень
                            // обнуляем счётчик правильных ответов
                            levelPractice++;
                            countSuccess = 0;
                            currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                            currentLevelBody.style.color = '#007700';
                            currentLevelBody.classList.add('active');
                            setTimeout(function () {
                                currentLevelBody.classList.remove('active');
                            }, 2000);
                        }
                    }

                    // через 1,5 сек скрываем результат
                    setTimeout(function () {
                        infoBlock.style.opacity = '0';
                    }, 1500);

                }, 500);
                // через 2 сек после ввода последней цифры запускаем упражнение по новому
                setTimeout(function () {
                    if (finishBegin === 0) {
                        return practiceStart();
                    }
                }, 2000);
            }
        }
    };

}

/**
 * показ блока окончания упражнения
 *
 * - останавливаем таймер начала показа следующего числа
 * - запускаем функцию запоминания уровня, на котором остановился пользователь
 * - скрываем блок показа упражнения
 * - показываем блок окончания упражнения
 */
function finishPractice() {

    finishBegin = 1;
    var blockResult = finishPracticeBody.querySelector('p.result-practice');
    var totalAnswer = arrResultTable[0] + arrResultTable[1];
    blockResult.innerHTML = Math.round(arrResultTable[0] * 500 / totalAnswer) + ' баллов';
    practiceWrapper.classList.remove('active');
    finishPracticeBody.classList.add('active');

    rememberLevel('remember-number');

}

/**
 * действия при нажатии кнопки "Повторить"
 *
 * - текущему значению уровня присваиваем изначальное значение
 * - обнуляем значения счётчика правильных и неправильных ответов
 * - скрываем блок окончания упражнения
 * - показываем блок с кнопкой "Старт"
 */
function repeatPractice() {

    timeLine.style.webkitTransitionDuration = '0ms';
    timeLine.style.mozTransitionDuration = '0ms';
    timeLine.style.oTransitionDuration = '0ms';
    timeLine.style.transitionDuration = '0ms';

    settingWrapper.style.display = 'block';
    timeLine.classList.remove('active');

    levelPractice = rememberLevelStart;
    countSuccess = 0;
    countError = 0;
    document.getElementById('finish-practice').classList.remove('active');
    startWrapper.classList.add('active');

}
