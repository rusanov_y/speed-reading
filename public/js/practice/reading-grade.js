/**
 * "Оценка чтения"
 */

'use strict';

var idText,
    timeStart,
    timeFinish,
    timeRead,
    heightInfoBody,
    countPage,
    currentTextPage = 0,
    successAnswer = 0,
    countWordText;

var settingWrapper = document.getElementById('setting-wrapper');
var titleBody = document.getElementById('setting-title');
var startWrapper = document.getElementById('start-wrapper');
var infoBlock = document.getElementById('info-block');
var practiceWrapper = infoBlock.parentNode;
var textBody = infoBlock.children[0];
var counterStart = document.getElementById('counter-start');
var controlBlock = document.getElementById('control-block');
var answerBlock = controlBlock.querySelectorAll('li');

idText = titleBody.value;

// кроссплатформенное создание XMLHttpRequest
function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function getText(idText) {

    var req = getXmlHttp();
    var sendLetters = "grade=true&currentTitle=" + idText;
    req.open('POST', '/practice/reading-grade', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {

                var arrNewData = JSON.parse(req.responseText);
                var str = "";
                for (var i = 0; i < arrNewData['text']['p'].length; i++) {
                    str += "<p>" + arrNewData['text']['p'][i] + "</p>";
                }
                textBody.innerHTML = str;
                countWordText = arrNewData['words'];
                var arrQuestions = arrNewData['questions']['question_body'];
                shuffle(arrQuestions);

                var controlElements = controlBlock.querySelectorAll('li');

                for (i = 0; i < controlElements.length; i++) {

                    str = "";
                    str += '<p class="orange-text-dark">' +(i + 1) + ' из 5</p>';
                    str += "<h4>" + arrQuestions[i]['question'] + "</h4><div>";

                    var arrTmp = arrQuestions[i]['answers']['answer'];

                    var firstAnswer = arrTmp[0];

                    shuffle(arrTmp);

                    for (var j = 0; j < arrTmp.length; j++) {
                        str += '<button class="btn" data-answer="' + firstAnswer + '" data-number="' + (i + 1) + '" onclick="answerClick(this)">' + arrTmp[j] + '</button>';
                    }
                    str += "</div>";

                   controlElements[i].innerHTML = str;
                }

            }
        }
    };

    req.send(sendLetters);

}

var elementMain = document.querySelector('main');
practiceWrapper.style.height = elementMain.offsetHeight - 15 + "px";
heightInfoBody = practiceWrapper.offsetHeight - 60;
heightInfoBody = Math.floor(Math.floor(heightInfoBody / 22) * 22);
infoBlock.style.height = heightInfoBody + "px";

titleBody.addEventListener('change', function (e) {
    getText(e.target.value);
});

/**
 * Действия по нажатию кнопки Старт
 * - перемешиваем массив со словами
 * - убираем блок с кнопкой
 * - активируем блок со счётчиком
 * - запускаем обратный отсчёт
 * - после показа цифры 1 запускаем функцию начала упражнения
 */
function clickStart() {

    startWrapper.classList.remove('active');
    counterStart.classList.add('active');

    var i = 3;
    setTimeout(function run() {
        counterStart.innerHTML = i;

        if (+counterStart.style.opacity === 0) {
            counterStart.style.opacity = '1';
        } else {
            counterStart.style.opacity = '0';
            i--;
        }
        if (i > 0) {
            setTimeout(run, 500);
        } else {
            setTimeout(function() {
                counterStart.innerHTML = '1';
                startCounter();
            }, 500);
        }
    }, 500);

}

/**
 * запуск упражнения
 */
function startCounter() {

    var elm = settingWrapper.nextElementSibling;
    elm.style.height = "100%";
    settingWrapper.style.display = 'none';
    countPage = Math.ceil(textBody.offsetHeight / heightInfoBody);

    rememberTitle(titleBody.value);

    practiceStart();
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    if (countPage === 1) {
        var btnNext = practiceWrapper.querySelector('button');
        btnNext.innerHTML = "Закончить";
        btnNext.setAttribute('onclick', 'finishReadText()');
    }

    // скрываем блок обратного отсчёта
    // показываем блок упражнения
    counterStart.classList.remove('active');
    practiceWrapper.style.zIndex = "0";
    timeStart = new Date();

}

function nextText() {

    currentTextPage++;
    textBody.style.top = currentTextPage * heightInfoBody * (-1) + 'px';

    if (currentTextPage === countPage - 1) {
        var btnNext = practiceWrapper.querySelector('button');
        btnNext.innerHTML = "Закончить";
        btnNext.setAttribute('onclick', 'finishReadText()');
    }
}

function finishReadText() {

    timeFinish = new Date();
    timeRead = (timeFinish - timeStart) / 60000;

    answerBlock[0].style.zIndex = '1';
    answerBlock[0].style.opacity = '1';

    practiceWrapper.style.zIndex = '-1';
    controlBlock.classList.add('active');
}

function answerClick(el) {

    if (el.dataset['answer'] === el.innerHTML) {
        successAnswer++;
    }

    if (el.dataset['number'] === "5") {

        answerBlock[el.dataset['number'] - 1].style.zIndex = "-3";
        setTimeout(function () {
            finishPractice();
        }, 700);

    } else {

        answerBlock[el.dataset['number'] - 1].style.opacity = "0";
        answerBlock[el.dataset['number']].style.zIndex = '1';

        setTimeout(function () {
            answerBlock[el.dataset['number'] - 1].style.zIndex = "-3";
            answerBlock[el.dataset['number']].style.opacity = '1';
        }, 500);

    }
}

function finishPractice() {

    var resultBlock = document.getElementById('result-reading');
    var collectionSpan = resultBlock.querySelectorAll('span');
    var answerBlock = resultBlock.querySelectorAll('p');

    if (!countWordText) {
        countWordText = collectionSpan[0].dataset.countwords;
    }

    collectionSpan[0].innerHTML = Math.round(countWordText / timeRead);
    collectionSpan[1].innerHTML = successAnswer;
    collectionSpan[2].innerHTML = 100 * successAnswer / 5;

    if ( ( 100 * successAnswer ) / 5 < 50 ) {

        answerBlock[0].classList.add('error');
        answerBlock[1].classList.add('error');
        answerBlock[1].innerHTML = 'Много ошибок. Результат не будет учтён.';

    } else {

        rememberSpeed(Math.round(countWordText / timeRead));
        answerBlock[0].classList.add('success');

    }

    controlBlock.classList.remove('active');
    document.getElementById('finish-practice').classList.add('active');

}

/**
 * действия при нажатии кнопки повторить
 */
function repeatPractice() {
    levelPractice = rememberLevelStart;
    timeSuccess = 0;
    timeError = 0;
    document.getElementById('finish-practice').classList.remove('active');
    startWrapper.classList.add('active');
}

/**
 * Сортировка массива в случайном порядке
 *
 * @param arr передаваемый для сортировки массив
 * @returns arr отсортированный массив
 */
function shuffle(arr){
    var j, temp;
    for(var i = arr.length - 1; i > 0; i--){
        j = Math.floor(Math.random()*(i + 1));
        temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
    return arr;
}

/**
 * Запоминание названия текста
 * @param titleText
 */
function rememberTitle(titleText) {
    var req = getXmlHttp();
    var sendTitle = "title=" + titleText;
    req.open('POST', '/practice/reading-grade', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(sendTitle);
}

function rememberSpeed(res) {
    var req = getXmlHttp();
    var sendRes = "level=" + res;
    req.open('POST', '/practice/reading-grade', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(sendRes);
}
