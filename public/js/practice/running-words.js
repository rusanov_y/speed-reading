/**
 * Обработка показа примеров упражнения Бегущая строка
 *
 * При загрузке страницы устанавливаем язык, на котором будет показываться тренажёр
 * инициализируется объект с настройками упражнения;
 * инициализуруются глобальные переменные упражнения;
 * запрашиваем и получаем в виде строки слова для показа
 * тут же строку преобразуем в массив
 *
 * @var lang string - язык;
 * @var arrLevels objekt - объект в котором указываются настройки уровней: 0 - скорость показа слов (знаков/мин)
 *      1 - время в миллисекундах, отводимое на показ одной буквы.
 * @var levelPractice integer - текущий уровень сложности
 * @var rememberLevelStart integer - запоминание уровня с которого началась тренировка
 * @var words string - строка с используемыми словами, полученная с сервера
 * @var arrWords array - массив используемых слов
 * @var successAnswer string - правильный ответ
 * @var filledOut boolean - указатель заполненности блока со словами
 * @var positionArrWords integer - указатель, показывающий где остановились в выборе слов
 * @var positionBlock integer - указатель, показывающий позицию следующего слова для показа в блоке показа слов
 * @var currentLine integer - номер текущей строки блока показа слов
 * @var currentLength integer - текущая длина строки
 * @var arrAnswer array - массив слов для показа в качестве ответов
 * @var arrViewElem array - массив, с позициями слов для показа
 * @var widthLetters integer - ширина символа
 * @var countSuccess array - количество данных подряд правильных ответов
 * @var countError array - количество данных подряд неправильных ответов
 */

'use strict';

var arrLevels = {
    1: [60, 1000],
    2: [70, 857],
    3: [80, 750],
    4: [90, 667],
    5: [100, 600],
    6: [150, 400],
    7: [200, 300],
    8: [250, 240],
    9: [300, 200],
    10: [350, 171],
    11: [400, 150],
    12: [450, 133],
    13: [500, 120],
    14: [600, 100],
    15: [700, 86],
    16: [800, 75],
    17: [900, 67],
    18: [1000, 60]
};

var words,
    successAnswer,
    filledOut = false,
    positionArrWords = 0,
    positionBlock = 0,
    currentLine = 0,
    currentLength = 0,
    arrWords = [],
    arrAnswer = [],
    endPractice = 0,
    arrViewElem = [],
    arrLengthWords = [],
    arrResultTable = [0],
    widthLetters,
    widthInfoBlock;

var settingForm = document.getElementById('setting-form');
var settingWrapper = settingForm.parentElement;
var startWrapper = document.getElementById('start-wrapper');
var tblBody = tblWrapper.querySelector('tbody');
var currentAction = practiceWrapper.querySelector('#currentAction');
var infoBlock = practiceWrapper.querySelector('#info-block');
var timePracticeBody = settingForm.querySelector('[name=setting-time]');
var levelPracticeBody = settingForm.querySelector('[name=setting-level]');

timePractice = timePracticeBody.value;
levelPractice = levelPracticeBody.value;

function getWords() {

    var req = getXmlHttp();
    var sendRequest = "words=true";
    req.open('POST', '/practice/running-words', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                var tmpData = JSON.parse(req.responseText);
                arrWords = tmpData['words'];
                arrWords = arrWords.split(',');
                shuffle(arrWords);
                widthLetters = tmpData['letters'];
            }
        }
    };
    req.send(sendRequest);

}

getWords();

timePracticeBody.addEventListener('change', function (e) {
    timePractice = e.target.value;
    timePractice = +timePractice;
});

levelPracticeBody.addEventListener('change', function (e) {
    levelPractice = e.target.value;
    levelPractice = +levelPractice;
    rememberLevelStart = levelPractice;
});

levelPractice = +levelPractice;
rememberLevelStart = levelPractice;
timePractice = +timePractice;

/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    // определяем ширину блока
    widthInfoBlock = infoBlock.offsetWidth;

    endPractice = new Date();
    endPractice = +endPractice;
    endPractice += timePractice;

    arrResultTable.push(arrLevels[levelPractice][0]);

    practiceStart();
    setTimeout(finishPractice, timePractice);
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    blockFilling();

    settingWrapper.style.display = 'none';

    timeLine.style.webkitTransitionDuration = timePractice + 'ms';
    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    counterStart.classList.remove('active');
    practiceWrapper.style.zIndex = '1';
    timeLine.classList.add('active');

    exercise();

}

/**
 * Заполнение блока, показываемых слов
 */
function blockFilling() {

    // временный массив с текущими словами
    var arrTmpWords = [];

    // очищаем массив с номерами элементов для показа и массив с вариантами ответов
    arrAnswer.length = 0;

    // берём очередные 7 слов для показа из общего массива слов
    // запоминаем их во временный массив
    // проверяем это окончание общего массива слов или нет
    // если это последнее слово в общем массиве, то начинаем добавлять слова с начала массива
    // устанавливаем новую исходную позицию в таблице слов
    if(arrWords.length - positionArrWords < 7) {
        var i = 7 - (arrWords.length - positionArrWords);
        arrTmpWords = arrWords.slice(positionArrWords);
        arrTmpWords = arrTmpWords.concat(arrWords.slice(0, i));
        positionArrWords = i;
    } else {
        arrTmpWords = arrWords.slice(positionArrWords, positionArrWords + 7);
        positionArrWords = positionArrWords + 7;
    }

    // запоминаем последнее слово
    successAnswer = arrTmpWords[6];

    // коллекция ранее добавленных слов
    var createElementsBlock = infoBlock.querySelectorAll('span');

    // коллекция строк блока показа слов
    var lineCollection = infoBlock.querySelectorAll('p');

    // заполнение блока показа слов
    // проверяем заполнен весь блок показа слов или нет
    if (filledOut === true) {

        // если заполнен
        // сохраняем последние элементы.
        var lastElements = [];
        for (var d = 0; d < positionBlock; d++) {
            lastElements[d] = createElementsBlock[d].innerHTML;
        }

        // очищаем все строки блока
        for (var z = 0; z < lineCollection.length; z++) {
            lineCollection[z].innerHTML = "";
        }

        // обнуляем счётчик строк и длину заполненной строки.
        currentLine = 0;
        currentLength = 0;
        filledOut = false;

        // заполняем остаток с предыдущего показа
        // проходим в цикле по всем элементам временного массива
        for (var w = 0; w < lastElements.length; w++) {

            // вычисляем длину строки
            // длину очередного слова устанавливаем в 0
            var currentWordWidth = 0;

            // в цикле берём каждую букву слова и прибавляем её ширину к общей ширине слова
            for (var h = 0; h < lastElements[w].length; h++) {
                currentWordWidth += +widthLetters[lastElements[w][h]];
            }

            // к длине строки прибавляем ширину слова и ширину пробела между слов
            currentLength += currentWordWidth + 4.16;

            // если вычисленная длина строки больше ширины блока
            // увеличиваем номер строки (переходим на следующую строку)
            if (currentLength > widthInfoBlock) {
                currentLine++;
                currentLength = 0;
                currentLength = currentWordWidth + 4.16;
            }

            // добавляем узел span в блок показа слов на нужной строке
            var newSpan = document.createElement('span');
            newSpan.innerHTML = lastElements[w];
            lineCollection[currentLine].append(newSpan);

        }

        // устанавливаем курсор последнего элемента
        positionBlock = lastElements.length;

        // заполняем блок новыми словами
        insertWords(arrTmpWords, createElementsBlock, widthInfoBlock, lineCollection);

    } else {

        insertWords(arrTmpWords, createElementsBlock, widthInfoBlock, lineCollection);

    }

    // отсеиваем первое слово из временного массива со словами
    arrTmpWords = arrTmpWords.slice(1);

    // перемешиваем массив с вариантами ответа
    shuffle(arrTmpWords);

    // получаем коллекцию ячеек таблицы с ответами
    var cellsTableAnswer = tblBody.querySelectorAll('td');

    // заполнение таблицы с вариантами ответа
    for (var j = 0; j < cellsTableAnswer.length; j++) {
        cellsTableAnswer[j].innerHTML = arrTmpWords[j];
    }

}

/**
 *
 * Заполнение блока показа слов новыми словами
 *
 * @param arrTmpWords array временный массив с очередными словами для показа
 * @param createElementsBlock collection коллекция ранее вставленных слов в блок показа
 * @param widthInfoBlock integer ширина блока показа слов
 * @param lineCollection collection коллекция строкв блоке показа слов (5 строк)
 * @var newLine boolean указатель того, что перешли на следующую строчку
 * @var lastInsertElement последний вставленный элемент (span)
 * @var recordStartBlock boolean указатель того,что блок полностью заполнен словами
 */
function insertWords(arrTmpWords, createElementsBlock, widthInfoBlock, lineCollection) {

    // очищаем массив с элементами для показа
    arrViewElem.length = 0;

    // переменная для счётчика заполненных строк
    var e = 0,
        lastInsertElement,
        newLine = false,
        recordStartBlock = false;

    // проходим в цикле по всем элементам временного массива
    for (var w = 0; w < 7; w++) {

        // удаляем переносы строк
        arrTmpWords[w] = arrTmpWords[w].replace(/\r\n|\r|\n/g,'');
        // заполняем массив с количеством знаков в слове
        arrLengthWords[w] = arrTmpWords[w].length;

        // вычисляем длину строки
        // длину очередного слова устанавливаем в 0
        var currentWordWidth = 0;

        // в цикле берём каждую букву слова и прибавляем её ширину к общей ширине слова
        for (var h = 0; h < arrTmpWords[w].length; h++) {
            currentWordWidth += +widthLetters[arrTmpWords[w][h]];
        }

        // к длине строки прибавляем ширину слова и ширину пробела между слов
        currentLength += currentWordWidth + 4.16;

        // если вычисленная длина строки больше ширины блока
        // увеличиваем номер строки (переходим на следующую строку)
        if (currentLength > widthInfoBlock) {

            currentLine++;
            currentLength = 0;
            currentLength = currentWordWidth + 4.16;
            newLine = true;

        }

        // добавляем новый span в документ и вносим в него очередное слово
        var newSpan = document.createElement('span');
        newSpan.innerHTML = arrTmpWords[w];

        // проверяем номер строки
        if (currentLine > 4 || recordStartBlock === true) {

            // если номер очередной строки выходит за пределы блока (т.е. строк больше 5)

            // указываем, что текущая строка 0 и ставим указатель того, что запись идёт в начало блока
            if (recordStartBlock === false) {
                currentLine = 0;
                recordStartBlock = true;

                // удаляем первый spanы, по количеству слов, которые ещё нужно добавить
                for ( var x = 0; x < 7 - arrViewElem.length; x++) {
                    createElementsBlock[x].remove();
                }

                // обновляем коллекцию
                createElementsBlock = infoBlock.querySelectorAll('span');

                // ставим очередной элемент в начало первой строки
                lineCollection[currentLine].prepend(newSpan);
                // запоминаем последний вставленный элемент
                lastInsertElement = newSpan;
                newLine = false;

            } else {

                // проверяем сменился номер строки или нет
                if (newLine) {
                    // если новая строка, о ставим очередной элемент в начало строки
                    lineCollection[currentLine].prepend(newSpan);
                    lastInsertElement = newSpan;
                    newLine = false;
                } else {
                    // ставим очередной элемент после предыдущего
                    lastInsertElement.after(newSpan);
                    lastInsertElement = newSpan;
                }
            }

            // если это последнее слово, которое добавляем
            if (w === 6) {
                // указываем что блок заполнен и начали заполнять его сначала
                filledOut = true;
                lastInsertElement = '';
            }

            arrViewElem[w] = e;
            e++;

        } else {

            // добавляем узел span со значением очередного слова в блок показа слов на нужной строке
            lineCollection[currentLine].append(newSpan);
            arrViewElem[w] = positionBlock + w;

        }

    }

}

/**
 * Показ упражнения
 *
 * @var elemInfoBlock collection коллекция элементов в блоке
 */
function exercise() {

    var elemInfoBlock = infoBlock.querySelectorAll('span');


    setTimeout(function () {

            runWords();

    }, 300);

    var i = 0;

    // показ слов

    function runWords() {

        // используя рекурсию показываем поочерёдно слова
        // интервал вычисляется исходя из уровня и длины слова
        // между словами интервал 0.1 сек
        setTimeout(function run() {

            elemInfoBlock[arrViewElem[i]].style.opacity = '1';

            setTimeout(function () {

                elemInfoBlock[arrViewElem[i]].style.opacity = '0';
                positionBlock = arrViewElem[i] + 1;
                i++;

                if (i < 7) {
                    setTimeout(run, 100);
                } else {
                    stopRun();
                }

            }, arrLevels[levelPractice][1]);

        }, 100);

    }

    // окончание показа слов
    function stopRun() {

        // выводим таблицу с вариантами последнего слова
        tblWrapper.style.opacity = '1';

        // отслеживаем, на какое слово был клик
        tblWrapper.onclick = function (e) {

            // если последнее слово указано верно
            // показваем пользователю, что ответ правильный (зелёным цветом)
            // увеличиваем счётчик введённых подряд праваильных ответов
            // если подряд правильно введено 3 раза, увеличиваем уровень (скорость показа слов)
            if (e.target.innerHTML === successAnswer) {

                elemInfoBlock[arrViewElem[6]].classList.add('success');
                elemInfoBlock[arrViewElem[6]].style.opacity = '1';
                countSuccess++;
                countError = 0;
                arrResultTable[0] = arrResultTable[0] + 1;
                setTimeout(function () {
                    elemInfoBlock[arrViewElem[6]].style.opacity = '0';
                    tblWrapper.style.opacity = '0';

                    if (levelPractice < Object.keys(arrLevels).length && countSuccess > 4) {
                        levelPractice++;
                        countSuccess = 0;
                        arrResultTable.push(arrLevels[levelPractice][0]);
                        currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                        currentLevelBody.style.color = '#007700';
                        currentLevelBody.classList.add('active');
                        setTimeout(function () {
                            currentLevelBody.classList.remove('active');
                        }, 2000);
                    }

                    var curTime = new Date();
                    curTime = +curTime + 1000;
                    if (curTime < endPractice) {
                        practiceStart();
                    }

                }, 500);

            } else {

                // если последнее слово указано не верно
                // показываем пользователю последнее слово красным цветом
                // увеличиваем счётчик неправильно подряд указаных ответов
                // если подряд неправильно указано 2 раза, уменьшаем уровень (скорость показа слов)
                elemInfoBlock[arrViewElem[6]].classList.add('error');
                elemInfoBlock[arrViewElem[6]].innerHTML = successAnswer;
                elemInfoBlock[arrViewElem[6]].style.opacity = '1';
                countError++;
                countSuccess = 0;
                arrResultTable[1] = arrResultTable[1] + 1;
                setTimeout(function () {
                    elemInfoBlock[arrViewElem[6]].style.opacity = '0';
                    tblWrapper.style.opacity = '0';

                    if (levelPractice > 1 && countError > 2) {
                        levelPractice--;
                        countError = 0;
                        arrResultTable.push(arrLevels[levelPractice][0]);
                        currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                        currentLevelBody.style.color = '#dd0000';
                        currentLevelBody.classList.add('active');
                        setTimeout(function () {
                            currentLevelBody.classList.remove('active');
                        }, 2000);
                    }

                    var curTime = new Date();
                    curTime = +curTime + 1000;
                    if (curTime < endPractice) {
                        practiceStart();
                    }
                }, 500);

            }
        };
    }

}

/**
 * показ блока окончания упражнения
 */
function finishPractice() {

    rememberLevel('running-words');

    var arrLabel = [],
        arrValue = [];

    arrResultTable.forEach(function (value, index) {
        arrLabel.push(index);
        arrValue.push(value);
    });

    var blockResult = finishPracticeBody.querySelector('p.result-practice');

    // var totalAnswer = arrResultTable[0] + arrResultTable[1];

    // blockResult.innerHTML = Math.round(arrResultTable[0] * 500 / totalAnswer) + ' баллов';
    blockResult.innerHTML = "Скорость чтения: " + arrResultTable.pop() + ' слов/мин';

    new Chartist.Line('.ct-chart', {
        labels: arrLabel,
        series: [
            arrValue
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 20,
            left: 0
        },
        // Настройки X-оси
        axisX: {
            // Отключаем сетку для этой оси
            showGrid: false
        },
        // настройки Y-оси
        axisY: {
            showGrid: false
        }
    });

    practiceWrapper.style.zIndex = '-1';
    finishPracticeBody.classList.add('active');
}

/**
 * действия при нажатии кнопки повторить
 */
function repeatPractice() {

    timeLine.style.webkitTransitionDuration = '0ms';
    timeLine.style.mozTransitionDuration = '0ms';
    timeLine.style.oTransitionDuration = '0ms';
    timeLine.style.transitionDuration = '0ms';

    settingWrapper.style.display = 'block';
    timeLine.classList.remove('active');

    levelPractice = rememberLevelStart;
    countSuccess = 0;
    countError = 0;
    finishPracticeBody.classList.remove('active');
    startWrapper.classList.add('active');

}
