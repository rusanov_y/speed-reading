'use strict';

var levelPractice,
    timePractice,
    maxNumber,
    countSuccess = 0,
    countError = 0;

var startWrapper = document.getElementById('start-wrapper');
if (document.getElementById('tbl-wrapper')) {
    var tblWrapper = document.getElementById('tbl-wrapper');
    var practiceWrapper = tblWrapper.parentNode;
}
var infoBlock = document.getElementById('info-block');
var counterStart = document.getElementById('counter-start');
var timeLine = document.getElementById('timeline');
var finishPracticeBody = document.getElementById('finish-practice');
var currentLevelBody = document.getElementById('current-level');

timePractice = document.getElementById('setting-time').innerText;
levelPractice = document.getElementById('setting-level').innerText;

levelPractice = +levelPractice;
timePractice = +timePractice;

//получение get параметра по ключу
function getParam(key) {
    var s = window.location.search;
    s = s.match(new RegExp(key + '=([^&=]+)'));
    return s ? s[1] : false;
}

// получение данных и создание ссылки для кнопки "Следующее упражнение" на заключительном экране,
// или формирование ссылки "Закончить", если это последнее упражнение в тренировке.
var training = getParam('training');
var listCurrentPractice = JSON.parse(sessionStorage.getItem(training));
var nextNumber = +getParam('number') + 1;
var nextTrainingLink = finishPracticeBody.querySelector('a.return-list');

switch (training) {
    case 'speedy': maxNumber = 5;
        break;
    case 'periodic': maxNumber = 7;
        break;
    case 'optimal': maxNumber = 12;
        break;
    case 'intense': maxNumber = 24;
}

if(nextNumber > maxNumber) {
    nextTrainingLink.setAttribute('href', '/training');
    nextTrainingLink.innerHTML = "Закончить";
} else {
    var nextPractice = listCurrentPractice[nextNumber - 1];
    nextTrainingLink.setAttribute('href', '/training/' + nextPractice[1] + '?training=' + training + '&number=' + nextNumber);
}

function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

/**
 * Действия по нажатию кнопки Старт
 * - убираем блок с кнопкой
 * - активируем блок со счётчиком обратного отсчёта
 * - запускаем обратный отсчёт
 * - после показа цифры 1 запускаем функцию начала упражнения
 */
function clickStart() {

    startWrapper.classList.remove('active');
    counterStart.classList.add('active');

    var i = 3;
    setTimeout(function run() {
        counterStart.innerHTML = i;

        if (+counterStart.style.opacity === 0) {
            counterStart.style.opacity = '1';
        } else {
            counterStart.style.opacity = '0';
            i--;
        }
        if (i > 0) {
            setTimeout(run, 500);
        } else {
            setTimeout(function() {
                counterStart.innerHTML = '1';
                startCounter();
            }, 500);
        }
    }, 500);

}

/**
 * Сортировка массива в случайном порядке
 *
 * @param arr передаваемый для сортировки массив
 * @returns arr отсортированный массив
 */
function shuffle(arr){
    var j, temp;
    for(var i = arr.length - 1; i > 0; i--){
        j = Math.floor(Math.random()*(i + 1));
        temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
    return arr;
}

/**
 * Запоминание уровня на котором остановился пользователь
 *
 */
/*async function rememberLevel() {

 fetch('/practice/schulte', {
 method: 'POST',
 headers: { 'Content-type': 'application/x-www-form-urlencoded' },
 body: "level=" + levelPractice,
 })
 .then(response => response.text())
 .then(results => console.log(results));

 }*/
function rememberLevel(fileName, data) {

    if (!data) {
        data = "level=" + levelPractice;
    }

    var req = getXmlHttp();
    var sendLevel = data;
    req.open('POST', '/practice/' + fileName, true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                console.log(req.responseText);
            }
        }
    };
    req.send(sendLevel);

}
