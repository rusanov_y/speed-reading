/**
 * Обработка действий в упражнении Поиск букв
 *
 * @var arrLetters array - массив значений букв выводимых в таблице
 * @var arrLevels array - настройки уровней: 0 - кол-во ячеек в таблице, 1 - кол-во столбцов, 2 - количество слов для поиска, 3 - размер шрифта
 * @var settingForm DOM-element - форма с настройками тренировки
 * @var startWrapper DOM-element - блок кнопки Старт
 * @var tblWrapper DOM-element - блок с таблицей
 * @var counterStart DOM-element - блок счётчика перед началом показа
 * @var currentAction DOM-element - строка где показывается, что надо сделать (Нажмите на кнопку, нажмите на букву)
 * @var infoBlock DOM-element - строка где показывается какие слова найти
 * @var practiceWrapper DOM-element - блок упражнения
 * @var levelPractice integer - текущий уровень сложности
 * @var countSuccess integer -
 * @var countError integer -
 * @var rememberLevelStart integer - уровень, с которого началась тренировка
 * @var currentSearchWords array - текущие слова для поиска
 * @var arrWords array - все слова
 * @var arrPos array
 */

'use strict';

var arrLevels = {
    1: [3, 5, 5, 3, 32, 15000],
    2: [3, 6, 5, 3, 30, 15000],
    3: [3, 6, 5, 4, 30, 15000],
    4: [3, 7, 5, 4, 28, 15000],
    5: [3, 7, 5, 5, 26, 15000],
    6: [3, 8, 5, 5, 24, 15000],
    7: [3, 9, 5, 5, 22, 15000],
    8: [3, 9, 5, 6, 22, 15000]
};

var currentSearchWords = [],
    arrWords = [],
    arrPos = [],
    arrResultTable = [];

var ageUser = sessionStorage.getItem('userAge');
var tblBody = tblWrapper.querySelector('tbody');

// получение массива слов и алфавита
function getWords() {

    var req = getXmlHttp();
    var sendLevel = "words=true";
    req.open('POST', '/practice/words-twins', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                arrWords = JSON.parse(req.responseText);
                shuffle(arrWords);
            }
        }
    };
    req.send(sendLevel);

}

getWords();

/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    practiceStart();
    setTimeout(finishPractice, timePractice);
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 */
function practiceStart() {

    // выбираем в случайном порядке пары слов, которые не будут повторяться.
    // заполняем массив слов для показа
    for (var i = 0; i < arrLevels[levelPractice][2];) {

        var randomDigital = Math.floor(Math.random() * arrWords.length);
        var j = false;

        // проверяем, чтобы не было повторов
        currentSearchWords.forEach(function (item) {
            if (item[0] === arrWords[randomDigital][0]) {
                j = true;
            }
        });

        if (arrWords[randomDigital][0].length <= arrLevels[levelPractice][3] && j === false) {
            currentSearchWords[i] = arrWords[randomDigital];
            i++;
        }

    }

    tableFilling();
}

/**
 * Заполнение таблицы значениями
 * - сортируем массив со значениями в случайном порядке
 * - в зависимости от уровня сложности определяем количество ячеек в строке (кол-во div),
 * - также вставляются, при необходимости, ячейки с пустыми значениями
 * - в цикле добавляем ячейки (div) в блок с таблицей
 *
 */
function tableFilling() {

    // устанавливаем размер шрифта для таблицы
    if (document.documentElement.clientWidth < 440) {
        tblWrapper.style.fontSize = arrLevels[levelPractice][4] - 4 + "px";
    } else {
        tblWrapper.style.fontSize = arrLevels[levelPractice][4] + "px";
    }


    // очищаем старые данные
    tblBody.innerHTML = '';
    arrPos.length = 0;

    // считаем количество ячеек в таблице
    var countCells = arrLevels[levelPractice][0] * arrLevels[levelPractice][1];

    // определяем в каких ячейках будут показываться не совпадающие слова
    for (var i = 0; i < arrLevels[levelPractice][2];) {

        var randomDigital = Math.floor(Math.random() * countCells);

        var j = false;

        arrPos.forEach(function (item) {
            if (item === randomDigital) {
                j = true;
            }
        });

        if (j === false) {
            arrPos[i] = randomDigital;
            i++;
        }

    }

    // заполняем таблицу
    var s = 0, // счётчик ячеек таблицы
        t = false; // индикатор наличия номера ячейки в массиве тех ячеек, которые будут вставляться без повтора

    for (var q = 0; q < arrLevels[levelPractice][1]; q++) {
        // создаём очередную строку таблицы
        var row = document.createElement('tr');
        tblBody.append(row);

        for (var r = 0; r < arrLevels[levelPractice][0];) {
            // выбираем любой элемент из массива слов
            var numberInsertWord = Math.floor(Math.random() * arrWords.length);

            // проверяем длину слова, чтобы не превышала установленной длины для уровня
            if (arrWords[numberInsertWord][0].length <= arrLevels[levelPractice][3]) {

                // проверяем наличие номера этой ячейки с массиве вставляемых ячеек
                t = arrPos.includes(s);

                var newTd = document.createElement('td');

                if (t === true) {
                    // если номер ячейки есть в массиве ячеек, которые будут вставляться со словами близнецами
                    var el = currentSearchWords.pop();
                    newTd.innerHTML = el[0] + "<br>" + el[1];
                    newTd.setAttribute('data-value', s);
                } else {
                    newTd.innerHTML = arrWords[numberInsertWord][0] + "<br>" + arrWords[numberInsertWord][0];
                }
                row.append(newTd);
                r++;
                s++;
            }

        }
    }

    exercise();
}

/**
 * Показ упражнения
 *
 * @var startTime integer время начала показа
 * @var finishTime integer время, когда найдено последнее слово
 * @var travelTime integer время за которое были найдены все слова
 * @var collectionWordsForSearch collection коллекция слов в инфоблоке
 */
function exercise() {

    var startTime,
        finishTime,
        travelTime,
        collectionWordsForSearch;

    arrPos.length = 0;

    timeLine.style.webkitTransitionDuration = timePractice + 'ms';
    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    // скрываем блок обратного отсчёта
    // показываем блок упражнения
    counterStart.classList.remove('active');
    tblWrapper.parentNode.classList.add('active');
    tblWrapper.style.opacity = '1';
    timeLine.classList.add('active');

    startTime = Date.now();


    collectionWordsForSearch = tblBody.querySelectorAll('td');

    // обрабатываем нажатие на кнопки
    var i = 0;
    tblWrapper.onclick = function (e) {

        var el = e.target;
        if ( el.dataset.value !== undefined ) {

            if (arrPos.indexOf(el.dataset.value) === -1) {

                arrPos[i] = el.dataset.value;
                i++;

                //скрываем в таблице найденное слово
                var clickElements = collectionWordsForSearch[el.dataset.value];
                    clickElements.style.opacity = '0';
                    clickElements.removeAttribute('data-value');

                // проверяем сколько слов найдено
                if (i === arrLevels[levelPractice][2]) {
                    // фиксируем время, когда найдено последнее слово
                    // вычисляем время, за которое были найдены все слова
                    // запоминаем значение времени прохождения таблицы и темпа
                    finishTime = Date.now();
                    travelTime = finishTime - startTime;
                    arrResultTable.push([travelTime, arrLevels[levelPractice][2] / (travelTime / 1000)]);

                    // в зависимости от времени, за которое были найдены слова
                    // увеличиваем или увеньшаем уровень сложности
                    if ( travelTime < arrLevels[levelPractice][5]) {

                        countSuccess++;
                        countError = 0;

                        if ( countSuccess > 4 ) {

                            if ( levelPractice !== Object.keys(arrLevels).length ) {
                                countSuccess = 0;
                                levelPractice++;
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#007700';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }

                    } else {

                        countSuccess = 0;
                        countError++;

                        if ( countError > 2 ) {
                            countError = 0;

                            if ( levelPractice !== 1 ) {
                                levelPractice--;
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#dd0000';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }
                    }

                    currentSearchWords.length = 0;
                    return practiceStart();
                }

            }

        }
    }

}

/**
 * действия после окончания времени тренировки
 */
function finishPractice() {
    rememberLevel('words-twins');

    var arrLabel = [],
        arrValue = [],
        t = 0,
        ef = 0;

    arrResultTable.forEach(function (item, index) {
        arrLabel.push(index + 1);
        arrValue.push(item[1].toFixed(2));
        t += item[0];
    });

    ef = Math.round(t / arrResultTable.length);

    var blockEfficiency = finishPracticeBody.querySelector('p.result-practice');

    if (ef <= 5000) {
        blockEfficiency.innerHTML = '5 баллов';
    }

    if ( (ageUser < 11 && (ef > 5000 && ef <= 9000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 5000 && ef <= 8000)) || ((ageUser >= 12 || !ageUser) && (ef > 5000 && ef <= 7000)) ) {
        blockEfficiency.innerHTML = '4 балла';
    }

    if ( (ageUser < 11 && (ef > 9000 && ef <= 14000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 8000 && ef <= 12000)) || ((ageUser >= 12 || !ageUser) && (ef > 7000 && ef <= 11000)) ) {
        blockEfficiency.innerHTML = '3 балла';
    }

    if ( (ageUser < 11 && (ef > 14000 && ef <= 20000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 12000 && ef <= 17000)) || ((ageUser >= 12 || !ageUser) && (ef > 11000 && ef <= 15000)) ) {
        blockEfficiency.innerHTML = '2 балла';
    }

    if ( (ageUser < 11 && ef > 20000) || ((ageUser >= 11 && ageUser < 12) && ef > 17000) || ((ageUser >= 12 || !ageUser) && ef > 15000) ) {
        blockEfficiency.innerHTML = '1 балл';
    }

    new Chartist.Line('.ct-chart', {
        labels: arrLabel,
        series: [
            arrValue
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 20,
            left: 0
        },
        // Настройки X-оси
        axisX: {
            // Отключаем сетку для этой оси
            showGrid: false
        },
        // настройки Y-оси
        axisY: {
            showGrid: false
        }
    });


    practiceWrapper.classList.remove('active');
    finishPracticeBody.classList.add('active');
}

clickStart();