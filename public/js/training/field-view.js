/**
 * Обработка показа примеров упражнения Бегущая строка
 *
 * При загрузке страницы устанавливаем язык, на котором будет показываться тренажёр
 * инициализируется объект с настройками упражнения;
 * инициализуруются глобальные переменные упражнения;
 * запрашиваем и получаем в виде строки слова для показа
 * тут же строку преобразуем в массив
 *
 * @var lang string - язык;
 * @var arrLevels objekt - объект в котором указываются настройки уровней: 0 - скорость показа слов (слов/мин)
 * @var levelPractice integer - текущий уровень сложности
 * @var rememberLevelStart integer - запоминание уровня с которого началась тренировка
 * @var words string - строка с используемыми словами, полученная с сервера
 * @var arrWords array - массив используемых слов
 * @var successAnswer string - правильный ответ
 * @var filledOut boolean - указатель заполненности блока со словами
 * @var positionArrWords integer - указатель, показывающий где остановились в выборе слов
 * @var positionBlock integer - указатель, показывающий позицию следующего слова для показа в блоке показа слов
 * @var currentLine integer - номер текущей строки блока показа слов
 * @var currentLength integer - текущая длина строки
 * @var arrAnswer array - массив слов для показа в качестве ответов
 * @var arrViewElem array - массив, с позициями слов для показа
 * @var widthLetters integer - ширина символа
 * @var countSuccess array - количество данных подряд правильных ответов
 * @var countError array - количество данных подряд неправильных ответов
 */

'use strict';

var arrLevels = {
    1: [1, 4],
    2: [1, 4],
    3: [2, 4],
    4: [3, 4],
    5: [3, 5],
    6: [4, 5],
    7: [4, 6],
    8: [5, 6],
    9: [5, 7]
};

var words,
    identicalLetters,
    updateLevel = false,
    arrLetters = [],
    arrCellsLevel = {},
    arrPos = [],
    arrResultTable = [0, 0];

var currentAction = practiceWrapper.querySelector('#currentAction');
var infoBlockBody = infoBlock.tBodies[0];
var rowInfoBlock = infoBlockBody.rows;

function getLetters() {

    var req = getXmlHttp();
    var sendLetters = "letters=true";
    req.open('POST', '/practice/field-view', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                arrLetters = JSON.parse(req.responseText);
            }
        }
    };
    req.send(sendLetters);

}

getLetters();

// создаём массивы , в которых будут показываться буквы
for (var key in arrLevels) {

    arrCellsLevel[key] = [];
    for (var x = 5 - arrLevels[key][0]; x <= 5 + arrLevels[key][0]; x++) {

        var m = rowInfoBlock[x].cells;

        for (var p in m) {

            if (p >= 5 - arrLevels[key][0] && p <= 5 + arrLevels[key][0]) {

                if (m.hasOwnProperty(p)) {

                    if (x !== 5 || (x === 5 && p !== '5')) {

                        arrCellsLevel[key].push(m[p]);

                    }

                }

            }

        }

    }
}


/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    // заполняем таблицу в случайном порядке буквами
    for (var i = 0; i < 11; i++) {

        var cellsCurrentRow = rowInfoBlock[i].cells;

        for (var j = 0; j < 11; j++) {

            var randomDigital = Math.floor(Math.random() * arrLetters.length);

            if (i === 5 && j === 5 ) {

            } else {
                cellsCurrentRow[j].innerHTML = arrLetters[randomDigital];
            }

        }

    }

    practiceStart();
    setTimeout(finishPractice, timePractice);
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    // удаляем ранее проставленные классы 'active'
    if (arrPos.length > 0) {
        for (var i = 0; i < arrPos.length; i++) {

            var rnd = Math.floor(Math.random() * arrLetters.length);
            arrCellsLevel[levelPractice][arrPos[i]].textContent = arrLetters[rnd];
            arrCellsLevel[levelPractice][arrPos[i]].classList.remove('active');

        }
    }

    // переход на следующий уровень
    if (updateLevel) {

        levelPractice += updateLevel;

        if (updateLevel === 1) {
            currentLevelBody.style.color = '#007700';
        } else {
            currentLevelBody.style.color = '#dd0000';
        }
        currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
        currentLevelBody.classList.add('active');
        setTimeout(function () {
            currentLevelBody.classList.remove('active');
        }, 2000);

        updateLevel = false;
    }

    blockFilling();

}

/**
 * Заполнение блока, показываемых слов
 *
 */
function blockFilling() {

    // выбираем букву, которую будем показывать
    var randomLetter = Math.floor(Math.random() * arrLetters.length);

    // определяем начальное условие, что все буквы разные
    identicalLetters = false;

    // определяем в случайном порядке показывать одинаковые буквы или нет
    var l = Math.floor(Math.random() * 20);

    if (l < 10) {
        identicalLetters = true;
    }

    // определяем ячейки для заполнения
    if (levelPractice === 1) {
        // если первый уровень, устанавливаем строго установленные ячейки
        // ромб
        arrPos[0] = 1;
        arrPos[1] = 3;
        arrPos[2] = 4;
        arrPos[3] = 6;

    } else {
        // случайным образом определяем в каких ячейках будут размещаться буквы
        // делаем эти ячейки активными
        for( var i = 0; i < arrLevels[levelPractice][1];) {

            var rnd = Math.floor(Math.random() * arrCellsLevel[levelPractice].length);

            if (arrPos.indexOf(rnd) === -1) {
                arrPos[i] = rnd;
                i++;
            }

        }

    }

    // если выбрано, что показываются одинаковые буквы
    // всем ячейкам присваиваем одинаковое значение
    // также ячейке проставляем класс active
    for (i = 0; i < arrPos.length; i++) {
        arrCellsLevel[levelPractice][arrPos[i]].textContent = arrLetters[randomLetter];
        arrCellsLevel[levelPractice][arrPos[i]].classList.add('active');
    }

    if (identicalLetters === false) {

        var cd;

        if (l > 10 && l < 18) {

            if ((l % 2) > 0) {
                cd = 1;
            } else {
                cd = 2;
            }
        } else {
            cd = 3;
        }

        for (var j = 0; j < cd; j++) {

            rnd = Math.floor(Math.random() * arrPos.length);
            i = Math.floor(Math.random() * arrLetters.length);
            arrCellsLevel[levelPractice][arrPos[rnd]].textContent = arrLetters[i];

        }

    }

    exercise();

}

/**
 * Показ упражнения
 *
 * @var elemInfoBlock collection коллекция элементов в блоке
 */
function exercise() {

    var startTime,
        finishTime,
        travelTime;

    timeLine.style.webkitTransitionDuration = timePractice + 'ms';
    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    // скрываем блок обратного отсчёта
    // показываем блок упражнения
    counterStart.classList.remove('active');
    practiceWrapper.classList.add('active');
    timeLine.classList.add('active');

    startTime = Date.now();

    tblWrapper.onclick = function (e) {

        finishTime = Date.now();
        travelTime = startTime - finishTime;

        var btn = false;

        if (e.target.dataset.value === '1') {
            btn = true;
        }

        if (btn === identicalLetters) {
            arrResultTable[0] = arrResultTable[0] + 1;
        } else  {
            arrResultTable[1] = arrResultTable[1] + 1;
        }

        if (btn === identicalLetters && travelTime < 1500 ) {

            countSuccess++;

            if (countSuccess > 4) {
                countSuccess = 0;
                countError = 0;

                if (levelPractice < Object.keys(arrLevels).length) {
                    updateLevel = 1;
                }
            }

        } else {

            countError++;

            if (countError > 2) {

                countSuccess = 0;
                countError = 0;

                if (levelPractice > 1) {
                    updateLevel = -1;
                }
            }

        }

        practiceStart();

    }

}

/**
 * показ блока окончания упражнения
 */
function finishPractice() {
    rememberLevel('field-view');

    var blockResult = finishPracticeBody.querySelector('p.result-practice');
    var par = blockResult.nextElementSibling;

    var totalAnswer = arrResultTable[0] + arrResultTable[1];

    blockResult.innerHTML = Math.round(arrResultTable[0] * 500 / totalAnswer) + ' баллов';
    par.innerHTML = "Правильно " + arrResultTable[0] + " из " + totalAnswer;

    practiceWrapper.classList.remove('active');
    finishPracticeBody.classList.add('active');
}

clickStart();