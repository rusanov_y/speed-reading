/**
 * Обработка действий в упражнении Поиск букв
 *
 * @var arrLetters array - массив значений букв выводимых в таблице
 * @var arrLevels array - настройки уровней: 0 - кол-во ячеек в таблице, 1 - кол-во столбцов, 2 - количество слов для поиска, 3 - размер шрифта
 * @var settingForm DOM-element - форма с настройками тренировки
 * @var startWrapper DOM-element - блок кнопки Старт
 * @var tblWrapper DOM-element - блок с таблицей
 * @var counterStart DOM-element - блок счётчика перед началом показа
 * @var currentAction DOM-element - строка где показывается, что надо сделать (Нажмите на кнопку, нажмите на букву)
 * @var infoBlock DOM-element - строка где показывается какие слова найти
 * @var practiceWrapper DOM-element - блок упражнения
 * @var levelPractice integer - текущий уровень сложности
 * @var countSuccess integer -
 * @var countError integer -
 * @var rememberLevelStart integer - уровень, с которого началась тренировка
 * @var currentSearchWords array - текущие слова для поиска
 * @var arrWords array - все слова
 * @var arrLetters array - алфавит
 * @var arrPos array
 */

'use strict';

var arrLevels = {
    1: [5, 5, 3, 34, 15000],
    2: [5, 6, 3, 32, 15000],
    3: [5, 7, 3, 30, 15000],
    4: [5, 8, 3, 28, 15000],
    5: [6, 7, 3, 30, 15000],
    6: [6, 8, 3, 28, 15000],
    7: [6, 9, 3, 26, 15000],
    8: [6, 10, 3, 24, 15000],
    9: [7, 9, 3, 26, 15000],
    10: [7, 10, 3, 24, 15000]
};

var currentSearchWords = [],
    arrWords = [],
    arrLetters = [],
    arrPos = [],
    arrResultTable = [],
    ageUser = sessionStorage.getItem('userAge');

var currentAction = practiceWrapper.querySelector('#currentAction');
var tblBody = tblWrapper.querySelector('tbody');

// получение массива слов и алфавита
function getWords() {

    var req = getXmlHttp();
    var sendLevel = "words=true";
    req.open('POST', '/practice/word-search', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                var tmpData = JSON.parse(req.responseText);
                var tmpWords = tmpData['words'];
                arrWords = tmpWords.split(',');
                arrLetters = tmpData['letters'];
            }
        }
    };
    req.send(sendLevel);

}

getWords();

/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    practiceStart();
    setTimeout(finishPractice, timePractice);
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 */
function practiceStart() {

    // удаляем классы, помогающие придать ячейкам одинаковый размер
    tblWrapper.classList.remove('col-' + arrLevels[levelPractice][2]);

        // выбираем в случайном порядке слова для поиска
        // заполняем массив слов для показа
    for (var i = 0; i < arrLevels[levelPractice][2];) {

        var randomDigital = Math.floor(Math.random() * arrWords.length);

        var j = false;

            // проверяем, чтобы не было повторов
        currentSearchWords.forEach(function (item) {
            if (item === arrWords[randomDigital]) {
                j = true;
            }
        });

        if (arrWords[randomDigital].length <= arrLevels[levelPractice][0] && j === false) {
            currentSearchWords[i] = arrWords[randomDigital];
            i++;
        }

    }

    tableFilling();
}

/**
 * Заполнение таблицы значениями
 * - сортируем массив со значениями в случайном порядке
 * - в зависимости от уровня сложности определяем количество ячеек в строке (кол-во div),
 * - также вставляются, при необходимости, ячейки с пустыми значениями
 * - в цикле добавляем ячейки (div) в блок с таблицей
 *
 */
function tableFilling() {

    // очищаем старые данные
    tblBody.innerHTML = '';
    arrPos.length = 0;

    // добавляем таблице класс по количеству колонок
    tblWrapper.classList.add('col-' + arrLevels[levelPractice][0]);

    // устанавливаем размер шрифта для таблицы
    tblWrapper.style.fontSize = arrLevels[levelPractice][3] + "px";

    // определяем строки, в которых будут показываться слова
    // для отработки парных букв, комбинации будут в каждой строке
    while (arrPos.length < arrLevels[levelPractice][2]) {
        var rand = Math.floor(Math.random() * arrLevels[levelPractice][1]);
        if (arrPos.indexOf(rand) === -1) {
            arrPos.push(rand);
        }
    }

    arrPos.sort(function(a, b) { return a - b; });

    // заполняем таблицу произвольными буквами
    for (var q = 0; q < arrLevels[levelPractice][1]; q++) {
        var row = document.createElement('tr');
        tblBody.append(row);
        var r = 0;
        while (r < arrLevels[levelPractice][0]) {
            var numberInsertLetter = Math.floor(Math.random() * arrLetters.length);
            var newTd = document.createElement('td');
            newTd.innerHTML = arrLetters[numberInsertLetter];
            row.append(newTd);
            r++;
        }
    }

    // вставляем в ранее выбранные строки слова для показа
    var arrRow = tblBody.rows;

    for (var w = 0; w < arrPos.length; w++) {

        var cellsRow = arrRow[arrPos[w]].cells;
        var beginWord = 0;

        if (cellsRow.length > currentSearchWords[w].length) {
            beginWord = Math.floor(Math.random() * (cellsRow.length - currentSearchWords[w].length + .5));
        }

        for (var z = 0; z < currentSearchWords[w].length; z++) {
            cellsRow[beginWord].innerHTML = currentSearchWords[w][z];
            cellsRow[beginWord].setAttribute('data-word', currentSearchWords[w]);
            beginWord++;
        }

    }

    // формируем строку, в которой будем показывать какие слова надо найти
    currentSearchWords.sort();

    var str = "";
    for (var i = 0; i < currentSearchWords.length; i++) {

        if (i === currentSearchWords.length - 1) {
            str += "<span data-word = '" + currentSearchWords[i] + "'>" + currentSearchWords[i] +"</span>";
        } else {
            str += "<span data-word = '" + currentSearchWords[i] + "'>" + currentSearchWords[i] +",</span> ";
        }
    }

    infoBlock.innerHTML = str;

    exercise();
}

/**
 * Показ упражнения
 *
 * @var startTime integer время начала показа
 * @var finishTime integer время, когда найдено последнее слово
 * @var travelTime integer время за которое были найдены все слова
 * @var collectionWordsForSearch collection коллекция слов в инфоблоке
 */
function exercise() {

    var startTime,
        finishTime,
        travelTime,
        countViewElement,
        collectionWordsForSearch;

    arrPos.length = 0;

    countViewElement = arrLevels[levelPractice][2];

    timeLine.style.webkitTransitionDuration = timePractice + 'ms';
    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    // скрываем блок обратного отсчёта
    // показываем блок упражнения
    counterStart.classList.remove('active');
    tblWrapper.parentNode.classList.add('active');
    tblWrapper.style.opacity = '1';
    timeLine.classList.add('active');

    startTime = Date.now();

    var ib = document.getElementById('info-block');
    collectionWordsForSearch = ib.querySelectorAll('span');

    // обрабатываем нажатие на кнопки
    var i = 0;
    tblWrapper.onclick = function (e) {

        var ceilClick = e.target;

        if ( ceilClick.dataset.word !== undefined ) {

            if ( arrPos.indexOf(ceilClick.dataset.word) === -1 && arrPos.indexOf(ceilClick.dataset.word) !== 'no-opacity' ) {

                arrPos[i] = ceilClick.dataset.word;
                i++;

                // скрываем в информблоке найденное слово
                collectionWordsForSearch.forEach(function (item) {
                    if (item.dataset.word === ceilClick.dataset.word ) {
                        item.style.opacity = '0';
                    }
                });

                //скрываем в таблице найденное слово
                var clickElements = tblBody.querySelectorAll('td[data-word="' + ceilClick.dataset.word + '"]');

                for (var c = 0; c < clickElements.length; c++) {
                    clickElements[c].style.opacity = '0';
                    clickElements[c].removeAttribute('data-word');
                }

                // проверяем сколько слов найдено
                if (i === countViewElement) {
                    // фиксируем время, когда найдено последнее слово
                    // вычисляем время, за которое были найдены все слова
                    finishTime = Date.now();
                    travelTime = finishTime - startTime;
                    arrResultTable.push([travelTime, arrLevels[levelPractice][2] / (travelTime / 1000)]);

                    // в зависимости от времени, за которое были найдены слова
                    // увеличиваем или увеньшаем уровень сложности
                    if ( travelTime < arrLevels[levelPractice][4]) {

                        countSuccess++;
                        countError = 0;

                        if ( countSuccess > 4 ) {

                            if ( levelPractice !== Object.keys(arrLevels).length ) {
                                countSuccess = 0;
                                levelPractice++;
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#007700';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }

                    } else {

                        countSuccess = 0;
                        countError++;

                        if ( countError > 2 ) {
                            countError = 0;

                            if ( levelPractice !== 1 ) {
                                levelPractice--;
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#dd0000';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }
                    }

                    currentSearchWords.length = 0;
                    return practiceStart();
                }

            }

        }
    }

}

/**
 * действия после окончания времени тренировки
 */
function finishPractice() {

    rememberLevel('word-search');

    var arrLabel = [],
        arrValue = [],
        t = 0;

    arrResultTable.forEach(function (value, index) {
        t += value[0];
        arrLabel.push(index++);
        arrValue.push(value[1].toFixed(2));
    });

    var ef = Math.round(t / arrResultTable.length);

    var blockEfficiency = finishPracticeBody.querySelector('p.result-practice');

    if (ef <= 5000) {
        blockEfficiency.innerHTML = '5 баллов';
    }

    if ( (ageUser < 11 && (ef > 5000 && ef <= 9000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 5000 && ef <= 8000)) || ((ageUser >= 12 || !ageUser) && (ef > 5000 && ef <= 7000)) ) {
        blockEfficiency.innerHTML = '4 балла';
    }

    if ( (ageUser < 11 && (ef > 9000 && ef <= 14000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 8000 && ef <= 12000)) || ((ageUser >= 12 || !ageUser) && (ef > 7000 && ef <= 11000)) ) {
        blockEfficiency.innerHTML = '3 балла';
    }

    if ( (ageUser < 11 && (ef > 14000 && ef <= 20000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 12000 && ef <= 17000)) || ((ageUser >= 12 || !ageUser) && (ef > 11000 && ef <= 15000)) ) {
        blockEfficiency.innerHTML = '2 балла';
    }

    if ( (ageUser < 11 && ef > 20000) || ((ageUser >= 11 && ageUser < 12) && ef > 17000) || ((ageUser >= 12 || !ageUser) && ef > 15000) ) {
        blockEfficiency.innerHTML = '1 балл';
    }

    new Chartist.Line('.ct-chart', {
        labels: arrLabel,
        series: [
            arrValue
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 20,
            left: 0
        },
        // Настройки X-оси
        axisX: {
            // Отключаем сетку для этой оси
            showGrid: false
        },
        // настройки Y-оси
        axisY: {
            showGrid: false
        }
    });

    practiceWrapper.classList.remove('active');
    document.getElementById('finish-practice').classList.add('active');
}

clickStart();
