/**
 * "Оценка чтения"
 */

'use strict';

var heightInfoBody,
    speed,
    timeTransition,
    countPage,
    maxNumber,
    currentTextPage = 1,
    countWordText,
    countLine,
    countLinePage;

var settingWrapper = document.getElementsByClassName('setting-wrapper')[0];
var titleBody = document.getElementById('setting-title');
var settingSpeed = document.getElementById('setting-speed');
var startWrapper = document.getElementById('start-wrapper');
var infoBlock = document.getElementById('info-block');
var practiceWrapper = infoBlock.parentNode;
var textBody = infoBlock.children[0];
var counterStart = document.getElementById('counter-start');
var curtainBody = infoBlock.querySelector('div.curtain');
var finishPracticeBody = document.getElementById('finish-practice');

var elementMain = document.querySelector('main');
practiceWrapper.style.height = elementMain.offsetHeight - 15 + "px";
heightInfoBody = elementMain.offsetHeight - 25;
heightInfoBody = Math.floor(Math.floor(heightInfoBody / 22) * 22);
infoBlock.style.height = heightInfoBody + "px";
countLinePage = heightInfoBody / 22;
countWordText = +textBody.dataset.words;
speed = settingSpeed.innerText;

//получение get параметра по ключу
function getParam(key) {
    var s = window.location.search;
    s = s.match(new RegExp(key + '=([^&=]+)'));
    return s ? s[1] : false;
}

// получение данных и создание ссылки для кнопки "Следующее упражнение" на заключительном экране,
// или формирование ссылки "Закончить", если это последнее упражнение в тренировке.
var training = getParam('training');
var listCurrentPractice = JSON.parse(sessionStorage.getItem(training));
var nextNumber = +getParam('number') + 1;
var nextTrainingLink = finishPracticeBody.querySelector('a.return-list');

switch (training) {
    case 'speedy': maxNumber = 5;
        break;
    case 'periodic': maxNumber = 7;
        break;
    case 'optimal': maxNumber = 12;
        break;
    case 'intense': maxNumber = 24;
}

if(nextNumber > maxNumber) {
    nextTrainingLink.setAttribute('href', '/training');
    nextTrainingLink.innerHTML = "Закончить";
} else {
    var nextPractice = listCurrentPractice[nextNumber - 1];
    nextTrainingLink.setAttribute('href', '/training/' + nextPractice[1] + '?training=' + training + '&number=' + nextNumber);
}

// кроссплатформенное создание XMLHttpRequest
function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

/**
 * Действия по нажатию кнопки Старт
 * - перемешиваем массив со словами
 * - убираем блок с кнопкой
 * - активируем блок со счётчиком
 * - запускаем обратный отсчёт
 * - после показа цифры 1 запускаем функцию начала упражнения
 */
function clickStart() {

    countPage = Math.ceil(textBody.offsetHeight / heightInfoBody);
    countLine = textBody.offsetHeight / 22;

    timeTransition = Math.round(countWordText / speed * 60 / countLine * 1000);

    startWrapper.classList.remove('active');
    counterStart.classList.add('active');

    var i = 3;
    setTimeout(function run() {
        counterStart.innerHTML = i;

        if (+counterStart.style.opacity === 0) {
            counterStart.style.opacity = '1';
        } else {
            counterStart.style.opacity = '0';
            i--;
        }
        if (i > 0) {
            setTimeout(run, 500);
        } else {
            setTimeout(function() {
                counterStart.innerHTML = '1';
                startCounter();
            }, 500);
        }
    }, 500);

}

/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    var elm = settingWrapper.nextElementSibling;
    elm.style.height = "100%";

    var widthBody = practiceWrapper.offsetWidth;
    var widthCurtainLine = widthBody * 2 + 200;

    var bgLine = 'linear-gradient(to right, rgba(255,255,255, .8) 0, rgba(255,255,255, .8) ' + widthBody + 'px, rgba(255,255,255, 0) ' + (widthBody + 25) + 'px, rgba(255,255,255, 0) ' + (widthBody + 175) + 'px, rgba(255,255,255, .8) ' + (widthBody + 200) + 'px, rgba(255,255,255, .8) ' + widthCurtainLine + 'px)';

    // заполняем занавес строками
    for (var i = 0; i < countLine; i++) {
        var curLine = document.createElement('p');
        curLine.innerHTML = '&nbsp';
        curLine.style.width = widthCurtainLine + 'px';
        curLine.style.left = (widthCurtainLine - practiceWrapper.offsetWidth) * (-1) + 'px';
        curLine.style.background = bgLine;
        curtainBody.append(curLine);
    }

    practiceStart();
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    // скрываем блок обратного отсчёта
    // показываем блок упражнения
    counterStart.classList.remove('active');
    practiceWrapper.style.zIndex = '0';

    var rate = (practiceWrapper.offsetWidth + 200) / 200;
    var collectionLineCurtain = curtainBody.querySelectorAll('p');
    var l = 0;

    setTimeout(function runLine() {

        collectionLineCurtain[l].style.webkitTransitionDuration = timeTransition + (timeTransition / rate) + 'ms';
        collectionLineCurtain[l].style.mozTransitionDuration = timeTransition + (timeTransition / rate) + 'ms';
        collectionLineCurtain[l].style.oTransitionDuration = timeTransition + (timeTransition / rate) + 'ms';
        collectionLineCurtain[l].style.transitionDuration = timeTransition + (timeTransition / rate) + 'ms';

        collectionLineCurtain[l].style.left = '0';

        l++;

        if (l < countLine) {

            if (l === currentTextPage * countLinePage + 1) {

                curtainBody.style.top = currentTextPage * heightInfoBody * (-1) + 'px';
                textBody.style.top = currentTextPage * heightInfoBody * (-1) + 'px';
                currentTextPage++;

            }

            setTimeout(runLine, timeTransition);

        } else {

            setTimeout(function () {
                finishPractice();
            }, timeTransition);

        }

    }, 300);

}

function finishPractice() {

    rememberLevel();

    practiceWrapper.style.zIndex = -1;
    document.getElementById('finish-practice').classList.add('active');

}

/**
 * Запоминание последнего уровня
 *
 */
function rememberLevel() {

    var req = getXmlHttp();
    var sendLevel = "level=" + titleBody.innerText;
    req.open('POST', '/practice/reading-accelerator', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(sendLevel);

}

clickStart();
