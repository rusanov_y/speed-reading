/**
 * Обработка действий в упражнении Поиск букв
 *
 * @const arrLetters array - массив значений букв выводимых в таблице
 * @const arrLevels array - многомерный массив в котором указываются настройки уровней: 0 - кол-во ячеек в таблице, 1 - кол-во столбцов, 2 - количество знаков, 3 - размер шрифта, 4 - кол-во цветных элементов
 * @var settingForm DOM-element - форма с настройками тренировки
 * @var startWrapper DOM-element - блок кнопки Старт
 * @var tblWrapper DOM-element - блок с таблицей
 * @var counterWrapper DOM-element - блок счётчика перед началом показа
 * @var currentAction DOM-element - строка где показывается, что надо сделать (Нажмите на кнопку, нажмите на букву)
 * @var currentElementClick DOM-element - строка где показывается какую цифру(букву) сейчас надо нажать
 * @var practiceWrapper DOM-element - блок упражнения
 */

'use strict';

var arrLevels = {
    1: [2, 5, 5, 36, 15000],
    2: [2, 6, 5, 32, 15000],
    3: [3, 5, 5, 36, 15000],
    4: [3, 6, 5, 32, 15000],
    5: [3, 7, 5, 30, 15000],
    6: [3, 8, 5, 28, 15000],
    7: [3, 9, 5, 26, 15000],
    8: [4, 7, 5, 30, 15000]
};

var currentDigital = 0,
    countElements,
    arrPos = [],
    arrResultTable = [],
    ageUser = sessionStorage.getItem('userAge');

var tblBody = tblWrapper.querySelector('tbody');
var currentAction = practiceWrapper.querySelector('#currentAction');
var currentElementClick = practiceWrapper.querySelector('#currentElementClick');
var currentLevelBody = document.getElementById('current-level');

/**
 * запуск упражнения
 * откладываем показ блока, сообщающего об окончании упражнения на время
 * отведённое на выполнение.
 */
function startCounter() {

    practiceStart();
    setTimeout(finishPractice, timePractice);
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    countElements = arrLevels[levelPractice][0] * arrLevels[levelPractice][1];

    currentDigital = (currentDigital === 0) ? 1 : 0;

    // удаляем классы, помогающие придать ячейкам одинаковый размер
    tblWrapper.classList.remove('col-' + (arrLevels[levelPractice][2] - 1));
    tblWrapper.classList.remove('col-' + arrLevels[levelPractice][2]);
    tblWrapper.classList.remove('col-' + (arrLevels[levelPractice][2] + 1));

    tableFilling();

    exercise();
}

/**
 * Заполнение таблицы значениями
 * - сортируем массив со значениями в случайном порядке
 * - в зависимости от уровня сложности определяем количество ячеек в строке (кол-во div),
 * - также вставляются, при необходимости, ячейки с пустыми значениями
 * - в цикле добавляем ячейки (div) в блок с таблицей
 *
 */
function tableFilling() {

    // очищаем старые данные
    tblBody.innerHTML = '';
    arrPos.length = 0;

    // добавляем таблице класс по количеству ячеек
    tblWrapper.classList.add('col-' + arrLevels[levelPractice][0]);

    // Массивы основных чисел
    var basicNumber = [];
    var searchNumber = [];

    //определяем строки, в которых будут чётные или нечётные числа.
    while (arrPos.length < arrLevels[levelPractice][2]) {
        var rand = Math.floor(Math.random() * countElements);
        if (arrPos.indexOf(rand) === -1) {
            arrPos.push(rand);
        }
    }

    // случайное число от 1000 до 9999
    function newDigital() {
        return Math.floor(1000 + Math.random() * 9000);
    }

    // заполнение массива с чётными числами
    function formationEvenNumber(countNumber) {

        var k;
        var arrEvenNumber = [];

        for (var i = 0; i < countNumber;) {
            k = newDigital();

            if (k%2 === 0) {
                arrEvenNumber[i] = k;
                i++;
            }
        }

        return arrEvenNumber;
    }

    // заполнение массива с нечётными числами
    function formationOddNumber(countNumber) {

        var m;
        var arrOddNumber = [];

        for (var j = 0; j < countNumber;) {

            m = newDigital();
            if (m%2 !== 0) {
                arrOddNumber[j] = m;
                j++;
            }
        }
        return arrOddNumber;
    }


    if (currentDigital === 0) {
        searchNumber = formationEvenNumber(arrLevels[levelPractice][2]);
        basicNumber = formationOddNumber(countElements - arrLevels[levelPractice][2]);
    } else {
        searchNumber = formationOddNumber(arrLevels[levelPractice][2]);
        basicNumber = formationEvenNumber(countElements - arrLevels[levelPractice][2]);
    }

    arrPos.sort(function(a, b) { return a - b; });

    var currentElem = 0;
    for (var q = 0; q < arrLevels[levelPractice][1]; q++) {
        var row = document.createElement('tr');
        tblBody.append(row);

        for (var r = 0; r < arrLevels[levelPractice][0]; r++) {

            var newTd = document.createElement('td');

            if (arrPos.indexOf(currentElem) === -1) {
                newTd.innerHTML = basicNumber.pop();
            } else {
                newTd.innerHTML = searchNumber.pop();
            }

            row.append(newTd);
            currentElem++;
        }
    }

}

/**
 * Показ упражнения
 * - скрываем блок со счётчиком
 * - показываем блок с таблицей
 * - выводим в верхнем блоке первый элемент, на который надо нажать
 * - запоминаем время начала показа
 * - отслеживаем событие клика по таблице
 * - при клике определяем по какому элементу таблицы был клик
 * - сравниваем значение элемента с текущим значением массива
 * - при совпадении переходим к следующему элементу (увеличиваем счётчик)
 * - проверяем это был последний элемент или нет
 * - если не последний элемент, показываем, какой следующий элемент надо нажать
 * - если последний, то запоминаем время окончания показа
 * - высчитываем время, затраченное на прохождение
 * - если время меньше 20 сек, то увеличиваем уровень сложности (после того как пользователь
 * два раза пройдёт текущий уровень быстрее 20 сек)
 * - если время больше 20 сек., то уменьшаем уровень сложности.
 * - перезапускаем показ перемешав элементы.
 *
 * @var startTime
 * @var finishTime
 * @var travelTime
 */
function exercise() {

    var startTime,
        finishTime,
        travelTime;

    arrPos.length = 0;

    timeLine.style.webkitTransitionDuration = timePractice + 'ms';
    timeLine.style.mozTransitionDuration = timePractice + 'ms';
    timeLine.style.oTransitionDuration = timePractice + 'ms';
    timeLine.style.transitionDuration = timePractice + 'ms';

    tblWrapper.style.fontSize = arrLevels[levelPractice][3] + "px";
    counterStart.classList.remove('active');
    tblWrapper.parentNode.classList.add('active');
    tblWrapper.style.opacity = 1;
    currentElementClick.innerHTML = (currentDigital === 0) ? 'чётные' : 'нечётные';
    timeLine.classList.add('active');

    startTime = Date.now();

    var i = 0;
    tblWrapper.onclick = function (e) {

        var el = e.target;

        if ( el.innerHTML % 2 === currentDigital ) {

            if ( arrPos.indexOf(el.innerHTML) === -1 ) {

                arrPos[i] = el.innerHTML;
                i++;
                el.style.opacity = '0';

                if (i === arrLevels[levelPractice][2]) {
                    finishTime = Date.now();
                    travelTime = finishTime - startTime;
                    arrResultTable.push([travelTime, arrLevels[levelPractice][2] / (travelTime / 1000)]);

                    if ( travelTime < arrLevels[levelPractice][4]) {
                        countSuccess++;

                        if ( countSuccess > 4 ) {

                            if ( levelPractice !== Object.keys(arrLevels).length ) {
                                countSuccess = 0;
                                levelPractice++;
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#007700';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);
                            }
                        }

                    } else {
                        countSuccess = 0;
                        countError++;

                        if ( countError > 2 ) {
                            countError = 0;

                            if ( levelPractice !== 1 ) {
                                levelPractice--;
                                currentLevelBody.innerHTML = 'Вы перешли на уровень ' + levelPractice;
                                currentLevelBody.style.color = '#dd0000';
                                currentLevelBody.classList.add('active');
                                setTimeout(function () {
                                    currentLevelBody.classList.remove('active');
                                }, 2000);

                            }
                        }
                    }

                    return practiceStart();
                }

            }

        }
    };

}

function finishPractice() {
    rememberLevel('even-odd');

    var arrLabel = [],
        arrValue = [],
        t = 0;

    arrResultTable.forEach(function (value, index) {
        t += value[0];
        arrLabel.push(index++);
        arrValue.push(value[1].toFixed(2));
    });

    var ef = Math.round(t / arrResultTable.length);

    var blockEfficiency = finishPracticeBody.querySelector('p.result-practice');

    if (ef <= 8000) {
        blockEfficiency.innerHTML = '5 баллов';
    }

    if ( (ageUser < 11 && (ef > 8000 && ef <= 13000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 8000 && ef <= 12000)) || ((ageUser >= 12 || ageUser === 0) && (ef > 8000 && ef <= 11000)) ) {
        blockEfficiency.innerHTML = '4 балла';
    }

    if ( (ageUser < 11 && (ef > 13000 && ef <= 18000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 12000 && ef <= 16000)) || ((ageUser >= 12 || ageUser === 0) && (ef > 11000 && ef <= 15000)) ) {
        blockEfficiency.innerHTML = '3 балла';
    }

    if ( (ageUser < 11 && (ef > 18000 && ef <= 24000)) || ((ageUser >= 11 && ageUser < 12) && (ef > 16000 && ef <= 21000)) || ((ageUser >= 12 || ageUser === 0) && (ef > 15000 && ef <= 19000)) ) {
        blockEfficiency.innerHTML = '2 балла';
    }

    if ( (ageUser < 11 && ef > 24000) || ((ageUser >= 11 && ageUser < 12) && ef > 21000) || ((ageUser >= 12 || ageUser === 0) && ef > 19000) ) {
        blockEfficiency.innerHTML = '1 балл';
    }

    new Chartist.Line('.ct-chart', {
        labels: arrLabel,
        series: [
            arrValue
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 20,
            left: 0
        },
        // Настройки X-оси
        axisX: {
            // Отключаем сетку для этой оси
            showGrid: false
        },
        // настройки Y-оси
        axisY: {
            showGrid: false
        }
    });

    practiceWrapper.classList.remove('active');
    document.getElementById('finish-practice').classList.add('active');
}

clickStart();