/**
 * "Оценка чтения"
 */

'use strict';

var timeStart,
    timeFinish,
    timeRead,
    heightInfoBody,
    countPage,
    maxNumber,
    currentTextPage = 0,
    successAnswer = 0,
    countWordText;

var settingWrapper = document.getElementsByClassName('setting-wrapper')[0];
var titleBody = document.getElementById('setting-title');
var startWrapper = document.getElementById('start-wrapper');
var infoBlock = document.getElementById('info-block');
var practiceWrapper = infoBlock.parentNode;
var textBody = infoBlock.children[0];
var counterStart = document.getElementById('counter-start');
var controlBlock = document.getElementById('control-block');
var answerBlock = controlBlock.querySelectorAll('li');
var elementMain = document.querySelector('main');
var finishPracticeBody = document.getElementById('finish-practice');

practiceWrapper.style.height = elementMain.offsetHeight - 15 + "px";
heightInfoBody = practiceWrapper.offsetHeight - 60;
heightInfoBody = Math.floor(Math.floor(heightInfoBody / 22) * 22);
infoBlock.style.height = heightInfoBody + "px";

//получение get параметра по ключу
function getParam(key) {
    var s = window.location.search;
    s = s.match(new RegExp(key + '=([^&=]+)'));
    return s ? s[1] : false;
}

// получение данных и создание ссылки для кнопки "Следующее упражнение" на заключительном экране,
// или формирование ссылки "Закончить", если это последнее упражнение в тренировке.
var training = getParam('training');
var listCurrentPractice = JSON.parse(sessionStorage.getItem(training));
var nextNumber = +getParam('number') + 1;
var nextTrainingLink = finishPracticeBody.querySelector('a.return-list');

switch (training) {
    case 'speedy': maxNumber = 5;
        break;
    case 'periodic': maxNumber = 7;
        break;
    case 'optimal': maxNumber = 12;
        break;
    case 'intense': maxNumber = 24;
}

if(nextNumber > maxNumber) {
    nextTrainingLink.setAttribute('href', '/training');
    nextTrainingLink.innerHTML = "Закончить";
} else {
    var nextPractice = listCurrentPractice[nextNumber - 1];
    nextTrainingLink.setAttribute('href', '/training/' + nextPractice[1] + '?training=' + training + '&number=' + nextNumber);
}

// кроссплатформенное создание XMLHttpRequest
function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

/**
 * Действия по нажатию кнопки Старт
 * - перемешиваем массив со словами
 * - убираем блок с кнопкой
 * - активируем блок со счётчиком
 * - запускаем обратный отсчёт
 * - после показа цифры 1 запускаем функцию начала упражнения
 */
function clickStart() {

    startWrapper.classList.remove('active');
    counterStart.classList.add('active');

    var i = 3;
    setTimeout(function run() {
        counterStart.innerHTML = i;

        if (+counterStart.style.opacity === 0) {
            counterStart.style.opacity = '1';
        } else {
            counterStart.style.opacity = '0';
            i--;
        }
        if (i > 0) {
            setTimeout(run, 500);
        } else {
            setTimeout(function() {
                counterStart.innerHTML = '1';
                startCounter();
            }, 500);
        }
    }, 500);

}

/**
 * запуск упражнения
 */
function startCounter() {

    var elm = settingWrapper.nextElementSibling;
    elm.style.height = "100%";
    settingWrapper.style.display = 'none';
    countPage = Math.ceil(textBody.offsetHeight / heightInfoBody);

    rememberTitle(titleBody.innerText);

    practiceStart();
}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    if (countPage === 1) {
        var btnNext = practiceWrapper.querySelector('button');
        btnNext.innerHTML = "Закончить";
        btnNext.setAttribute('onclick', 'finishReadText()');
    }

    // скрываем блок обратного отсчёта
    // показываем блок упражнения
    counterStart.classList.remove('active');
    practiceWrapper.style.zIndex = "0";
    timeStart = new Date();

}

function nextText() {

    currentTextPage++;
    textBody.style.top = currentTextPage * heightInfoBody * (-1) + 'px';

    if (currentTextPage === countPage - 1) {
        var btnNext = practiceWrapper.querySelector('button');
        btnNext.innerHTML = "Закончить";
        btnNext.setAttribute('onclick', 'finishReadText()');
    }
}

function finishReadText() {

    timeFinish = new Date();
    timeRead = (timeFinish - timeStart) / 60000;

    answerBlock[0].style.zIndex = '1';
    answerBlock[0].style.opacity = '1';

    practiceWrapper.style.zIndex = '-1';
    controlBlock.classList.add('active');
}

function answerClick(el) {

    if (el.dataset['answer'] === el.innerHTML) {
        successAnswer++;
    }

    if (el.dataset['number'] === "5") {

        answerBlock[el.dataset['number'] - 1].style.zIndex = "-3";
        setTimeout(function () {
            finishPractice();
        }, 700);

    } else {

        answerBlock[el.dataset['number'] - 1].style.opacity = "0";
        answerBlock[el.dataset['number']].style.zIndex = '1';

        setTimeout(function () {
            answerBlock[el.dataset['number'] - 1].style.zIndex = "-3";
            answerBlock[el.dataset['number']].style.opacity = '1';
        }, 500);

    }
}

function finishPractice() {

    var resultBlock = document.getElementById('result-reading');
    var collectionSpan = resultBlock.querySelectorAll('span');
    var answerBlock = resultBlock.querySelectorAll('p');

    if (!countWordText) {
        countWordText = collectionSpan[0].dataset.countwords;
    }

    collectionSpan[0].innerHTML = Math.round(countWordText / timeRead);
    collectionSpan[1].innerHTML = successAnswer;
    collectionSpan[2].innerHTML = 100 * successAnswer / 5;

    if ( ( 100 * successAnswer ) / 5 < 50 ) {

        answerBlock[0].classList.add('error');
        answerBlock[1].classList.add('error');
        answerBlock[1].innerHTML = 'Много ошибок. Результат не будет учтён.';

    } else {

        rememberSpeed(Math.round(countWordText / timeRead));
        answerBlock[0].classList.add('success');

    }

    controlBlock.classList.remove('active');
    document.getElementById('finish-practice').classList.add('active');

}

/**
 * Запоминание названия текста
 * @param titleText
 */
function rememberTitle(titleText) {
    var req = getXmlHttp();
    var sendTitle = "title=" + titleText;
    req.open('POST', '/practice/reading-grade', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(sendTitle);
}

function rememberSpeed(res) {
    var req = getXmlHttp();
    var sendRes = "level=" + res;
    req.open('POST', '/practice/reading-grade', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(sendRes);
}

clickStart();