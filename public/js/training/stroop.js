/**
 * Тест Струпа
 *
 */

'use strict';

var arrColors = {},
    currentColors = [],
    arrResultTable = [0, 0, 0, 0, 0, 0, 0],
    colorsName = ['red', 'blue', 'green', 'yellow', 'brawn'];

var startWrapper = document.getElementById('start-wrapper');
var counterStart = document.getElementById('counter-start');
var practiceWrapper = document.getElementById('practice-wrapper');
var card1Body = document.getElementById('card-1').querySelector('tbody');
var card2Body = document.getElementById('card-2').querySelector('tbody');
var card3Body = document.getElementById('card-3').querySelector('tbody');
var btnCard1 = document.getElementById('btn-card1').querySelector('tbody');
var btnCard2 = document.getElementById('btn-card2').querySelector('tbody');
var btnCard3 = document.getElementById('btn-card3').querySelector('tbody');
var finishPracticeBody = document.getElementById('finish-practice');

var countColors = '3';

// кроссплатформенное создание XMLHttpRequest
function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

/**
 * Запоминание последнего уровня
 *
 */
function getColorsName() {

    var req = getXmlHttp();
    var sendColors = "colors=true";
    req.open('POST', '/practice/stroop', true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function() {
        if (req.readyState === 4) {
            if(req.status === 200) {
                var tmpData = req.responseText.split(',');
                tmpData.forEach(function (value, index) {
                    arrColors[index] = [colorsName[index], value];
                });
            }
        }
    };
    req.send(sendColors);

}

getColorsName();


/**
 * Действия по нажатию кнопки Старт
 * - перемешиваем массив со словами
 * - убираем блок с кнопкой
 * - активируем блок со счётчиком
 * - запускаем обратный отсчёт
 * - после показа цифры 1 запускаем функцию начала упражнения
 */
function clickStart() {

    startWrapper.classList.remove('active');
    counterStart.classList.add('active');

    var i = 3;
    setTimeout(function run() {
        counterStart.innerHTML = i;

        if (+counterStart.style.opacity === 0) {
            counterStart.style.opacity = '1';
        } else {
            counterStart.style.opacity = '0';
            i--;
        }
        if (i > 0) {
            setTimeout(run, 500);
        } else {
            setTimeout(function() {
                counterStart.innerHTML = '1';
                practiceStart();
            }, 500);
        }
    }, 500);

}

/**
 * Начало выполнения упражнения
 * - удаляем из блока показа таблицы стили для разбивки блока на равные ячейки
 * - определяем количество элементов для показ, в зависимости от уровня
 * - выбираем массив с нужными элементами, в зависимости от выбранного типа символов
 * - запускаем функцию заполнения таблицы
 * - запускаем функцию показа таблицы
 *
 * @var arrCurrent array - текущий массив с данными для показа
 * @var arrSort array - копия текущего массива, преднахначенная для сортировки в случайном порядке
 * @var countElements integer - количество показываемых элементов в зависимости от уровня сложности
 */
function practiceStart() {

    blockFilling();
    exercise();

}

function blockFilling() {

    var countElements,
        btnCards;

    // в зависимости от выбранного количества цветов устанавливаем размер таблиц и
    // готовим таблицу со значениями цветов
    switch (countColors) {
        case '3':
            countElements = 6;
            card1Body.parentElement.classList.add('col-6');
            card2Body.parentElement.classList.add('col-6');
            card3Body.parentElement.classList.add('col-6');
            for (var i = 0; i < 3; i++) {
                currentColors[i] = arrColors[i];
            }
            btnCards = "<tr><td data-color='red'>" + arrColors[0][1] + "</td><td data-color='blue'>" + arrColors[1][1] + "</td><td data-color='green'>" + arrColors[2][1] + "</td></tr>";
            break;
        case '4':
            countElements = 8;
            card1Body.parentElement.classList.add('col-8');
            card2Body.parentElement.classList.add('col-8');
            card3Body.parentElement.classList.add('col-8');
            for (i = 0; i < 4; i++) {
                currentColors[i] = arrColors[i];
            }
            btnCards = "<tr><td data-color='red'>" + arrColors[0][1] + "</td><td data-color='blue'>" + arrColors[1][1] + "</td><td data-color='green'>" + arrColors[2][1] + "</td><td data-color='yellow'>" + arrColors[3][1] + "</td></tr>";
            break;
        default:
            countElements = 10;
            card1Body.parentElement.classList.add('col-10');
            card2Body.parentElement.classList.add('col-10');
            card3Body.parentElement.classList.add('col-10');
            for (i = 0; i < 5; i++) {
                currentColors[i] = arrColors[i];
            }
            btnCards = "<tr><td data-color='red'>" + arrColors[0][1] + "</td><td data-color='blue'>" + arrColors[1][1] + "</td><td data-color='green'>" + arrColors[2][1] + "</td><td data-color='yellow'>" + arrColors[3][1] + "</td><td data-color='brawn'>" + arrColors[4][1] + "</td></tr>";
    }

    // заполняем таблицы для ввода значений
    btnCard1.innerHTML = btnCards;
    btnCard2.innerHTML = btnCards;
    btnCard3.innerHTML = btnCards;

    // временная таблица используемых цветов
    // т.к. должно быть по два раза один и тот же цвет, то объединяем таблицу с текущими цветами саму с собой
    var tmpArr = [];

    tmpArr = currentColors.concat(currentColors);
    shuffle(tmpArr);

    // заполняем карточки значениями.
    card1Body.innerHTML = setContentTable(tmpArr, countElements, false);
    card2Body.innerHTML = setContentTable(tmpArr, countElements, false);
    card3Body.innerHTML = setContentTable(tmpArr, countElements, true);

}

function setContentTable(arr, countElements, textcolor) {

    var contentTable = "",
        arrColorText = [];

    if (textcolor === true) {

        arr.forEach(function (value) {
            arrColorText.push(value[0]);
        });

    }

    for (var i = 0; i < countElements; i++) {
        shuffle(arr);
        shuffle(arrColorText);

        contentTable += "<tr>";
        for (var j = 0; j < countElements; j++) {
            if (textcolor === true) {

                contentTable += "<td class='" + arrColorText[j] + "' data-color='" + arrColorText[j] + "'>";
            } else {
                contentTable += "<td data-color='" + arr[j][0] + "'>";
            }
            contentTable += arr[j][1];
            contentTable += "</td>";
        }
        contentTable += '</tr>';
    }

    arr = [];
    arrColorText.length = 0;

    return contentTable;

}

/**
 * Сортировка массива в случайном порядке
 *
 * @param arr передаваемый для сортировки массив
 * @returns arr отсортированный массив
 */
function shuffle(arr) {
    var j, temp;
    for(var i = arr.length - 1; i > 0; i--){
        j = Math.floor(Math.random()*(i + 1));
        temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
    return arr;
}


function exercise() {

    var startTime,
        finishTime,
        travelTime,
        cardElements,
        countElements;

    counterStart.classList.remove('active');
    practiceWrapper.classList.add('active');

    cardElements = card1Body.querySelectorAll('td');
    countElements = cardElements.length;
    arrResultTable[0] = countElements;

    var i = 0;
    startTime = new Date();

    cardElements[i].classList.add('active');

    btnCard1.onclick = function (ev) {

        var clickElem = ev.target;
        var viewElem = cardElements[i];

        if (clickElem.dataset.color !== viewElem.dataset.color) {

            arrResultTable[1]  = arrResultTable[1] + 1;

        }

        cardElements[i].classList.remove('active');

        i++;

        if (i < countElements) {

            cardElements[i].classList.add('active');

        } else {

            finishTime = new Date();
            travelTime = finishTime - startTime;
            arrResultTable[2] = travelTime;

            document.getElementById('card-1').style.zIndex = '-2';
            document.getElementById('card-2').style.zIndex = '0';

            cardElements = card2Body.querySelectorAll('td');

            i = 0;

            startTime = new Date();

            cardElements[i].classList.add('active');
        }

    };

    btnCard2.onclick = function (ev) {

        var clickElem = ev.target;
        var viewElem = cardElements[i];

        if (clickElem.dataset.color !== viewElem.dataset.color) {

            arrResultTable[3]  = arrResultTable[3] + 1;

        }

        cardElements[i].classList.remove('active');

        i++;

        if (i < countElements) {
            cardElements[i].classList.add('active');
        } else {

            finishTime = new Date();
            travelTime = finishTime - startTime;
            arrResultTable[4] = travelTime;

            document.getElementById('card-2').style.zIndex = '-2';
            document.getElementById('card-3').style.zIndex = '0';

            cardElements = card3Body.querySelectorAll('td');

            i = 0;

            startTime = new Date();

            cardElements[i].classList.add('active');

        }

    };

    btnCard3.onclick = function (ev) {
        var clickElem = ev.target;
        var viewElem = cardElements[i];

        if (clickElem.dataset.color !== viewElem.dataset.color) {

            arrResultTable[5]  = arrResultTable[5] + 1;

        }

        cardElements[i].classList.remove('active');

        i++;

        if (i < countElements) {

            cardElements[i].classList.add('active');

        } else {

            finishTime = new Date();
            travelTime = finishTime - startTime;
            arrResultTable[6] = travelTime;

            finishPractice();

        }
    };

}

function finishPractice() {

    console.log(arrResultTable);
    var resultLines = finishPracticeBody.querySelectorAll('p.result-practice');

    resultLines[0].innerHTML = 'Ошибок: ' + arrResultTable[1] + ' из ' + arrResultTable[0] + '<br>Время прохождения: ' + Math.round(arrResultTable[2] / 1000) + ' сек.';
    resultLines[1].innerHTML = 'Ошибок: ' + arrResultTable[3] + ' из ' + arrResultTable[0] + '<br>Время прохождения: ' + Math.round(arrResultTable[4] / 1000) + ' сек.';
    resultLines[2].innerHTML = 'Ошибок: ' + arrResultTable[5] + ' из ' + arrResultTable[0] + '<br>Время прохождения: ' + Math.round(arrResultTable[6] / 1000) + ' сек.';

    //

    practiceWrapper.classList.remove('active');
    finishPracticeBody.classList.add('active');

}

clickStart();
