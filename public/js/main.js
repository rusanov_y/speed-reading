'use strict';

/**
 * кроссплатформенное создание XMLHttpRequest
 */
function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

/**
 * Проверка наличия на странице чекбокса для изменения пароля
 * При его наличии отслеживаем изменение сосотояния чекбокса
 * если он включен убираем атрибут disabled в поле ввода пароля
 * если он выключен ставим данный атрибут
 */
if ( document.getElementById('edit_password') ) {
    var editPassword = document.getElementById('edit_password');

    editPassword.onchange = function () {

        if (editPassword.checked) {

            document.getElementById('frm-pass').removeAttribute('disabled');

        } else {

            document.getElementById('frm-pass').setAttribute('disabled', true);

        }

    };
}

if ( document.getElementById('view-menu') && window.screen.width < 1500 ) {

    var viewMenu = document.getElementById('view-menu');
    var asideElem = viewMenu.parentNode;
    var sectionElem = asideElem.nextSibling.nextSibling;

    if ( window.screen.width < 1200 ) {

        setTimeout(function () {
            asideElem.style.left = "-250px";
        }, 2000);

    }


    viewMenu.onchange = function () {

        var styleAside = getComputedStyle(asideElem);
        if ( window.screen.width > 1199 ) {

            if (styleAside.left === "0px") {

                asideElem.style.left = "-250px";
                sectionElem.style.marginLeft = "0";

            } else {

                asideElem.style.left = "0";
                sectionElem.style.marginLeft = "270px";

            }

        } else {

            if (styleAside.left === "0px") {

                asideElem.style.left = "-250px";

            } else {

                asideElem.style.left = "0";

            }

        }


    }

}

function homeworkInfo(id) {

    $.ajax({
        type: "POST",
        data: "do=info&id=" + id,
        url: "/mental/main/homework",
        error: function (xhr, str) {
            alert('Возникла ошибка: ' + xhr.responseCode);
        }
    }).done(function (data) {

        if ( data !== 'empty' ) {
            setHome(JSON.parse(data));
        } else {
            setHome('no');
        }
    });

}

function closeHome() {

    var homeworkWrapper= document.getElementById('homework-info-wrapper');
    var homeworkBody = homeworkWrapper.querySelector('div#homework-info-body');
    var tBody = homeworkBody.querySelector('tbody');

    homeworkWrapper.style.display = "none";

    tBody.innerHTML = "";

}

function setHome(data) {

    var homeworkWrapper= document.getElementById('homework-info-wrapper');
    var homeworkBody = homeworkWrapper.querySelector('div#homework-info-body');
    var tBody = homeworkBody.querySelector('tbody');
    var example = new Map([
        ['simple', 'простые'],
        ['five', 'формула 5'],
        ['ten-plus', 'формула 10+'],
        ['ten', 'формула 10-'],
        ['all', 'все формулы'],
        ['multi', 'умножение'],
        ['division', 'деление'],
        ['crazy', 'crazy']
    ]);

    if ( data !== 'no' ) {
        for ( var key in data ) {
            var dateHomework = new Date( Date.parse( data[key]['date_home'] ) );
            var mounthHome = dateHomework.getMonth() + 1;
            var dh = "<tr>" +
                "<td.txt>" + dateHomework.getDate().toString().padStart(2, "0") + "." + mounthHome.toString().padStart(2, "0") + "</td.txt>" +
                "<td.txt>" + example.get(data[key]['example']) + "</td.txt>" +
                "<td.txt>" + data[key]['bit'] + "</td.txt>" +
                "<td.txt>" + data[key]['speed'] / 1000 + "сек.</td.txt>" +
                "<td.txt>" + data[key]['amount'] + "</td.txt>" +
                "<td.txt>" + data[key]['done'] + "</td.txt>" +
                "<td.txt>" + data[key]['cor'] +"</td.txt>" +
                "</tr>";
            $(tBody).append(dh);
            dh = "";
        }
    } else {
        $(tBody).append("<tr><td.txt colspan='7' style='color: #ff0000;'>В памяти нет выполненых домашних заданий </td.txt></tr>");
    }

    $(homeworkWrapper).fadeIn(600);
}

/**
 * запоминание результата выполнения домашнего задания, результат передаётся в файл
 * function.php, где идёт запись в базу данных
 * @param id int - id пользователя
 * @param example str - уровень примеров
 * @param bit int - разрдность примеров
 * @param speed int - скорость выполнения
 * @param amount int - количество примеров (рядов) в одном заходе
 * @param counterAll int - количество решённых заданий
 * @param counterOk int - количество правильных решений
 */
function rememberResult(example, bit, speed, amount) {

    var counterAll = document.getElementById("counter-all");
    var counterOk = document.getElementById("counter-ok");

    var dataUser = "do=remember&counterAll=" + counterAll.textContent + "&counterOk=" + counterOk.textContent + "&example=" + example + "&bit=" + bit + "&speed=" + speed + "&amount=" + amount;

    $.ajax({
        type: "POST",
        data: dataUser,
        url: "/training/homework",
        error: function (xhr, str) {
            alert('Возникла ошибка: ' + xhr.responseCode);
        }
    }).done(function (data) {
        if ( data === 'success' ) {
            alert('Результат записан.');
        } else {
            alert('Ошибка. Попробуйте ещё раз.');
        }
    });

    return false;

}

function openPracticeInfo() {
    console.log('open');
    var elem = document.getElementById('practice-info-wrapper');
    elem.style.display = 'flex';
}

function closePracticeInfo() {
    var elem = document.getElementById('practice-info-wrapper');
    elem.style.display = 'none';
}

if (document.getElementById('localization-form')) {
    
    var langSelectForm = document.getElementById('localization-form');
    var langSelect = langSelectForm.querySelector('select');

    langSelect.addEventListener('change', function (ev) {

        var req = getXmlHttp();
        var data = "lang=" + langSelect.value;
        req.open('POST', '/main/language', true);
        req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.onreadystatechange = function () {
            if (req.readyState === 4) {
                if (req.status === 200) {
                    location.reload();
                }
            }
        };
        req.send(data);

    });
}